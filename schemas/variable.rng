<?xml version="1.0" encoding="UTF-8"?>
<grammar xmlns:a="http://relaxng.org/ns/compatibility/annotations/1.0" xmlns="http://relaxng.org/ns/structure/1.0" datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes">
  <!--
    Copyright (C) 2018 Columbia University in the City of New York and others.
    
    Please see the AUTHORS file in the main source directory for a full list
    of contributors.
    
    This file is part of enki-codegen.
    
    enki-codegen is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    enki-codegen is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.
    
    You should have received a copy of the GNU Lesser General Public License
    along with enki-codegen. If not, see <http://www.gnu.org/licenses/>.
    
  -->
  <include href="spud_base.rng"/>
  <define name="generic_symbol">
    <element name="symbol">
      <ref name="anystring"/>
    </element>
  </define>
  <define name="scalar">
    <element name="rank">
      <a:documentation>The rank of the variable (scalar, rank 0).</a:documentation>
      <attribute name="name">
        <value>Scalar</value>
      </attribute>
    </element>
  </define>
  <define name="vector">
    <element name="rank">
      <a:documentation>The rank of the variable (vector, rank 1).</a:documentation>
      <attribute name="name">
        <value>Vector</value>
      </attribute>
      <element name="size">
        <a:documentation>length of vector (for checking shape)</a:documentation>
        <ref name="integer"/>
      </element>
    </element>
  </define>
  <define name="matrix">
    <element name="rank">
      <a:documentation>The rank of the variable (matrix, rank 2).</a:documentation>
      <attribute name="name">
        <value>Matrix</value>
      </attribute>
      <element name="n_rows">
        <a:documentation>number of rows</a:documentation>
        <ref name="integer"/>
      </element>
      <element name="n_cols">
        <a:documentation>number of columns</a:documentation>
        <ref name="integer"/>
      </element>
    </element>
  </define>
  <define name="vector_K">
    <element name="rank">
      <a:documentation>The rank of the variable (vector, rank 1).</a:documentation>
      <attribute name="name">
        <value>Vector</value>
      </attribute>
      <element name="size">
        <a:documentation>length of vector (set by number of end-members)</a:documentation>
        <value>K</value>
      </element>
    </element>
  </define>
  <define name="vector_N">
    <element name="rank">
      <a:documentation>The rank of the variable (vector, rank 1).</a:documentation>
      <attribute name="name">
        <value>Vector</value>
      </attribute>
      <element name="size">
        <a:documentation>length of vector (set by number of phases)</a:documentation>
        <value>N</value>
      </element>
    </element>
  </define>
  <define name="vector_J">
    <element name="rank">
      <a:documentation>The rank of the variable (vector, rank 1).</a:documentation>
      <attribute name="name">
        <value>Vector</value>
      </attribute>
      <element name="size">
        <a:documentation>length of vector (set by number of reactions)</a:documentation>
        <value>J</value>
      </element>
    </element>
  </define>
  <define name="matrix_N_by_Kmax">
    <element name="rank">
      <a:documentation>The rank of the variable (matrix, rank 2).</a:documentation>
      <attribute name="name">
        <value>Matrix</value>
      </attribute>
      <element name="n_rows">
        <a:documentation>number of rows set by maximum number of
phases N</a:documentation>
        <value>N</value>
      </element>
      <element name="n_cols">
        <a:documentation>number of columns set by maximum number of
endmembers Kmax</a:documentation>
        <value>Kmax</value>
      </element>
    </element>
  </define>
  <define name="generic_variable">
    <element name="variable">
      <a:documentation>Model Variable, these are independent variables of the thermodynamic functions:  Every parameter has a name, an optional symbol and  units.</a:documentation>
      <attribute name="name">
        <data type="string"/>
      </attribute>
      <choice>
        <ref name="scalar"/>
        <ref name="vector"/>
        <ref name="matrix"/>
      </choice>
      <element name="units">
        <a:documentation>space separated list of strings of units</a:documentation>
        <attribute name="lines">
          <value>1</value>
        </attribute>
        <list>
          <oneOrMore>
            <data type="string"/>
          </oneOrMore>
        </list>
      </element>
      <optional>
        <ref name="generic_symbol">
          <a:documentation> Optional sympy symbol for use with this variable, otherwise will default to the variable name
string describing the units of the variable</a:documentation>
        </ref>
      </optional>
      <ref name="comment"/>
    </element>
  </define>
  <define name="variable_T">
    <element name="variable">
      <a:documentation>Model Variable, these are independent variables of the thermodynamic functions
Temperature in Kelvin</a:documentation>
      <attribute name="name">
        <value>T</value>
      </attribute>
      <ref name="scalar"/>
      <element name="units">
        <a:documentation>string describing the units of the variable</a:documentation>
        <value>K</value>
      </element>
      <ref name="comment"/>
    </element>
  </define>
  <define name="variable_P">
    <element name="variable">
      <a:documentation>Model Variable, these are independent variables of the thermodynamic functions
Pressure in bars</a:documentation>
      <attribute name="name">
        <value>P</value>
      </attribute>
      <ref name="scalar"/>
      <element name="units">
        <a:documentation>string describing the units of the variable</a:documentation>
        <value>bar</value>
      </element>
      <ref name="comment"/>
    </element>
  </define>
  <define name="variable_V">
    <element name="variable">
      <a:documentation>Model Variable, these are independent variables of the thermodynamic functions

Volume in bars</a:documentation>
      <attribute name="name">
        <value>V</value>
      </attribute>
      <ref name="scalar"/>
      <element name="units">
        <a:documentation>string describing the units of the variable</a:documentation>
        <value>J/bar-m</value>
      </element>
      <ref name="comment"/>
    </element>
  </define>
  <define name="variable_n">
    <element name="variable">
      <a:documentation>Model Variable, these are independent variables of the thermodynamic functions

vector of moles of components.  The size is automatically set by the spud parser
depending on the number of endmembers included in the file</a:documentation>
      <attribute name="name">
        <value>n</value>
      </attribute>
      <ref name="vector_K"/>
      <element name="units">
        <a:documentation>string describing the units of the variable</a:documentation>
        <value>mol</value>
      </element>
      <optional>
        <ref name="generic_symbol"/>
      </optional>
      <ref name="comment"/>
    </element>
  </define>
  <define name="variable_s">
    <element name="variable">
      <a:documentation>optional vector of order parameters s.  Size must be set by the model</a:documentation>
      <attribute name="name">
        <data type="string"/>
      </attribute>
      <ref name="vector"/>
      <element name="units">
        <a:documentation>string describing the units of the variable</a:documentation>
        <value>None</value>
      </element>
      <optional>
        <ref name="generic_symbol"/>
      </optional>
      <ref name="comment"/>
    </element>
  </define>
  <define name="variable_C">
    <element name="variable">
      <a:documentation>Model Variable, these are independent variables of the thermodynamic functions

matrix of mass weighted concentrations of components in phases.  The size is automatically set by the spud parser
depending on the number of endmembers and phases included in the file</a:documentation>
      <attribute name="name">
        <value>C</value>
      </attribute>
      <ref name="matrix_N_by_Kmax"/>
      <element name="units">
        <value>None</value>
      </element>
      <ref name="comment"/>
    </element>
  </define>
  <define name="variable_Phi">
    <element name="variable">
      <a:documentation>Model Variable, these are independent variables of the thermodynamic functions

vector of phase volume fractions.  The size is automatically set by the spud parser
depending on the number of phases included in the file</a:documentation>
      <attribute name="name">
        <value>Phi</value>
      </attribute>
      <ref name="vector_N"/>
      <element name="units">
        <a:documentation>string describing the units of the variable</a:documentation>
        <value>%</value>
      </element>
      <ref name="comment"/>
    </element>
  </define>
</grammar>
