# Copyright (C) 2018 Columbia University in the City of New York and others.
#
# Please see the AUTHORS file in the main source directory for a full list
# of contributors.
#
# This file is part of enki-codegen.
#
# enki-codegen is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# enki-codegen is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with enki-codegen. If not, see <http://www.gnu.org/licenses/>.
#


include "spud_base.rnc"
include "potentials.rnc"
include "variable.rnc"

explicit =
(
	## Explicit Function: <symbol> = Expression(variables)
	element type {
		attribute name { 'explicit' },
		expression
	}
)

implicit =
(
	## Implicit Function:
	## 	    if scalar: <symbol> = sym.Function(variables)
	##	    if vector: <symbol> = sym.MatrixSymbol(size)?
	##
	##                 Residual(<symbol>) = 0.
	##                 optional sympy expressions  for initial guess and/or bounds
	element type {
		attribute name { 'implicit' },
		( scalar | vector ),
		residual,
		## Initial_Guess:  Optional sympy expression, assumes sympy namespace as sym and is a function of base variables and parameters
		element initial_guess {
			python_code
		}?,
		## Bounds: Optional Boolean sympy expression that evaluates if solution is in bounds
		element bounds {
			python_code
		}?
	}
)

# careful...apparently external is a key word here
external_func =
(
	## External Function:  an externally derived function that depends on defined variables
	##
	##  these symbols must be resolved by the parsing scripts
	##
	##  examples include  mu(T,P) (chemical potentials defined elsewhere) or Debye(x)
	##  can be scalar or vector valued
	element type {
		attribute name {'external'},
		( scalar | vector ),
		( variable_T | variable_P | variable_V | generic_variable)+,
		comment
		}
)
## New coder based unrolled implicit and external functions

implicit_function =
(
	## Implicit Function:  a scalar  implicit function that is resolved with a non-linear solve
	## each function has:
	##     a name (e.g. V)
	## 	   symbol (e.g. V(T,P) = sym.Function('name')(T,P)
	##	   residual:  a function f(name, variables) such that solutions are roots f(0)
	##	   initial guess:  a sympy expression for an initial guess
	element implicit_function {
		attribute name { xsd:string },
		expression,
		element residual {
			attribute name { xsd:string },
			expression,
			comment
		},
		element initial_guess {
			attribute name {xsd:string},
			expression,
			comment
		},
		comment
	}
)

## New coder based order_function for complex solutions.
ordering_function =
(
	## Ordering Function:  a vector valued non-linear function   needed to resolve local order-disorder
	## equilibria through a non-linear solve
	## each function has:
	##     a name
	## 	   dependent variable  (e.g. s) as sympy expression
	##	   residual:  a vector valued function F[s] (list of sympy expressions) st. solutions have F(s) = 0
	##	   initial guess:  a sympy expression for an initial guess
	## 	   bounds:  a sympy boolean and expression to determine if the variable is in permissible bounds
	element ordering_function {
		attribute name { xsd:string },
		variable_s,
		element residual {
			attribute name { xsd:string },
			expression,
			comment
		},
		element guess {
			attribute name {xsd:string},
			expression,
			comment
		},
		element bounds {
			attribute name {xsd:string},
			expression,
			comment
		},
		comment
	}
)

external_function =
(
	## External Function:  an externally derived function that depends on defined variables
	##
	##  these symbols must be resolved by the parsing scripts
	##
	##  examples include  mu(T,P) (chemical potentials defined elsewhere) or Debye(x)
	##  can be scalar or vector valued
	element external_function {
		attribute name { xsd:string },
		( scalar | vector ),
		( variable_T | variable_P | variable_V | generic_variable)+,
		comment
		}
)

generic_function =
  (
	## Function:  Sympy expression: assuming namespace sym to describe function that depends on global variables f(T,P) e.g and local/global parameters
	## Functions can be either  implicit  or explicit or external
	## All functions have a name, an optional sympy symbol
	## explict and implicit functions also have an expression (unless external functions)
	element function {
	  attribute name { xsd:string},
	  ( explicit | implicit | external_func) ,
	  ##   sympy expression that either evaluates to the function of interest (explicit)
	  ##  or is an implicit function of the function that evaluates to zero for the correct value (requires a root solver)
	   ##  Optional sympy symbol for use with this function, otherwise will default to the function name
	  generic_symbol?,
	  reference?,
          comment
       }
  )

function_mu_G =
(
	## reserved symbol for vector of chemical potentials of endmembers mu(T,P) or mu(T,V)
	## size will be set by parser from number  of endmembers K
	element function {
	  attribute name { 'mu' },
	  element type {
		attribute name {'external'},
		vector_K,
		variable_T,
		variable_P,
		comment
	  },
	   ##  Optional sympy symbol for use with this function
	  generic_symbol?,
	  reference?,
          comment
       }

)

function_mu_A =
(
	## reserved symbol for vector of chemical potentials of endmembers mu(T,P) or mu(T,V)
	## size will be set by parser from number  of endmembers K
	element function {
	  attribute name { 'mu' },
	  element type {
		attribute name {'external'},
		vector_K,
		variable_T,
		variable_V,
		comment
	  },
	   ##  Optional sympy symbol for use with this function
	  generic_symbol?,
	  reference?,
          comment
       }

)

function_A =
(
	## reserved symbol for vector of affinities A.
	## Size will be set by parser from number of reactions J
		element function {
	  attribute name { 'A' },
	  element type {
		attribute name {'external'},
		vector_J,
		variable_T,
		variable_P,
		variable_C,
		comment
	  },
	  ##   sympy expression that either evaluates to the function of interest (explicit)
	  ##  or is an implicit function of the function that evaluates to zero for the correct value (requires a root solver)
	   ##  Optional sympy symbol for use with this function, otherwise will default to the function name
	  generic_symbol?,
	  reference?,
          comment
       }

)

function_rho =
(
	## reserved symbol for vector of densities of products for each reaction.
	## Size will be set by parser from number of reactions J
		element function {
	  attribute name { 'rho' },
	  element type {
		attribute name {'external'},
		vector_J,
		variable_T,
		variable_P,
		comment
	  },
	  ##   sympy expression that either evaluates to the function of interest (explicit)
	  ##  or is an implicit function of the function that evaluates to zero for the correct value (requires a root solver)
	   ##  Optional sympy symbol for use with this function, otherwise will default to the function name
	  generic_symbol?,
	  reference?,
          comment
       }

)
