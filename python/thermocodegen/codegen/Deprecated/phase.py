# -*- coding: utf-8 -*-
import sympy as sym

class Phase(object):
    '''
    Class describing sympy generated functions for phases.
    '''

    def __init__(self, phase_dict, printer):
        self.phase_dict = phase_dict
        self.printer = printer
        self.endmembers = self.phase_dict['endmembers']

        # Gibbs free energy
        self.G = self.phase_dict['potentials'][-1]['expression']

        # Total number of components
        self.K = self.phase_dict['K']

        # Set variables
        self.T = [var['symbol'] for var in self.phase_dict['model']['variables'] if var['name'] == 'T'][0]
        self.P = [var['symbol'] for var in self.phase_dict['model']['variables'] if var['name'] == 'P'][0]
        self.n = [var['symbol'] for var in self.phase_dict['model']['variables'] if var['name'] == 'n'][0]

        # Total number of moles in the solution
        self.nT = (sym.ones(1,self.K)*self.n)[0]

        # Vector of mole fractions of components in the system
        self.x = sym.Matrix([sym.symbols('x[' + str(i) + ']') for i in range(self.K)])

        # Vector of molar masses
        self.M = sym.Matrix([sym.symbols('M[' + str(i) + ']') for i in range(self.K)])

        # Vector of concentrations of components in the system
        self.c = sym.Matrix([sym.symbols('c[' + str(i) + ']') for i in range(self.K)])

        # Set vector of chemical potentials mu
        self.set_mu()

        # Vector of derivatives of mu w.r.t to T and P
        self.dmu_dP = [self.mu[i].diff(self.P) for i in range(self.K)]
        self.dmu_dT = [self.mu[i].diff(self.T) for i in range(self.K)]

        # Derivatives of G
        self.dGdT = self.G.diff(self.T)
        self.dGdP = self.G.diff(self.P)
        self.d2GdT2 = self.dGdT.diff(self.T)
        self.d2GdTdP = self.dGdT.diff(self.P)
        self.d2GdP2 = self.dGdP.diff(self.P)

    def endmember_list(self):
        """..."""
        endmember_str = "std::vector<std::shared_ptr<EndMember> > {"
        endmember_list = [ "std::make_shared<{endmember}>()".format(endmember=e) for e in self.endmembers ]
        endmember_str += ",".join(endmember_list) +" }"
        return endmember_str

    def dx_dxi(self):
        """Calculate derivative of mole fraction vector x with respect to component x[i]."""
        _dx_dxi = [sym.symbols('_dx_dxi[' + str(j) + ']' ) for j in range(self.K)]
        return _dx_dxi

    def mu_molar(self):
        """Vector of chemical potentials for each component as a function of moles n."""
        _mu_mol = [self.G.diff(self.n[i]) for i in range(self.K)]
        return _mu_mol

    def set_mu(self):
        """Vector of chemical potentials for each component as a function of molar fractions x."""
        _mu = []
        n_total = sym.symbols('nT')
        xk = [(self.n[i], n_total*self.x[i]) for i in range(self.K)]
        _mu_molar = self.mu_molar()
        for i in range(self.K):
            mu_i = (_mu_molar[i].subs([(self.nT, n_total)]))
            mu_i = (mu_i.subs(xk)).simplify().subs([(sum(self.x), 1)])
            _mu.append(mu_i)

        self.mu = _mu

    def dmu_dxi(self):
        """Derivatives of chemical potentials with respect to the ith molar fraction x[i]."""
        _dmu_dxi = []
        _mu, _dx_dxi = self.mu, self.dx_dxi()
        for k in range(self.K):
            _dmu_dxi_k = sum([_mu[k].diff(self.x[j])*_dx_dxi[j] for j in range(self.K)])
            _dmu_dxi.append(_dmu_dxi_k)

        return _dmu_dxi

    def d2mu_dPdxi(self):
        """Derivative of mu with respect to P and x[i]."""
        _d2mu_dPdxi = []
        _dmu_dP, _dx_dxi = self.dmu_dP, self.dx_dxi()
        for k in range(self.K):
            _d2mu_dPdxi_k = sum([_dmu_dP[k].diff(self.x[j])*_dx_dxi[j] for j in range(self.K)])
            _d2mu_dPdxi.append(_d2mu_dPdxi_k)

        return _d2mu_dPdxi


    def rho(self):
        """Mean density rho(T, P, c)."""
        invrho = 0
        for i in range(self.K):
            invrho += self.c[i]*self.dmu_dP[i]/self.M[i]

        return 1./invrho

    def drho_dT(self):
        """Derivative of rho w.r.t to temperature."""
        return self.rho().diff(self.T)

    def drho_dP(self):
        """Derivative of rho w.r.t to pressure."""
        return self.rho().diff(self.P)

    def scalar_functions(self):
        """
        Dictionary of code printed scalar functions.
        """
        scalar_funcs = {"G": self.G,
                        "dGdT": self.dGdT,
                        "dGdP": self.dGdP,
                        "d2GdT2": self.d2GdT2,
                        "d2GdTdP": self.d2GdTdP,
                        "d2GdP2": self.d2GdP2,
                        "rho": self.rho(),
                        "drho_dT": self.drho_dT(),
                        "drho_dP": self.drho_dP()}

        # Code print functions
        scalar_funcs.update({k: self.printer.doprint(v) for k, v in scalar_funcs.items()})

        return scalar_funcs

    def vector_functions(self):
        """
        Dictionary of code printed vector functions.
        """
        vector_funcs = {"mu": self.mu,
                        "dmu_dT": self.dmu_dT,
                        "dmu_dP": self.dmu_dP,
                        "dmu_dxi": self.dmu_dxi(),
                        "d2mu_dPdxi": self.d2mu_dPdxi()}

        # Code print functions
        f = lambda x: """{f_i}""".format(f_i=self.printer.doprint(x))
        vector_funcs.update({k: ',\n  '.join(list(map(f, v))) for k, v in vector_funcs.items()})

        return vector_funcs

    def phase_functions(self):
        """
        Returns a dictionary of functions to be substituted into templates.
        """

        # Construct dictionary to be substituted into C++ template
        ph_funcs = {"phasename": self.phase_dict['name'],
                    "phasename_uppercase": self.phase_dict['name'].upper(),
                    "name": self.phase_dict['name'],
                    "endmembers": self.endmember_list()}

        # Add functions to dictionary
        ph_funcs.update(self.scalar_functions())
        ph_funcs.update(self.vector_functions())

        return ph_funcs

    def calibration_functions(self, active_param):
        """
        Returns a dictionary describing derivatives of functions in 'phase_functions'
        with respect to active parameters.
        """
        raise OSError('Calibration functions not yet implemented')
