# -*- coding: utf-8 -*-
"""Script for calculating stoichiometric coefficients"""

import numpy as np
import molmass
import re
import sympy as sym

def from_berman(formula):
    """
    converts a berman style formula to standard form (with some 1s).
    should not effect a formula in standard form (I think)

    :param formula:  Chemical formula SI(1)O(2)  or SiO2
    :return:  standform Si1O2 (doesn't drop the 1)
    """

    formula_standard = formula.title().replace('(', '').replace(')', '')
    return formula_standard


def chemical_composition_matrix(endmembers):
    """
    Calculates stoichiometry matrix of endembers in local atomic basis

    :param endmembers: list
        list of formula strings for endmembers in a reaction
    :return M: ndarray
        numpy integer array of atom counts in endmembers
    """

    # Split by element symbol and number of atoms, e.g. H20 -> [('H',2),('O',1)]
    # FIXME:  this regular expression assumes standard form of equation (need to check for berman style and convert)
    formula_split = [re.findall('[A-Z][^A-Z]*', from_berman(em))  for em in endmembers ]
    for i in range(len(formula_split)):
        elem_count = []
        for elem in formula_split[i]:
            split_by_count = re.split('(\d+)', elem)
            if len(split_by_count) == 1:
                elem_count.append((split_by_count[0], 1))
            else:
                elem_count.append((split_by_count[0], int(split_by_count[1])))

        formula_split[i] = elem_count

    # Construct chemical-composition matrix M
    M = {}
    for i in range(len(formula_split)):
        for elem_count in formula_split[i]:
            if elem_count[0] not in M:
                M[elem_count[0]] = np.zeros(len(endmembers))

    for i in range(len(formula_split)):
        for elem_count in formula_split[i]:
            M[elem_count[0]][i] = elem_count[1]

    M = np.vstack(tuple(M.values())).astype(int)

    return M


def stoichiometric_coeffs(endmembers, mass_weighted=False):
    """
    Calculates reaction stoichiometry by computing the basis for the nullspace
    of the chemical-composition matrix M using sympy.

    Note: only accepts systems with nullity = 1 (nullity > 1 implies any linear
    combination of reactions balances the system and nullity = 0 implies no
    balanced reaction is possible).

    :param endmembers: list
        list of formula strings for endmembers in a reaction
    :param mass_weighted: bool
        if True return  mass-weighted stoichiometric coefficients
        if False return normalized molar stoichiometric coefficients for 1 mol of reactant
    :return nu: numpy array (float64)
        stoichiometry coefficients

    """

    # Assemble chemical-composition matrix
    M = chemical_composition_matrix(endmembers)

    # use sympy to calculate the nullspace symbolically before converting to floats
    M = sym.Matrix(M)
    nullity = M.shape[1] - M.rank()
    if nullity != 1:
        print('Can\'t find dimension 1 nullspace')
        raise ValueError(
            "dimension of nullspace should be equal to 1, got {}".format(nullity))
    else:
        ns = M.nullspace()[0]
        # Make sure that sign of reactants is negative
        if np.sign(ns[0]) > 0:
            ns = -ns
        # Scale nullspace vector so that the sum of reactant coeffs equals -1
        sum_react = 0
        for n in ns:
            if n < 0:
                sum_react += n
        ns /= abs(sum_react)
        # convert to numpy float array
        nu = np.array(ns).astype(np.float64).flatten()
        if mass_weighted:
            #FIXME: using molmass to determine molecular weights which has slightly higher precision
            # than ThermoEngine elements database, but simple comparisons agree to floating point error
            Mass = np.array([molmass.Formula(from_berman(em)).mass for em in endmembers])
            nu *= Mass
            Mj = np.sum(nu[nu > 0.])
            nu /= Mj
    return nu


if __name__ == "__main__":
    # some quick debugging checks
    em = ['MG(1)SI(1)O(3)', 'MG(2)SI(1)O(4)', 'SI(2)O(4)']

    M = chemical_composition_matrix(em)
    nu = stoichiometric_coeffs(em)

    print('M=\n{}'.format(M))
    print()
    print('nu={}'.format(nu))

    f0 = from_berman(em[0])
    f1 = from_berman(f0)
    print()
    print(em[0],f0,f1)