version_release = 1
version_major   = 1
version_minor   = 0
version_patch   = 0
version_short   = ".".join([repr(version_major),repr(version_minor),repr(version_patch)])
version         = version_short if version_release else version_short+'+'
