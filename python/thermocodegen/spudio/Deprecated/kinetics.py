#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
   reaction:

    collection of python routines for parsing Spud .kxml files
    to (and eventually from) nested python dictionaries for use in general
    code-generation schemes
"""


# import libspud for reading SPuD xml files
import libspud

from . import endmember as em
from . import phase as ph
from . import parser
import molmass as mm

import os
import urllib
from glob import glob
import tarfile
import tempfile

def parse_reaction_name(file):
    """reads a SPud derived reaction description file and
    extracts the model name
    """
    if file is not None:
        libspud.load_options(file)
    name =  libspud.get_option('/name/name')
    if file is not None:
        libspud.clear_options()
    return name

def parse_database_name(file):
    """reads a SPud derived reaction description file and
    extracts the phase name and abbrev
    """
    if file is not None:
        libspud.load_options(file)
    database_url =  libspud.get_option('/database')
    if file is not None:
        libspud.clear_options()
    return database_url

def parse_phase_names():
    """ reads a SPuD derive rxn description file and returns a list of
    phase  names
    """

    N_phases =  libspud.option_count('/phase')
    phases = []
    for n in range(N_phases):
        name  = libspud.get_option('/phase[{}]/name'.format(n))
        phases.append(name)
    return phases

def parse_reactions(file,db_dict):
    """ reads reactants, and products  for a reaction description
    starting at path and  returns a reaction dictionary

    Input:
        path  Spud option path to start of reaction option file
        db_dict:  dictionary of parsers from underlying thermo database

        Tries validating reaction pairs as they are read

    returns reaction dictionary
    """

    # set up some validation dictionaries from db_dict
    # legal phase names from the database
    phase_set = set(db_dict['phases'].keys())

    # dictionary of phase and endmember pair
    p_dict = {p.name:p.endmembers for p in db_dict['phases'].values()}


    #clear options for safety
    libspud.clear_options()
    libspud.load_options(file)

    J_reactions = libspud.option_count('reaction')
    reactions = []
    for j in range(J_reactions):
        reaction = {}
        path = 'reaction[{}]'.format(j)
        reaction['name'] = libspud.get_option(path+"/name")

        # get reactants
        N = libspud.option_count(path+'/reactant')
        reactants=[]
        for i in range (N):
            phase = libspud.get_option(path+"/reactant[{}]/phase".format(i))
            endmember = libspud.get_option(path+"/reactant[{}]/endmember".format(i))
            if phase not in phase_set or endmember not in  p_dict[phase]:
                print('\n  Error in reaction:{} Bad reactant: ({}, {})'.
                      format(reaction['name'],phase,endmember))
                reactants.append((None,None,None))
            else:
                formula = db_dict['endmembers'][endmember].formula
                reactants.append((formula,phase,endmember))
        reaction['reactants']= reactants

        N = libspud.option_count(path+'/product')
        products=[]
        for i in range (N):
            phase = libspud.get_option(path+"/product[{}]/phase".format(i))
            endmember = libspud.get_option(path+"/product[{}]/endmember".format(i))
            if phase not in phase_set or endmember not in  p_dict[phase]:
                print('\n  Error in reaction:{} Bad product: ({}, {})'.
                      format(reaction['name'],phase,endmember))
                products.append((None,None,None))
            else:
                formula = db_dict['endmembers'][endmember].formula
                products.append((formula,phase,endmember))
        reaction['products'] = products

        reactions.append(reaction)

    libspud.clear_options()
    return reactions

### utilities for interacting with databases of endmembers and phases

def get_files_parsers(topdir,ext):
    """ returns a list of spud parsers in a nested directory starting at topdir that match the extension ext

        Usage: endmembers = get_parsers(os.getcwd(),'.emml')

        should return an empty list if no matching files

    """
    parsers = []
    filenames = []
    for dirname, dirnames, files in os.walk(topdir, topdown=True):
        builddirs = [ d for d in dirnames if d.endswith('.build')]
        for d in builddirs:
            dirnames.remove(d)
        for file in files:
          if file.endswith(ext):
              ffull = dirname+'/'+file
              filenames.append(ffull)
              parsers.append(parser.Parser(ffull))

    return filenames,parsers

def get_parsers_database(file):
    """ downloads a temporary database tarball from kxml file and a dict containing lists of  endmember and phase parsers """
    database_url = parse_database_name(file)

    # open the database URL
    try:
        database_stream = urllib.request.urlopen(database_url)
    except ValueError as e:
        print(e)
        exit(keep_kernel=True)

    # FIXME: need to be more clever about valid tarfiles that aren't gzipped
    # open a temporary directory and dump the tarball in it
    with tempfile.TemporaryDirectory() as tmpdirname:
        #print('created temporary directory', tmpdirname)
        tar = tarfile.open(fileobj=database_stream, mode="r|gz")
        tar.extractall(path=tmpdirname)
        tar.close()
        #  get parsers for endmembers and phases
        efiles,endmembers = get_files_parsers(tmpdirname,'.emml')
        pfiles,phases    = get_files_parsers(tmpdirname,'.phml')

    # return a dictionary of lists of endmembers and phases.
    edict = {e.name:e for e in endmembers}
    pdict = {p.name:p for p in phases}
    return {'endmembers':edict, 'phases':pdict}




def get_phase_dict(phase,db_dict):
    """ return a dictionary of information from each phase file

        db_dict: dictionary of endmember and phase parsers from thermodyanmic
        database

        FIXME:  needs better error checking but should raise a KeyError if
        the phase is not in the database
    """

    phase_dict = {}
    phase_dict['name'] = db_dict['phases'][phase].name
    phase_dict['endmembers'] = db_dict['phases'][phase].endmembers
    phase_dict['formula'] =  [ db_dict['endmembers'][e].formula for e in phase_dict['endmembers'] ]
    phase_dict['Mik'] =  [ mm.Formula(formula).mass for formula in phase_dict['formula'] ]
    return phase_dict



def init_dictionary():
    """Initializes dictionary with reaction name, """
    rx_dict = {}
    rx_dict['name'] = libspud.get_option('name/name')
    rx_dict['database'] = libspud.get_option('database')
    return rx_dict



def read_kinetics(file):
    """Reads a SPuD derived reaction description file (.kxml file)
    and returns a nested python dictionary of data, phases and
    endmembers for

    On input file is the name of an .rxml files
    On output, the function returns a python dictionary

    Uses the libspud to read spud files

    Parameters
    ----------
    file : string containing name of endmember markup language file (.emml)



Returns
    -------
    dict : Nested dictionary with fields
        ['name']
        ['filename'] name of kxml file
        ['database'] url of thermodynamic database
        ['phase_names'] list of phase names from kxml file
        ['phases'] [ list of  phase dictionaries ]
        ['reactions'] [ list of reaction dictionaries ]

    phase dictionary has structure
        phase['name']
        phase['endmembers'] [ list of endmembers ]
        phase['formulas'] [ list of endmember formulas]
        phase['Mik'] [ list of molecular masses for each endmember]

    reaction dictionary  has structure
        ['name']
        ['reactants'] [list of reactant tuples]
        ['products'] [list of product tuples ] (but apparently only need the sum of them)

    each tuple consists of (formula,phase,endmember) where formula is the endmember formula


    Examples
    --------

    >>> dict = spudio.read_kinetics('file.kxml')
    >>> print (dict)

    """

    #make file paths unique
    dirname,basename = os.path.split(os.path.abspath(file))

    # load .kxml files (will raise exception of no appropriate file)
    libspud.load_options(file)

    #initialize dictionary with name, formula, model and any global parameters
    rx_dict = init_dictionary()

    # add filename to dictionary
    rx_dict['filename'] = basename
    rx_dict['phase_names'] = parse_phase_names()

    #clear the options file for safety
    libspud.clear_options()

    # get database dictionary of parsers
    db = get_parsers_database(file)
    rx_dict['phases'] = [ get_phase_dict(p,db) for p in rx_dict['phase_names'] ]

    # get  reaction dictionaries
    rx_dict['reactions'] = parse_reactions(file,db)
    return rx_dict



if __name__ == "__main__":

    #valid_dict = get_valid_reactions()

    kx_dict = read_kinetics('fo-siO2-binary.kxml')
    print(kx_dict)


    #dictD = read_endmember('test_GD.emml')
