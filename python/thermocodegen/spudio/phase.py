#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
   phase:

	collection of python routines for reading and writing Spud .phml files
	to (and eventually from) nested python dictionaries for use in general code-generation
	schemes
"""
from collections import Counter, OrderedDict
from sympy.core.compatibility import exec_

# import libspud for reading SPuD xml files
import libspud

# import sympy for symbolic preprocessing
import sympy as sym
import ast

try:
	from .optionparsers import *
except ModuleNotFoundError:
	from optionparsers import *

import os, sys


def parse_phase_abbrev(file):
	"""reads a SPud derived phase description file and
	extracts the phase name and abbrev
	"""
	libspud.load_options(file)
	abbrev = libspud.get_option('name/abbrev/name')
	libspud.clear_options()
	return abbrev


def parse_model_name(file):
	"""reads a SPud derived phase description file and just extracts the
	free energy model name
	"""
	libspud.load_options(file)
	name = libspud.get_option('/free_energy_model/name')
	libspud.clear_options()
	return name


def parse_endmembers(file):
	""" reads a SPuD derive phase descrition file and returns a list of
	endmember names
	"""
	libspud.load_options(file)
	N_endmembers = libspud.option_count('/endmembers/endmember_name')
	endmembers = []
	for n in range(N_endmembers):
		name = libspud.get_option('/endmembers/endmember_name[{}]/name'.format(n))
		endmembers.append(name)
	libspud.clear_options()
	return endmembers


def list_available_parameters(file):
	""" utility function to list out all parameter names and lengths of vector
	valued parameters
	"""

	parameters = parse_all_parameters(file)
	scalars = [param for param in parameters if param['rank'] == 'Scalar']
	vectors = [param for param in parameters if param['rank'] == 'Vector']

	print('Scalar Parameters')
	fmt = '    {:<10.10s} {:13.6e} {:<10.10s}'
	for scalar in scalars:
		print(fmt.format(scalar['name'], scalar['value'], scalar['units']))
	print('Vector Parameters')
	for vector in vectors:
		for i in range(vector['size']):
			print('    {:<0.10s}[{}] {:19.6e} {:<15.15s}'.
				  format(vector['name'], i, vector['value'][i], vector['units'][i]))


def init_dictionary():
	"""Initializes dictionary with phase name, formula and models"""
	dict = OrderedDict()
	dict['name'] = libspud.get_option('name/name')
	try:
		dict['abbrev'] = libspud.get_option('name/abbrev/name')
	except libspud.SpudKeyError:
		dict['abbrev'] = None

	# parse formula dictionary
	strings = ['formula_string', 'conversion_string', 'test_string']
	dict['formula'] = {ss: libspud.get_option('/formula/{}'.format(ss)) for ss in strings}
	dict['K'] = libspud.option_count('endmembers/endmember_name')
	for s in strings[1:]:
		dict['formula'][s] = ast.literal_eval(dict['formula'][s])

	# parse reference
	try:
		dict['reference'] = libspud.get_option('/reference')
	except libspud.SpudKeyError:
		dict['reference'] = None

	# parse endmember names
	endmembers = []
	for i in range(dict['K']):
		endmembers.append(libspud.get_option('/endmembers/endmember_name[{}]/name'.format(i)))
	dict['endmembers'] = endmembers
	dict['local_dict'] = {}
	dict['global_dict'] = {}
	dict['global_dict']["sym"] = sym
	return dict


def parse_free_energy_model(dict):
	""" parse the free_energy model part of the spud file and
	return a dictionary including name, lists of model variables and Parameters

	input:
		dict: current phase dictionary
	"""

	# local dictionary for
	local_dict = dict['local_dict']
	global_dict = dict['global_dict']

	# nested dictionary for model
	model = {}
	model['name'] = libspud.get_option('free_energy_model/name')

	# collect list of variables
	str = 'free_energy_model/variable'
	nvar = libspud.option_count(str)
	variables = []
	for i in range(nvar):
		var = parse_variable(str + '[{}]'.format(i), dict)
		##FIXED in parse_variable -- now returns sym.Matrix rather than sym.MatrixSymbol
		# if(var['rank'] != 'Scalar'):
		#    var['symbol'] = sym.Matrix(var['symbol'])
		variables.append(var)

	model['variables'] = variables

	# collect list of variables
	str = 'free_energy_model/parameter'
	nvar = libspud.option_count(str)
	params = []
	for i in range(nvar):
		params.append(parse_parameter(str + '[{}]'.format(i)))

	model['parameters'] = params

	# collect list of functions
	# horrible hack...constructing intermediate dictionary to include parameters
	tmp_dict = dict.copy()
	tmp_dict['model'] = model
	str = 'free_energy_model/function'
	nfunc = libspud.option_count(str)
	funcs = []
	for i in range(nfunc):
		funcs.append(parse_function(str + '[{}]'.format(i), tmp_dict, global_dict))

	model['functions'] = funcs
	return model


def parse_potential(path, dict, global_dict, verbose=True, simplify=False):
	"""Reads a single potential component of an endmember options tree and returns
	single sympy expression with all non-active parameters and functions
	substituted in.

	Parameters
	----------
	path: python string top path of an endmember potential option tree
	dict:  Phase dictionary containing lists of variables, parameters and subs_dict
	verbose: optional flag for printing progress
	simplify: optional boolean to attempt simplification of expression on initial read
		defaults to false.  However,  if parameter substitution is required, simplification is always called
		on the final expression.

	Output
	--------
	dictionary of optimized potential with fields
	potential['name']: python string with name of potential
	potential['expression']: optimized sympy expression for potential

	Examples
	--------
	potential_dict  = parse_potential('potential[0]',subs_dict)
	"""

	subs_dict = dict['subs_dict']

	potential = {}
	potential['name'] = libspud.get_option(path + '/name')
	potential['symbol'] = sym.Symbol(potential['name'])

	if verbose:
		print('Processing potential {}...'.format(potential['name']), end='')

	# sequentially evaluate explicit functions with substitution and build up
	# expression
	n_func = libspud.option_count(path + '/function')
	explicit_functions = []
	functions = []
	for i in range(n_func):
		str = path + '/function[{}]'.format(i)
		function = parse_function(str, dict, global_dict)
		# print('\tProcessed {} local function {}'.format(function['type'],function['name']))
		# print(function)
		if function['type'] == 'explicit':
			# add the explicit function symbol to the subs dictionary
			global_dict.update({function['name']: function['expression']})
			explicit_functions.append(function)
		else:
			functions.append(function)
	potential['functions'] = functions
	potential['explicit_functions'] = explicit_functions

	expression = parse_expression(path, potential['name'], global_dict)
	if simplify:
		if verbose:
			print('Simplifying Expression...', end='')
		expression = expression.simplify()
	if len(subs_dict) > 0:
		if verbose:
			print('Substituting parameters and Simplifying...', end='')
		expression = expression.subs(subs_dict).simplify()

	potential['expression'] = expression
	global_dict.update({potential['name']: potential['expression']})

	if verbose:
		print('done')

	return potential


def parse_implicit_function(path, dict, global_dict):
	"""Reads a single implicit_function component of an endmember options tree and returns
	a dictionary containing components of an implicit function

	Parameters
	----------
	path: str: spud path to the top of an implicit function   option tree
	global_dict:  Endmember dictionary containing lists of variables, parameters and subs_dict for exec

	Output
	--------
	dictionary of implicit_function  with fields
	implicit_function['name']: python string with name of implicit function
	implicit_function['variable']:  sympy.symbol of implicit function
	implicit_function['residual']:  sympy expression for F, s.t. F(name)=0 is the solution
	implicit_function['guess']: initial guess for newton solver

	Examples
	--------
	implicit_function  = parse_implicit_function(path,dict,global_dict)
	"""

	implicit_function = {}
	name = libspud.get_option(path + '/name')
	implicit_function['name'] = name
	implicit_function['variable'] = parse_expression(path, name, global_dict)
	global_dict.update({name: sym.Function(name)})

	spud_path = '{}/{}'.format(path, 'residual')
	name = libspud.get_option(spud_path + '/name')
	implicit_function['residual'] = parse_expression(spud_path, name, global_dict)

	spud_path = '{}/{}'.format(path, 'initial_guess')
	name = libspud.get_option(spud_path + '/name')
	implicit_function['guess'] = parse_expression(spud_path, name, global_dict)

	return implicit_function


def parse_ordering_function(path, dict, global_dict):
	"""Reads a single ordering_functions component of an phase options tree and returns
	a dictionary containing components of an ordering function

	Parameters
	----------
	path: str: spud path to the top of an ordering functions   option tree
	global_dict:  Phase dictionary containing lists of variables, parameters and subs_dict for exec

	Output
	--------
	dictionary of ordering_functions  with fields
	implicit_function['name']: python string with name of implicit function
	implicit_function['variable']:  dictionary of ordering variable (name, variables, symbol, units)
	implicit_function['residual']:  sympy expression for F, s.t. F(name)=0 is the solution
	implicit_function['guess']: initial guess for newton solver
	implicit_function['bounds']: solution bounds symbol


	Examples
	--------
	ordering_functions  = parse_ordering_function(path,dict,global_dict)
	"""

	ordering_function = {}
	name = libspud.get_option(path + '/name')
	ordering_function['name'] = name
	var_dict = parse_variable(path + '/variable')
	ordering_function['variable'] = var_dict
	global_dict.update({var_dict['name']: var_dict['symbol']})

	for field in ['residual', 'guess', 'bounds']:
		spud_path = '{}/{}'.format(path, field)
		name = libspud.get_option(spud_path + '/name')
		ordering_function[field] = parse_expression(spud_path, name, global_dict)

	return ordering_function


def read_phase(file, active=None, flatten_vectors=True, flatten_potentials=True):
	"""
		Reads a SPuD derived phase description file (.phml file)
	and returns a nested python dictionary of data, parameters and
	sympy expressions that can be passed on to various code-generation
	schemes.

	On input file is the name of an .phml files
	On output, the function returns a python dictionary

	Uses the libspud to read spud files

	Parameters
	----------
	file : string containing name of endmember markup language file (.emml)
	active:  Flag or dictionary describing the active set of parameters  thar
			will be compiled into the eventual calibration code as variables
			if active == None:  all parameter values are substituted in as numbers
			if active == 'All',  all parameters are assumed to be active
			otherwise active is a list of parameter names to be made active
	flatten_vectors: boolean to determine how vector valued parameters are returned,
		e.g if p as a vector valued parameter with length n,
		flatten_vectors = True returns n parameters named p_i for i in range(n)
	flatten_potentials: boolean.  If true just returns a single potential that
		is the sum of all listed potentials


	Returns
	-------
	dict : Nested dictionary with fields
		['name']
			['abbrev']
		['formula']
			['formula_string']
			['conversion_string']
			['test_string]
		['reference']
		['endmembers']
		['model']
			['name'] ('Gibss, Helmholtz otherwise')
			['variables'] [list of Model variables as extended parameters]
			['functions'] [list of global function symbols e.g. \mu]
		['parameters'] [ list of all parameters]
		['active_params'] [ names of active parameters (or "All" or None)]
		['subs_dict'][ dictionary for substituting values for non-active parameters]
		[functions]
			['implicit_functions']
			['ordering_functions']
		[potentials] [ list of potential dictionaries]

	potentialdictionary  has structure
		['name']
		['symbol'] sympy symbol for potential
		['Implicit Functions'] [list of structures for constructing implicit functions]
		['External Functions'] [list of defined External function symbols ]
		['Expression'] sympy expression with all non-active parameters subbed in

	if flatten_potentials=True.  Only 1 potential is returned that is the sum of all the sub-potentials

	Examples
	--------

	> dict = spudio.read_phase('file.phml')
	> print (dict)
	> dict = spudio.read_phase('file.phml',active="All")
	> print (dict)
	> params = ['H0', 'V0', 'S0']
	> dict = spudio.read_phase('file.phml',active=params)
	"""

	assert (isinstance(flatten_potentials, bool))
	assert (isinstance(flatten_vectors, bool))
	# read all parameters
	parameters = parse_all_parameters(file)

	# load .phml files (will raise exception of no appropriate file)
	libspud.load_options(file)

	# initialize dictionary with name, formula, model and any global parameters
	dict = init_dictionary()

	# add filename to dictionary
	dict['filename'] = (file.split('/')[-1]).split('.')[0]

	# attach all parameters
	dict['parameters'] = parameters

	# sort out active and non-active parameters
	s, a = set_subs_parameters(parameters, active)
	dict['active_parameters'] = a
	dict['subs_dict'] = s

	# FIXME: this is klutzy,  I'm extracting global variables from the free energy model
	# and then discarding them because I've already extracted all the parameters
	model = parse_free_energy_model(dict)
	model.pop('parameters')
	dict['model'] = model

	# set up a global dictionary of symbols for resolving symbols in exec
	global_dict = {}
	global_dict["sym"] = sym
	global_dict.update(sym.__dict__)
	global_dict.update({parameter['name']: parameter['symbol'] for parameter in parameters})
	global_dict.update({variable['name']: variable['symbol'] for variable in dict['model']['variables']})
	global_dict.update({function['name']: function['symbol'] for function in dict['model']['functions']})

	n_pot = libspud.option_count('potential')
	potentials = []
	for i in range(n_pot):
		potential = parse_potential('potential[{}]'.format(i), dict, global_dict)
		potentials.append(potential)

	if flatten_potentials:
		p_tot = {}
		if dict['model']['name'] == 'Gibbs':
			p_tot['name'] = 'G'
		elif dict['model']['name'] == 'Helmholtz':
			p_tot['name'] = 'A'
		else:
			p_tot['name'] = dict['model']['name']
			p_tot['symbol'] = sym.Symbol(p_tot[name])

		p_tot['functions'] = []
		expression = sym.S.Zero

		for potential in potentials:
			p_tot['functions'].append(potential['functions'])
			expression += potential['expression']

		p_tot['expression'] = expression.simplify()

		potentials.append(p_tot)
	# print('Processed flattened potential {}'.format(p_tot['name']))

	dict['potentials'] = potentials

	#  parse implicit functions
	dict['implicit_functions'] = []
	spud_path = 'functions/implicit_function'
	if libspud.have_option(spud_path):
		print('processing implicit functions')
		# read implicit functions
		n_ifunc = libspud.option_count(spud_path)
		for i in range(n_ifunc):
			spud_path = 'functions/implicit_function[{}]'.format(i)
			ifunc = parse_implicit_function(spud_path, dict, global_dict)
			dict['implicit_functions'].append(ifunc)

	# get ordering function if they exist
	spud_path = '/functions/ordering_function'
	dict['ordering_functions'] = None
	if libspud.have_option(spud_path):
		dict['ordering_functions'] = parse_ordering_function(spud_path, dict, global_dict)

	libspud.clear_options()
	return dict


def write_phase(model_dict, filename=None, path=None):
	""" Writes a SPuD derived phase description file (.phml file)
	from a nested python dictionary of data, parameters and
	sympy expressions that can be passed on to various code-generation
	schemes.

	On input:  a complete model_dictionary for endmembers
	On output, a .phml file

	Uses the libspud to write spud files

	Parameters
	----------
	model_dict : Python dictionary describing endmember model
	filename:  optional name of file. Will default to model_dict['name'].emml
	path:      optional directory path to write file (otherwise CWD)


	Returns
	-------
	Nothing

	Examples
	--------

	>>>  spudio.write_phase(model_dict)
	"""
	assert (isinstance(model_dict, dict))

	md = model_dict.copy()

	# Path to templates
	tcg_home = os.environ.get('THERMOCODEGEN_HOME')
	if tcg_home is None:
		raise OSError('Environment variable THERMOCODEGEN_HOME is not set')

	template_dir = tcg_home + '/share/thermocodegen/templates/spud'

	## FIXME:  should have better error checking to see if model_dict is valid
	# Check model type and load appropriate template directory

	try:
		libspud.clear_options()
	except:
		pass

	try:
		model_name = md['model']['name']
		spud_template = '{}/PhaseModel_{}.phml'.format(template_dir, model_name)
		libspud.load_options(spud_template)
	# libspud.print_options()
	except KeyError as err:
		print('Only supports Energy models, Gibbs and Helmholtz', err)
		raise KeyError

	# set pre-assigned values in template
	write_attribute_name('/name', md['name'])
	write_attribute_name('/name/abbrev', md['abbrev'])
	write_str('/formula/formula_string', md['formula']['formula_string'])
	write_str_list('/formula/conversion_string', md['formula']['conversion_string'])
	write_str_list('/formula/test_string', md['formula']['test_string'])
	write_str('/reference', md['reference'])

	# write endmember names
	write_endmember_names(md['endmembers'])

	# set vector valued sympy expressions
	write_model_symbols(md['model'])

	# now populate global parameters
	for p in md['parameters']:
		spud_path = '/parameters/parameter::{}'.format(p['name'])
		write_parameter(spud_path, p)

	# write out implicit functions if they exist
	for ifunc in md['implicit_functions']:
		spud_path = '/functions/implicit_function::{}'.format(ifunc['name'])
		write_implicit_function(spud_path, ifunc)

	# write out ordering functions if they exist
	if md['ordering_functions'] is not None:
		ofunc = md['ordering_functions']
		spud_path = '/functions/ordering_function::{}'.format(ofunc['name'])
		write_ordering_function(spud_path, ofunc)

	# write out external functions if they exist
	# for efunc in md['external_functions']:
	#     spud_path = '/functions/external_function::{}'.format(efunc['name'])
	#     write_explicit_functions(spud_path, efunc)

	# write out Potentials
	for pot in md['potentials']:
		spud_path = '/potential::{}'.format(pot['name'])
		write_potential(spud_path, pot)

	# Output the option files
	if filename is None:
		filename = md['name'] + '.phml'

	if path is not None:
		filename = "{}/{}".format(path, filename)

	# libspud.print_options()
	libspud.write_options(filename)
	libspud.clear_options()

	return None


def write_model_symbols(model):
	vars = ['n']
	for v in model['variables']:
		if v['name'] in vars:
			spud_path = '/free_energy_model::Gibbs/variable::{}/symbol'.format(v['name'])
			write_str(spud_path, str(v['symbol']))
	# set mu expression
	func = model['function']
	spud_path = '/free_energy_model::Gibbs/function::{}/symbol'.format(func['name'])
	write_str(spud_path, str(func['symbol']))


def write_endmember_names(endmembers):
	for em in endmembers:
		try:
			spud_path = '/endmembers/endmember_name::{}'.format(em)
			libspud.add_option(spud_path)
		except  libspud.SpudNewKeyWarning:
			pass


def write_ordering_function(spud_path, ofunc):
	try:
		libspud.add_option(spud_path)
	except libspud.SpudNewKeyWarning as err:
		pass

	# add variable
	path = '{}/variable::{}'.format(spud_path, ofunc['variable']['name'])
	# expression = '{} = sym.Function(\'{}\'){}'.format(ofunc['name'], ofunc['name'], str(ofunc['variable'].args))
	write_variable(path, ofunc['variable'])

	# add residual
	name = ofunc['name']
	path = '{}/residual::{}'.format(spud_path, name)
	try:
		libspud.add_option(path)
	except libspud.SpudNewKeyWarning as err:
		pass
	expression = '{} = (\n{}\n)'.format(name, textwrap.fill(str(ofunc['residual'])))
	write_expression(path, expression)

	# add initial_guess
	name = 'guess'
	path = '{}/guess::{}'.format(spud_path, name)
	try:
		libspud.add_option(path)
	except libspud.SpudNewKeyWarning as err:
		pass
	expression = '{} = {} '.format(name, str(ofunc['guess']))
	write_expression(path, expression)

	# add bounds
	name = 'bounds'
	path = '{}/bounds::{}'.format(spud_path, name)
	try:
		libspud.add_option(path)
	except libspud.SpudNewKeyWarning as err:
		pass
	expression = '{} = {} '.format(name, str(ofunc['bounds']))
	write_expression(path, expression)


if __name__ == "__main__":
	# list_available_parameters('test_asymmetric.phml')
	from collections import OrderedDict
	import sympy as sym
	import os, sys
	import libspud
	from glob import glob
	from thermocodegen.coder.coder import SimpleSolnModel, ComplexSolnModel

	files = glob('Ortho*.phml')
	for file in files:
		print(file)
		model_dict = read_phase(file, active='all', flatten_potentials=False, flatten_vectors=False)
		if model_dict['ordering_functions'] is None:
			model = SimpleSolnModel.from_dict(model_dict)
		else:
			model = ComplexSolnModel.from_dict(model_dict)
		model.write_code()

# active = None
# dict = read_phase('asymmetric_regular_solution.phml',active=active)
# print(dict)
# T, P, T_r, P_r, n1, n2, n3, R, G = sym.symbols('T,P,T_r,P_r,n1,n2,n3,R,G')
# mu1 = sym.Function('mu1')
# mu2 = sym.Function('mu2')
# mu3 = sym.Function('mu3')
#
# model_dict = OrderedDict([('name', 'Felsdpar'),
#                           ('abbrev', None),
#                           ('formula',
#                            {'formula_string': 'Ca[Ca]Na[Na]K[K]Al[Al]Si[Si]O8',
#                             'conversion_string': ['[0]=[Na]', '[1]=[Ca]', '[2]=[K]'],
#                             'test_string': ['[0] > 0.0', '[1] > 0.0', '[2] > 0.0']}),
#                           ('reference',
#                            'http://localhost:8889/files/Example-14-Simple-Solution_coder.ipynb'),
#                           ('K', 3),
#                           ('endmembers',
#                            ['High_Albite_berman', 'Anorthite_berman', 'Potassium_Feldspar_berman']),
#                           ('filename', None),
#                           ('model',
#                            {'name': 'Gibbs',
#                             'variables': [{'name': 'T', 'rank': 'Scalar', 'units': 'K', 'symbol': T},
#                                           {'name': 'P', 'rank': 'Scalar', 'units': 'bar', 'symbol': P},
#                                           {'name': 'n',
#                                            'rank': 'Vector',
#                                            'size': 3,
#                                            'units': 'mol',
#                                            'expression': sym.Matrix([
#                                                [n1],
#                                                [n2],
#                                                [n3]])}],
#                             'function': {'name': 'mu',
#                                          'type': 'external',
#                                          'rank': 'Vector',
#                                          'size': 3,
#                                          'expression': sym.Matrix([
#                                              [mu1(T, P)],
#                                              [mu2(T, P)],
#                                              [mu3(T, P)]])}}
#                            ),
#                           ('parameters',
#                            [{'name': 'T_r',
#                              'rank': 'Scalar',
#                              'size': 1,
#                              'value': 298.15,
#                              'units': 'K',
#                              'symbol': T_r},
#                             {'name': 'P_r',
#                              'rank': 'Scalar',
#                              'size': 1,
#                              'value': 1.0,
#                              'units': 'bar',
#                              'symbol': P_r},
#                             {'name': 'R',
#                              'rank': 'Scalar',
#                              'size': 1,
#                              'value': 8.31446261815324,
#                              'units': 'J/mol/K',
#                              'symbol': R}
#                             ]),
#                           ('subs_dict', None),
#                           ('potentials',
#                            [{'name': 'G',
#                              'symbol': G,
#                              'expression': R * T * (
#                                      n1 * sym.log(n1 / (n1 + n2 + n3)) + n2 * sym.log(
#                                  n2 / (n1 + n2 + n3)) + n3 * sym.log(
#                                  n3 / (n1 + n2 + n3))) + n1 * mu1(T, P) + n2 * mu2(T, P) + n3 * mu3(T, P)}]),
#                           ('implicit_functions', []),
#                           ('ordering_functions', [])])
#
# write_phase(model_dict)

# dictD = read_endmember('test_GD.emml')
