#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 15 20:28:48 2018

@author: mspieg
@file:spudio/parser.py

    description: Convenience python class for parsering ENKI spud markup language files
"""
#from __future__ import absolute_import
from __future__ import print_function
from . import endmember as em
from . import phase as ph
from . import reaction as rx
from . import optionparsers as op
import libspud
import os


class Parser(object):

    """Convenience Class for reading and parsing Spud xml files from the ENKI projedt

    handles all available markup language files describing various
    thermodynamic objects e.g.

    endmembers: .emml
    phases: .phml
    reactions: .rxml
    """

    def __init__(self,filename):
        """ Initialize class with a valid spud filename

        Input
        --------
        filename: path to a spud filename including extension
        (.emml, .phml, .rxml )

        """
        
        # get base and directory from absolute path of file
        # initialize superset of internal parameters (ugly)
        self.filename = os.path.abspath(filename)
        self.modeldir = os.path.dirname(self.filename)
        self.name = op.parse_name(self.filename)
        self.current_dict = None
        self.parameters = None
        self.parameter_names = None
        self.endmembers = None
        self.phase_abbrev = None
        self.model = None
        #FIXME:  these should all be implemented in overloaded classes
        if filename.endswith('.emml'):
            try:
                self.filetype='emml'
                self.parameters = op.parse_all_parameters(filename)
                self.parameter_names = [ param['name'] for param in self.parameters ]
                self.read_file = em.read_endmember
                #self.name = em.parse_endmember_name(filename)
                self.model = em.parse_model_name(filename)
                self.formula = em.parse_endmember_formula(filename)
            except libspud.SpudKeyError:
                raise OSError('File {} is not a valid endmember .emml file'.format(filename))
        elif filename.endswith('.phml'):
            try:
                self.filetype='phml'
                self.parameters = op.parse_all_parameters(filename)
                self.parameter_names = [ param['name'] for param in self.parameters ]
                self.endmembers = ph.parse_endmembers(filename)
                self.read_file = ph.read_phase
                #self.name = ph.parse_phase_name(filename)
                self.phase_abbrev =ph.parse_phase_abbrev(filename)
                self.model = em.parse_model_name(filename)
            except libspud.SpudKeyError:
                raise OSError('File {} is not a valid phase .phml file'.format(filename))
        elif filename.endswith('.rxml'):
            try:
                self.filetype='rxml'
                self.read_file = rx.read_reactions
                #self.name = rx.parse_reaction_name(self.filename)
            except libspud.SpudKeyError:
                raise OSError('File {} is not a valid reaction .rxml file'.format(filename))
        else:
            raise OSError('file type {} not recognized'.format(filename ))


    def list_parameters(self):
        """pretty print available parameter names, values and units)"""


        try:
            scalars = [ param  for param in self.parameters if param['rank'] == 'Scalar']
            vectors = [ param  for param in self.parameters if param['rank'] == 'Vector']

            print('Scalar Parameters')
            fmt='    {:<10.10s} {:13.6e} {:<10.10s}'
            for scalar in scalars:
                print(fmt.format(scalar['name'],scalar['value'],scalar['units']))

            print('Vector Parameters')
            for vector in vectors:
                for i in range(vector['size']):
                    print('    {:<0.10s}[{}] {:19.6e} {:<15.15s}'.
                          format(vector['name'],i,vector['value'][i],vector['units'][i]))
                #print('')
        except Exception as err:
            print('No parameters in this dictionary')
            
    def list_variables(self,dict=None):
        """pretty print available variable names from dictionary.
        
        Usage: parser.list_variables(dict=None)
        
        if no dictionary is passed, it will try to use the last generated dictionary
        
        """
    
        if dict is None:
            dict = self.current_dict
            
        try: 
            var_dict = self.get_variables(dict)
        
            print('Variables:\nname\tsymbol')
            for key,val in var_dict.items() :
                print('{}\t{}'.format(key,val))
        except Exception:
            print('No variables in this dictionary')
            
    def list_potentials(self,dict=None):
        """pretty print available potentials from dictionary.
        
        Usage: parser.list_potentials(dict=None)
        
        if no dictionary is passed, it will try to use the last generated dictionary
        
        """
    
        if dict is None:
            dict = self.current_dict
                    
        try:
            print('\nPotentials')
            for potential in dict['potentials']:
                print('\n***** {} *****'.format(potential['name']))
                print(potential['expression'])
                print('')
        except Exception:
            print('No Potentials in this dictionary')
        
    

    def get_variables(self,dict=None):
        """return a dictionary of { variable_name:variable_symbol}.
        
        Usage: parser.get_variables(dict=None)
        
        if no dictionary is passed, it will try to use the last generated dictionary
        
        """
    
        if dict is None:
            local_dict = self.current_dict
        else:
            local_dict = dict
            
        assert(local_dict is not None),"list_variables: please generate or pass a dictionary"
        
        try:
            var_dict = {}
            for var in local_dict['model']['variables']:
                var_dict.update({var['name']:var['symbol']})
            
            return var_dict
        except Exception:
            print('No variables in this dictionary')
            return None
            
            

    def generate_dict(self, active='all', flatten_potentials=False):
        """ parse spud file and process sympy expression into nested dictionary

        Inputs
        -------
        active: list of Parameter names to be used in the active set for calibration
            takes multiple values
            active = None (default) all parameter values are substituted in
            active = 'All'  all parameter values are active for calibration
            active = [ python list ] list of strings with valid parameter names

        flatten_potentials:  if True, add additional summed potential

        Output
        ------
        dict: nested python dictionary with information from spud files to be passed
        to other software (particularly code generation routines)

        """
        if self.filetype == 'rxml':
            dict = self.read_file(self.filename)
        else:    
            assert(isinstance(flatten_potentials,(bool)))

            # check that active list is valid
            if isinstance(active,   list):
                for p_a in active:
                    assert(p_a in self.parameter_names)

                    # Check rank of active parameter
                    p_a_index = [i for i, p in enumerate(self.parameters) if p['name'] == p_a][0]
                    if self.parameters[p_a_index]['rank'] == 'Vector':
                        raise OSError('Active vector-valued parameters not handled yet')

            # read file and generate dictionary
            dict = self.read_file(self.filename,active=active,flatten_potentials=flatten_potentials)
        
        self.current_dict = dict
        return dict


if __name__ == "__main__":
    """ quick check of Parser Class"""
    
    emp = Parser('test_GD_multiline.emml')
    print('Filename = {}'.format(emp.filename))
    print('name = {}'.format(emp.name))

    emp.list_parameters()
    
    
    active = ['H0','V0','S0']
    dict = emp.generate_dict(active=active)
    emp.list_potentials()
        
    
    
    #php = Parser('test_ideal.phml')
    #ph_dict = php.generate_dict()
    #print(ph_dict)
    
    
    ph_Fs = Parser('test_asymmetric.phml')
    print('Filename:\t{}'.format(emp.filename))
    print('Phase:   \t{}'.format(emp.name))
    print('*** Endmembers ***')
    for endmember in ph_Fs.endmembers:
        print('\t{}'.format(endmember))
    
    ph_Fs.list_parameters()
    ph_Fs_dict = ph_Fs.generate_dict()
    ph_Fs.list_potentials()

    
    file = '/Users/mspieg/Repos/gitlab/ThermoCodegen/examples/codegen/berman_reactions/Forsterite_Fayalite_binary.rxml'
    rxn = Parser(file)
    rxn_dict = rxn.generate_dict()
    rxn.validate()
    
    
        
            
