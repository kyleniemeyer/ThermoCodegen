#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
toBerman()

Utility function to convert standard Chemical formula to Berman style formula
Created on Wed Aug  1 06:24:35 2018

@author: mspieg
"""

import molmass

# utility routine to convert to Berman formulas
def toBerman(formula):
    """Utility function to convert standard chemical formula to Berman Style formula
        Usage toBerman(formula)
        
        where formula is a string e.g. Mg2SiO4
        returns Berman style formula Mg(2)Si(1)O(4)
    """
    #FIXME: should check for exceptions but this won't catch Formula error
    try:
        f = molmass.Formula(formula)
    except:
        print('Error: {} is not a valid chemical formula'.format(formula))
        return None
              
    composition = f.composition()
    index = [ formula.index(element[0]) for element in composition ]
    # some evil python magic 
    order = sorted(range(len(index)),key=index.__getitem__)
    
    f_berman = ''
    for i in order:
        f_berman += '{}({})'.format(composition[i][0],composition[i][1])
    return f_berman
        
