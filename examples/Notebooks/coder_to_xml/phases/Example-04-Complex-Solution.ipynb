{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Complex Solution SymPy Code Generation\n",
    "\n",
    "This Notebook is lightly adapted from Mark Ghiorso's Notebook in ThermoEngine, for complex solutions with Cation ordering to produce a 3 endmember 1 Species Pyroxene model.\n",
    "\n",
    "## Solutions with Cation Ordering\n",
    "Generation of configurational entropy (${S^{config}}$) using combinatorics coupled with $n^{th}$-order Taylor expansion of the non-configurational Gibbs free energy (${G^*}$).  The final expression for the Gibbs free energy of solution is given by $G = -{T}{S^{config}} + {G^*}$.  \n",
    "\n",
    "This notebook illustrates construction of this problem using the coder module of the thermoengine package.  \n",
    "\n",
    "Generally, the Taylor expansion of ${G^*}$ is taken to order two (equivalent to regular solution theory) and cation-ordering between symmetrically non-equivalent crystallographic sites is assumed to be non-convergent, i.e. the random ordering state is not acheived at finite temperature.  Alternately, cation-ordering may be modeled as convergent, inducing a symmetry breaking phase transition at finite temperature, which necessitates Taylor expansion of ${G^*}$ to at least $4^{th}$ order (in ordering parameter) with retention of only even powers of the ordering variable(s) in the expansion.  \n",
    "\n",
    "This notebook illustrates non-convergent ordering in a reciprocal solution model for orthpyroxene in the compositional space of the pyroxene quadrilateral: Mg<sub>2</sub>Si<sub>2</sub>O<sub>6</sub>-Fe<sub>2</sub>Si<sub>2</sub>O<sub>6</sub>-CaMgSi<sub>2</sub>O<sub>6</sub>-CaFeSi<sub>2</sub>O<sub>6</sub>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os,sys\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "import sympy as sym\n",
    "import hashlib\n",
    "import time\n",
    "sym.init_printing()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from thermocodegen.coder import coder"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### let's set up some directory names for clarity"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "HOME_DIR = os.path.abspath(os.curdir)\n",
    "SPUD_DIR = HOME_DIR+'/spudfiles_soln_phases'\n",
    "try:\n",
    "    os.mkdir(SPUD_DIR)\n",
    "except:\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set a reference string for this Notebook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reference = 'Thermocodegen-v0.6/share/thermocodegen/examples/Notebooks/coder_to_xml/phases/Example-04-Ternary_OPX_Complex_Soln.ipynb'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Complex Solution Properties - General structure of the model\n",
    "There are three terms:\n",
    "- Terms describing standard state contributions\n",
    "- Terms describing the configurational entropy of solution\n",
    "- Terms describing the excess enthalpy of solution  \n",
    "\n",
    "Assumptions:\n",
    "- There are $c$ components in the system\n",
    "- There may be more endmember species, $w$, than there are components, thereby allowing for reciprocal solutions\n",
    "- Cation ordering is permitted, which may be either convergent or non-convergent.  There may be zero or more cation ordering variables, $s$.\n",
    "- The configurational entropy formulation assumes random mixing on symmetrically distinct crystallographic sites\n",
    "- The excess enthalpy is described using a Taylor series expansion in compositional and ordering variables.  The order of the expansion is $\\nu$. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Number of solution components and number of solution species\n",
    "Note, that the example illustrated in this notebook - orthopyroxene in the system Mg<sub>2</sub>Si<sub>2</sub>O<sub>6</sub>-Fe<sub>2</sub>Si<sub>2</sub>O<sub>6</sub>-CaMgSi<sub>2</sub>O<sub>6</sub>-CaFeSi<sub>2</sub>O<sub>6</sub>, requires three endmember thermodynamic components but clearly has four endmember species.  This is an example of a recipocal solution. One of the species endmembers is compositionally redundent, but *not* energetically redundant.  Hence the Gibbs free energy change of the reaction:  \n",
    "Mg<sub>2</sub>Si<sub>2</sub>O<sub>6</sub> + 2 CaFeSi<sub>2</sub>O<sub>6</sub> = Fe<sub>2</sub>Si<sub>2</sub>O<sub>6</sub> + 2 CaMgSi<sub>2</sub>O<sub>6</sub>  \n",
    "is not zero, even though the concentration of the species Fe<sub>2</sub>Si<sub>2</sub>O<sub>6</sub> may be expressed as:  \n",
    "2 CaFeSi<sub>2</sub>O<sub>6</sub> - 2 CaMgSi<sub>2</sub>O<sub>6</sub> + Mg<sub>2</sub>Si<sub>2</sub>O<sub>6</sub>  \n",
    "Additionally, there is one variable that denotes the degree of cation ordering of Fe<sup>++</sup> and Mg over the M1 and M2 crystallographic sites in the pyroxene structure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nc = 3\n",
    "nw = 4\n",
    "ns = 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create a complex solution model\n",
    "A *complex* solution is one that includes ordering parameters as well as endmember thermodynamic components.  \n",
    "Instantiate the class with the specified number of endmember thermodynamic components and species"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = coder.ComplexSolnModel.from_type(nc=nc, ns=ns, nw=nw)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Retrieve primary compositional variables\n",
    "- $n$ is a vector of mole numbers of each component  \n",
    "- $n_T$ is the total number of moles in the solution\n",
    "- $s$ is a vector of ordering parameters\n",
    "\n",
    "### and construct a derived mole fraction variable\n",
    "- $X$ is a vector of mole fractions of thermodynamic components in the system\n",
    "\n",
    "### and a reduced set of independent composition variables\n",
    "- $r$ is a vector of independent mole fractions in the system. By convention, $r_{i-1}=X_i$, where $i$ ranges from the second index of $X$ up to $c$.  Hence the length of the vector $r$ is $c-1$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = model.n\n",
    "nT = model.nT\n",
    "s = model.s\n",
    "X = n/nT\n",
    "r = X[1:]\n",
    "n, nT, X, r, s"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Retrieve the temperature, pressure, and standard state chemical potentials\n",
    "- $T$ is temperature in $K$\n",
    "- $P$ is pressure in $bars$\n",
    "- $\\mu^o$ in Joules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T = model.get_symbol_for_t()\n",
    "P = model.get_symbol_for_p()\n",
    "mu = model.mu\n",
    "T,P,mu"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define the standard state contribution to solution properties"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "G_ss = (n.transpose()*mu)[0]\n",
    "G_ss"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define configurational entropy and configurational Gibbs free energy\n",
    "Configurational enropy is calculated by counting site configurations, that is the number of ways of mixing Fe<sup>++</sup>, Mg and Ca on the M2 site, $\\Omega^{M2}$,  times the number of ways of mixing Fe<sup>++</sup> and Mg on the M1 site, $\\Omega^{M1}$; configurations ($\\Omega$) equal  $\\Omega^{M1}\\Omega^{M2}$.  The assumption is made that the mixing on each site is random, i.e.  \n",
    "If there are two cations on site M1, and their mole fractions on that site are denoted $X$ and $Y$, and if there is one such sites in the formula unit, then the number of configurations, $\\Omega$, associted with ***random*** mixing of cations on that site is:    \n",
    "$\\Omega  = \\left[ {\\frac{{\\left( {X + Y} \\right)!}}{{X!Y!}}} \\right]$  \n",
    "and the molar configurational entropy conribution associated with these configurations is given by Boltzmann's law: ${{\\hat S}^{conf}} =  R\\log \\Omega$:  \n",
    "${{\\hat S}^{conf}} =  cR\\log \\left[ {\\frac{{\\left( {X + Y} \\right)!}}{{X!Y!}}} \\right]$  \n",
    "Using Stirlings approximation for large factorials, $\\log X! = X\\log X - X$, the configurational entropy can be written:  \n",
    "${{\\hat S}^{conf}} =  cR\\left[ - {X\\log X - Y\\log Y + \\left( {X + Y} \\right)\\log \\left( {X + Y} \\right)} \\right]$  \n",
    "\n",
    "Consequently, to utilize this appropach we must define site mole fractions in terms of our chosen set of independent compositional variables and ordering parameters.\n",
    "#### There are 5 site mole fractions:  \n",
    "$X_{Ca}^{M2}$, $X_{Mg}^{M2}$, $X_{{Fe}^{2+}}^{M2}$, $X_{Mg}^{M1}$, $X_{{Fe}^{2+}}^{M1}$  \n",
    "#### The requirement of filled sites requires:\n",
    "1. $X_{Ca}^{M2}$ + $X_{Mg}^{M2}$ + $X_{{Fe}^{2+}}^{M2}$ = 1\n",
    "2. $X_{Mg}^{M1}$ + $X_{{Fe}^{2+}}^{M1}$ =1\n",
    "\n",
    "#### Asuuming the endmembers are ordered as:\n",
    "- $n_1$, $X_1$, CaMgSi<sub>2</sub>O<sub>6</sub>\n",
    "- $n_2$, $X_2$, CaFeSi<sub>2</sub>O<sub>6</sub>\n",
    "- $n_3$, $X_3$, Mg<sub>2</sub>Si<sub>2</sub>O<sub>6</sub>\n",
    "\n",
    "#### There are two independent compositional variables:\n",
    "- CaMgSi<sub>2</sub>O<sub>6</sub> = $1-r_1-r_2$\n",
    "- CaFeSi<sub>2</sub>O<sub>6</sub> = $r_1$\n",
    "- Mg<sub>2</sub>Si<sub>2</sub>O<sub>6</sub> = $r_2$  \n",
    "\n",
    "#### The requirement of mass balance requires:\n",
    "3. $r_1$ = $X_{{Fe}^{2+}}^{M2}$ + $X_{{Fe}^{2+}}^{M1}$\n",
    "4. $r_2$ = 1 - $X_{Ca}^{M2}$\n",
    "\n",
    "#### There is one ordering parameter:\n",
    "5. $s_1$ = $X_{{Fe}^{2+}}^{M2}$ - $X_{Mg}^{M2}$\n",
    "\n",
    "#### Relations 1-5 may be solved simultaneously to give the following site mole fraction definitions:\n",
    "- $X_{Ca}^{M2}$ = $1-r_2$\n",
    "- $X_{Mg}^{M2}$ = $\\frac{r_2-s_1}{2}$\n",
    "- $X_{{Fe}^{2+}}^{M2}$ = $\\frac{r_2+s_1}{2}$\n",
    "- $X_{Mg}^{M1}$ = $1-r_1+\\frac{r_2+s_1}{2}$\n",
    "- $X_{{Fe}^{2+}}^{M1}$ = $r_1-\\frac{r_2+s_1}{2}$  \n",
    "\n",
    "While this system is fairly easy to solve by inspection, for more complex situations, assemble the relations in a list of equations that evaluate to zero, and automatiocally solve that system of equations using the sympy routine linsolve, i.e.  \n",
    "```\n",
    "system = [xCaM2 + xMgM2 + xFeM2 - 1, xMgM1 + xFeM1 - 1, xFeM2 + xFeM1 - r[0], 1 - xCaM2 - r[1],\n",
    "          xFeM2 - xMgM2 - s[0]]\n",
    "ans = sym.linsolve(system, xCaM2, xMgM2, xFeM2, xMgM1, xFeM1)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xCaM2 = 1 - r[1]\n",
    "xMgM2 = (r[1]-s[0])/2\n",
    "xFeM2 = (r[1]+s[0])/2\n",
    "xMgM1 = 1 - r[0] + (r[1]+s[0])/2\n",
    "xFeM1 = r[0] - (r[1]+s[0])/2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The following functions implement random mixing configurational entropy on the M1 and M2 sites: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def Sconf_M1_random(X, Y):\n",
    "    A = X*sym.log(X) - X\n",
    "    B = Y*sym.log(Y) - Y\n",
    "    ApB = (X+Y)*sym.log(X+Y) - (X+Y)\n",
    "    return ApB - A - B\n",
    "def Sconf_M2_random(X, Y, Z):\n",
    "    A = X*sym.log(X) - X\n",
    "    B = Y*sym.log(Y) - Y\n",
    "    C = Z*sym.log(Z) - Z\n",
    "    ApBpC = (X+Y+Z)*sym.log(X+Y+Z) - (X+Y+Z)\n",
    "    return ApBpC - A - B - C"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Configurational entropy\n",
    "$R$ is the gas constant"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "R = sym.symbols('R')\n",
    "S_config = Sconf_M1_random(xMgM1, xFeM1) + Sconf_M2_random(xCaM2, xMgM2, xFeM2)\n",
    "S_config *= R*nT\n",
    "S_config"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Configurational Gibbs free energy\n",
    "Note that this quantity is extensive, with units of J, *not J/mole*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "G_config = -T*S_config\n",
    "G_config"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## $\\hat G^*$ - Non-configurational molar Gibbs free energy\n",
    "$\\hat G^*$ includes all standard state and excess Gibbs free energy contributions.  It is generally modeled as a Taylor expansion in composition ($r$) and ordering ($s$) variables of order 2, 3 or 4.  Here, we choose a model of order two.\n",
    "#### Taylor expansion of $\\hat G^*$\n",
    "For a second order expansion, the number of Taylor expansion coefficients is:\n",
    "- 1 for $G_{0}$\n",
    "- nc-1 for $G_{r_i}$, $i=1...nc-1$\n",
    "- ns for $G_{s_i}$, $i=1...ns$\n",
    "- (nc-1)(nc-2)/2 for $G_{{r_i},{r_{i+1}}}$, $i= 1...nc-2$\n",
    "- ns(ns-1)/2 for $G_{{s_i},{s_{i+1}}}$, $i= 1...ns-1$\n",
    "- ns(nc-1) for $G_{{r_i},{s_j}}$, $i= 1...nc-1$, $j=1...ns$\n",
    "- nc-1 for $G_{{r_i},{r_i}}$, $i= 1...nc-1$\n",
    "- ns for $G_{{s_i},{s_i}}$, $i= 1...ns$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(count, taylor, taylor_coeff, taylor_terms) = model.taylor_expansion()\n",
    "print ('Number of Taylor expansion terms = {0:.0f}'.format(count))\n",
    "taylor"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Identify Taylor terms of $\\hat{G}^*$ corresponding to component endmembers:\n",
    "1. diopside, CaMgSi<sub>2</sub>O<sub>6</sub>\n",
    "2. hedenbergite, CaFeSi<sub>2</sub>O<sub>6</sub>\n",
    "3. enstatite, Mg<sub>2</sub>Si<sub>2</sub>O<sub>6</sub>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "eqn1 = model.eval_endmember([1,0,0],[0],taylor) - mu[0]\n",
    "eqn2 = model.eval_endmember([0,1,0],[0],taylor) - mu[1]\n",
    "eqn3 = model.eval_endmember([0,0,1],[-1],taylor) - mu[2]\n",
    "params = []\n",
    "units = []\n",
    "symparams = []\n",
    "eqn1, eqn2, eqn3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Identify Taylor terms of $\\hat{G}^*$ corresponding to dependent species endmembers:\n",
    "ferrosilite, Fe<sub>2</sub>Si<sub>2</sub>O<sub>6</sub>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gFs = model.eval_endmember([-2,2,1],[1],taylor)\n",
    "gFs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Identify the free energy of the reciprocal reaction between endmember species:\n",
    "Mg<sub>2</sub>Si<sub>2</sub>O<sub>6</sub> + 2 CaFeSi<sub>2</sub>O<sub>6</sub> = Fe<sub>2</sub>Si<sub>2</sub>O<sub>6</sub> + 2 CaMgSi<sub>2</sub>O<sub>6</sub>  \n",
    "is defined as the \"reciprocal energy,\" $F$, denoting the non-co-planarity of the non-configurational Gibbs free energy of the endmember species. In general, all reciprocal solutions have non-zero $F$. In the paper on pyroxene thermodynamics by Sack and Ghiorso (Contributions to Mineralogy and Petrology 116: 277-286, 1994) $F$ is notated as $\\Delta \\bar G_{27}^o$  \n",
    "\n",
    "$F$ is conveniently defined in terms of expressions 1-4:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Fh,Fs,Fv = sym.symbols('Fh Fs Fv')\n",
    "params.append('Fh')\n",
    "units.append('J/mol')\n",
    "symparams.append(Fh)\n",
    "params.append('Fs')\n",
    "units.append('J/K-mol')\n",
    "symparams.append(Fs)\n",
    "params.append('Fv')\n",
    "units.append('J/bar-mol')\n",
    "symparams.append(Fv)\n",
    "F = Fh - T*Fs + P*Fv\n",
    "eqn4  =   model.eval_endmember([-2,2,1],[ 1],taylor)\n",
    "eqn4 += 2*model.eval_endmember([ 1,0,0],[ 0],taylor)\n",
    "eqn4 -=   model.eval_endmember([ 0,0,1],[-1],taylor)\n",
    "eqn4 -= 2*model.eval_endmember([ 0,1,0],[ 0],taylor)\n",
    "eqn4 -= F\n",
    "eqn4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Identify the free energy of the ordering reaction:\n",
    "MgFeSi<sub>2</sub>O<sub>6</sub> = FeMgSi<sub>2</sub>O<sub>6</sub>  \n",
    "which will be notated as $Gex$, (in Sack and Ghiorso, 1994, $\\Delta \\bar G_{EX}^o$)  \n",
    "Note that both compositions, MgFeSi<sub>2</sub>O<sub>6</sub> and FeMgSi<sub>2</sub>O<sub>6</sub>, are equivalent and defined by CaFeSi<sub>2</sub>O<sub>6</sub> - CaMgSi<sub>2</sub>O<sub>6</sub> + Mg<sub>2</sub>Si<sub>2</sub>O<sub>6</sub>. They differ only by the sign of the ordering parameter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Hex,Vex = sym.symbols('Hex Vex')\n",
    "params.append('Hex')\n",
    "units.append('J/mol')\n",
    "symparams.append(Hex)\n",
    "params.append('Vex')\n",
    "units.append('J/bar-mol')\n",
    "symparams.append(Vex)\n",
    "Gex = Hex + P*Vex\n",
    "eqn5  = model.eval_endmember([-1,1,1],[ 1],taylor)\n",
    "eqn5 -= model.eval_endmember([-1,1,1],[-1],taylor)\n",
    "eqn5 -= Gex\n",
    "eqn5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Identify the free energy of the reciprocal ordering reaction:\n",
    "Mg<sub>2</sub>Si<sub>2</sub>O<sub>6</sub> + Fe<sub>2</sub>Si<sub>2</sub>O<sub>6</sub> = MgFeSi<sub>2</sub>O<sub>6</sub> + FeMgSi<sub>2</sub>O<sub>6</sub>  \n",
    "which will be notated as $Gx$, (in Sack and Ghiorso, 1994, $\\Delta \\bar G_{X}^o$) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Hx,Vx = sym.symbols('Hx Vx')\n",
    "params.append('Hx')\n",
    "units.append('J/mol')\n",
    "symparams.append(Hx)\n",
    "params.append('Vx')\n",
    "units.append('J/bar-mol')\n",
    "symparams.append(Vx)\n",
    "Gx = Hx + P*Vx\n",
    "eqn6  = model.eval_endmember([-1,1,1],[ 1],taylor)\n",
    "eqn6 += model.eval_endmember([-1,1,1],[-1],taylor)\n",
    "eqn6 -= model.eval_endmember([ 0,0,1],[-1],taylor)\n",
    "eqn6 -= model.eval_endmember([-2,2,1],[ 1],taylor)\n",
    "eqn6 -= Gx\n",
    "eqn6"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Identify regular solution interaction parameters:\n",
    "- Ca-Mg interaction on the M2 site, the join Mg<sub>2</sub>Si<sub>2</sub>O<sub>6</sub> - CaMgSi<sub>2</sub>O<sub>6</sub>, denoted WM2CaMg\n",
    "- Ca-Fe interaction on the M2 site, the join Fe<sub>2</sub>Si<sub>2</sub>O<sub>6</sub> - CaFeSi<sub>2</sub>O<sub>6</sub>, denoted WM2CaFe\n",
    "- Fe-Mg interaction on the M1 site, the joins Mg<sub>2</sub>Si<sub>2</sub>O<sub>6</sub> - MgFeSi<sub>2</sub>O<sub>6</sub> or Fe<sub>2</sub>Si<sub>2</sub>O<sub>6</sub> - FeMgSi<sub>2</sub>O<sub>6</sub> or CaMgSi<sub>2</sub>O<sub>6</sub> - CaFeSi<sub>2</sub>O<sub>6</sub>, which are assumed to be energetically equivalent, denoted WM1FeMg (in Sack and Ghiorso, 1994, $W_{12}$\n",
    "- Fe-Mg interaction on the M2 site, the joins FeMgSi<sub>2</sub>O<sub>6</sub> - Mg<sub>2</sub>Si<sub>2</sub>O<sub>6</sub> or Fe<sub>2</sub>Si<sub>2</sub>O<sub>6</sub> - MgFeSi<sub>2</sub>O<sub>6</sub>, which are assumed to be energetically equivalent, denoted WM2FeMg  \n",
    "\n",
    "Along the A-B join, described using a regular solution parameter, $W$, $\\hat G^*$ is given by  \n",
    "${\\hat G}^*(X_A,X_B)={X_A}{\\hat G}^*(A)+{X_B}{\\hat G}^*(B)+W{X_A}{X_B}$, so   \n",
    "$W = \\frac{{\\hat G}^*(X_A,X_B) - {X_A}{\\hat G}^*(A) - {X_B}{\\hat G}^*(B)}{{X_A}{X_B}}$  \n",
    "Taking the midpoint of the join provides a way to define the parameter:  \n",
    "$W = \\frac{{\\hat G}^*(\\frac{1}{2},\\frac{1}{2}) - {\\frac{1}{2}}{\\hat G}^*(A) - {\\frac{1}{2}}{\\hat G}^*(B)}{{\\frac{1}{2}}{\\frac{1}{2}}} = 4{\\hat G}^*(\\frac{1}{2},\\frac{1}{2}) - 2{\\hat G}^*(A) - 2{\\hat G}^*(B)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Ca-Mg interaction on the M2 site, the join Mg<sub>2</sub>Si<sub>2</sub>O<sub>6</sub> - CaMgSi<sub>2</sub>O<sub>6</sub>, denoted WM2CaMg"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WhM2CaMg,WvM2CaMg = sym.symbols('WhM2CaMg WvM2CaMg')\n",
    "params.append('WhM2CaMg')\n",
    "units.append('J/mol')\n",
    "symparams.append(WhM2CaMg)\n",
    "params.append('WvM2CaMg')\n",
    "units.append('J/bar-mol')\n",
    "symparams.append(WvM2CaMg)\n",
    "WM2CaMg = WhM2CaMg + P*WvM2CaMg\n",
    "eqn7 = model.eval_regular_param([1,0,0],[0],[0,0,1],[-1],taylor) - WM2CaMg\n",
    "eqn7"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Ca-Fe interaction on the M2 site, the join Fe<sub>2</sub>Si<sub>2</sub>O<sub>6</sub> - CaFeSi<sub>2</sub>O<sub>6</sub>, denoted WM2CaFe"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WhM2CaFe,WvM2CaFe = sym.symbols('WhM2CaFe WvM2CaFe')\n",
    "params.append('WhM2CaFe')\n",
    "units.append('J/mol')\n",
    "symparams.append(WhM2CaFe)\n",
    "params.append('WvM2CaFe')\n",
    "units.append('J/bar-mol')\n",
    "symparams.append(WvM2CaFe)\n",
    "WM2CaFe = WhM2CaFe + P*WvM2CaFe\n",
    "eqn8 = model.eval_regular_param([0,1,0],[0],[-2,2,1],[1],taylor) - WM2CaFe\n",
    "eqn8"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Fe-Mg interaction on the M1 site, the joins Mg<sub>2</sub>Si<sub>2</sub>O<sub>6</sub> - MgFeSi<sub>2</sub>O<sub>6</sub> or Fe<sub>2</sub>Si<sub>2</sub>O<sub>6</sub> - FeMgSi<sub>2</sub>O<sub>6</sub> or CaMgSi<sub>2</sub>O<sub>6</sub> - CaFeSi<sub>2</sub>O<sub>6</sub>, which are assumed to be energetically equivalent, denoted WM1FeMg (in Sack and Ghiorso, 1994, $W_{12}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WhM1FeMg,WvM1FeMg = sym.symbols('WhM1FeMg WvM1FeMg')\n",
    "params.append('WhM1FeMg')\n",
    "units.append('J/mol')\n",
    "symparams.append(WhM1FeMg)\n",
    "params.append('WvM1FeMg')\n",
    "units.append('J/bar-mol')\n",
    "symparams.append(WvM1FeMg)\n",
    "WM1FeMg = WhM1FeMg + P*WvM1FeMg\n",
    "eqn9 = model.eval_regular_param([1,0,0],[0],[0,1,0],[0],taylor) - WM1FeMg\n",
    "eqn9"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Fe-Mg interaction on the M2 site, the joins FeMgSi<sub>2</sub>O<sub>6</sub> - Mg<sub>2</sub>Si<sub>2</sub>O<sub>6</sub> or Fe<sub>2</sub>Si<sub>2</sub>O<sub>6</sub> - MgFeSi<sub>2</sub>O<sub>6</sub>, which are assumed to be energetically equivalent, denoted WM2FeMg"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WhM2FeMg,WvM2FeMg = sym.symbols('WhM2FeMg WvM2FeMg')\n",
    "params.append('WhM2FeMg')\n",
    "units.append('J/mol')\n",
    "symparams.append(WhM2FeMg)\n",
    "params.append('WvM2FeMg')\n",
    "units.append('J/bar-mol')\n",
    "symparams.append(WvM2FeMg)\n",
    "WM2FeMg = WhM2FeMg + P*WvM2FeMg\n",
    "eqn10 = model.eval_regular_param([-1,1,1],[1],[0,0,1],[-1],taylor) - WM2FeMg\n",
    "eqn10"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Solve for the Taylor coefficients in terms of the preferred parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "system = [eqn1, eqn2, eqn3, eqn4, eqn5, eqn6, eqn7, eqn8, eqn9, eqn10]\n",
    "system"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "taylor_soln = sym.linsolve(system, taylor_coeff).args[0]\n",
    "taylor_soln"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Substitute terms into $\\hat G^*$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sub_list = []\n",
    "for a,b in zip(taylor_coeff,taylor_soln):\n",
    "    sub_list.append((a,b))\n",
    "G_star_molar = taylor.subs(sub_list)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(params)\n",
    "print(units)\n",
    "print(symparams)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define the Gibbs free energy of solution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "G = G_config + nT*G_star_molar\n",
    "G"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Find the condition of homogeneous equilibrium:\n",
    "$\\frac{{\\partial \\hat G*}}{{\\partial {s_1}}} = 0$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dgds = (nT*G_star_molar+G_config).diff(s[0]).simplify()\n",
    "dgds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Identify bounds on the ordering parameter \n",
    "The code generated to implement this model must compute numerical values of the ordering parameter as a functrion of compositrion, temperature and pressure.  This task requires an iterative procedure.  To construct this procedure the model must have information on the permissble domain of the ordering parameter.  \n",
    "\n",
    "Values of the ordering parameter are bounded by the composition of the solution. We contrain all site mole fractions to have values in the range 0 to 1, and solve this system of inequality constraints to obtain a logical expression that embodies the feasible domain for the numerical procedure. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "out = sym.reduce_inequalities(inequalities=[\n",
    "    0 <= (r[1]-s[0])/2, (r[1]-s[0])/2 <= 1,\n",
    "    0 <= (r[1]+s[0])/2, (r[1]+s[0])/2 <= 1, \n",
    "    0 <= 1-r[0]+(r[1]+s[0])/2,  1-r[0]+(r[1]+s[0])/2 <= 1, \n",
    "    0 <= r[0]-(r[1]+s[0])/2, r[0]-(r[1]+s[0])/2 <= 1], symbols=[s[0]])\n",
    "out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Add the Gibbs free energy of solution to the model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "params.append('R')\n",
    "units.append('J/mol/K')\n",
    "symparams.append(R)\n",
    "model.add_potential_to_model('G',G, list(zip(params, units, symparams)), ordering_functions=([dgds],s,[0],out))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... exam the model dictionary"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#model.model_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Set specific values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "values_dict=model.get_values()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.empty_keys()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "values_dict.update(dict(formula_string = 'Ca[Ca]Mg[Mg]Fe[Fe]Si[Si]O6',\n",
    "                        conversion_string = ['[0]=[Ca]-[Fe]', '[1]=[Fe]', '[2]=-0.5*[Ca]+0.5*[Fe]+0.5*[Mg]'],\n",
    "                        test_string = ['[0]+[1] > 0.0', '[1] > 0.0', '[0]+2.0*[2] > 0.0']\n",
    "                       ))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Define Parameters of an Orthopyroxene Solution\n",
    "Components\n",
    "1. diopside, CaMgSi<sub>2</sub>O<sub>6</sub>\n",
    "2. hedenbergite, CaFeSi<sub>2</sub>O<sub>6</sub>\n",
    "3. enstatite, Mg<sub>2</sub>Si<sub>2</sub>O<sub>6</sub>\n",
    "\n",
    "Original calibration from Sack and Ghiorso (Contributions to Mineralogy and Petrology, 116:287-300, 1994):\n",
    "```\n",
    "F       = -13807 + 2.319*T - 0.05878*P;  /* joules     */\n",
    "Gex     =  -7824           - 0.1213*P;   /* joules/K   */\n",
    "Gx      =  -1883           + 0.02824;    /* joules/bar */\n",
    "WM2CaMg =  31631           + 0.03347*P;  /* joules     */\n",
    "WM2CaFe =  17238           + 0.04602*P;  /* joules/K   */\n",
    "WM1FeMg =   8368           + 0.01412*P;  /* joules/bar */\n",
    "WM2FeMg =   8368           + 0.01412*P;  /* joules     */\n",
    "```\n",
    "Asymmetry along the Ca-Mg and Ca-Fe joins (considered by Sack and Ghiorso, 1994) is not considered in this example in order to simplify the presentation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print (params)\n",
    "paramValues = {'Fh':-13807.0, 'Fs':-2.319, 'Fv':-0.05878, \\\n",
    "               'Hex':-7824.0, 'Vex':-0.1213, \\\n",
    "               'Hx':-1883.0, 'Vx':0.02824, \\\n",
    "               'WhM2CaMg':31631.0, 'WvM2CaMg':0.03347, \\\n",
    "               'WhM2CaFe':17238.0, 'WvM2CaFe':0.04602, \\\n",
    "               'WhM1FeMg':8368.0, 'WvM1FeMg':0.01412, \\\n",
    "               'WhM2FeMg':8368.0, 'WvM2FeMg':0.01412, \\\n",
    "               'T_r':298.15, 'P_r':1.0, 'R':8.31446261815324}\n",
    "print (paramValues)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "values_dict.update(paramValues)\n",
    "values_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Add additional parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "values_dict.update(dict(name='Orthopyroxene',abbrev='Opx',\n",
    "                        reference=reference,\n",
    "                        endmembers = ['Diopside_berman', 'Hedenbergite_berman', 'Enstatite_berman']))\n",
    "values_dict"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.set_values(values_dict)\n",
    "model.model_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Generate Spud XML files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### dump spudfile"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.to_xml(path=SPUD_DIR, validate=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### validate xml files and return to top directory"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "os.chdir(SPUD_DIR)\n",
    "!spud-update-options *.phml\n",
    "os.chdir(HOME_DIR)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
