# -*- coding: utf-8 -*-
"Script to write base class files"
import sys
import os
import subprocess


def generate_db_base_files(build_dir, template_dir):
    """Generate base files for database"""
    # Read base files for endmembers and phases
    endmember_header = open(template_dir + "/endmembers/base.h", "r")
    endmember_cpp = open(template_dir + "/endmembers/base.cpp", "r")
    phase_header = open(template_dir + "/phases/base.h", "r")
    phase_cpp = open(template_dir + "/phases/base.cpp", "r")

    with open(build_dir + "/include/EndMember.h", "w") as header_file:
        header_file.write(endmember_header.read())

    header_file.close()

    with open(build_dir + "/src/EndMember.cpp", "w") as cpp_file:
        cpp_file.write(endmember_cpp.read())

    cpp_file.close()

    # Write phase files
    with open(build_dir + "/include/Phase.h", "w") as header_file:
        header_file.write(phase_header.read())

    header_file.close()

    with open(build_dir + "/src/Phase.cpp", "w") as cpp_file:
        cpp_file.write(phase_cpp.read())

    cpp_file.close()


def generate_reaction_base_files(build_dir, template_dir):
    """Generate base files for reaction"""
    # Read base files
    header = open(template_dir + "/reactions/base.h", "r")
    cpp = open(template_dir + "/reactions/base.cpp", "r")

    # Write base files
    with open(build_dir + "/include/Reaction.h", "w") as header_file:
        header_file.write(header.read())

    header_file.close()

    with open(build_dir + "/src/Reaction.cpp", "w") as cpp_file:
        cpp_file.write(cpp.read())

    cpp_file.close()


def main():
    # Path to build directory
    build_dir = sys.argv[1]

    # Path to templates
    tcg_home = os.environ.get('THERMOCODEGEN_HOME')
    if tcg_home is None:
        raise OSError('Environment variable THERMOCODEGEN_HOME is not set')

    template_dir = tcg_home + '/share/thermocodegen/templates/cpp'

    base_type = sys.argv[2]
    if base_type == 'database':
        generate_db_base_files(build_dir, template_dir)
    elif base_type == 'reaction':
        generate_reaction_base_files(build_dir, template_dir)
    else:
        raise ValueError("Base files must be of type 'database' or 'reaction'"
                         ", got {}".format(base_type))


if __name__ == '__main__':
    main()
