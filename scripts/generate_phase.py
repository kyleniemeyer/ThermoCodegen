# -*- coding: utf-8 -*-
"Script to generate C++ and pythonbinding code from C code"

import sys
import os
import warnings
import thermocodegen as tcg
from thermocodegen.spudio import phase
from thermocodegen.coder.coder import SimpleSolnModel, ComplexSolnModel
from string import Template as template
from generate_endmember import set_identifier

import pandas as pd
from ast import literal_eval

warnings.simplefilter('ignore')


def main():
    # Path to templates
    tcg_home = os.environ.get('THERMOCODEGEN_HOME')
    if tcg_home is None:
        raise OSError('Environment variable THERMOCODEGEN_HOME is not set')

    template_dir = tcg_home + '/share/thermocodegen/templates/cpp'

    # phml file
    phml_file = sys.argv[1]

    # Get date, time and hash for this spud file
    identifier = set_identifier(phml_file)

    # Path to build directory
    build_dir = sys.argv[2]

    # Get Spud derived endmember dictionary
    m_dict = phase.read_phase(phml_file, active='all',
                              flatten_potentials=False)

    # Phase name, filename and official abbreviation
    phasename = m_dict['name']
    filename = m_dict['filename']
    abbrev = m_dict['abbrev']

    # Extract endmembers from phase dictionary
    endmembers = m_dict['endmembers']
    em_list = ["std::make_shared<{}>()".format(em) for em in endmembers]
    em_str = "std::vector<std::shared_ptr<EndMember> > {"
    em_str += ",".join(em_list) + " }"

    print("Generating phase: {}".format(phasename))
    # FIXME:  this is going to be fragile for compound names
    C_name = '{}_coder'.format(phasename)

    # Create directory to put coder generated C source
    try:
        coder_src_dir = '{}/src/coder'.format(build_dir)
        os.makedirs(coder_src_dir)
    except OSError:
        pass

    os.chdir(coder_src_dir)
    if m_dict['ordering_functions'] is None:
        model = SimpleSolnModel.from_dict(m_dict)
    else:
        model = ComplexSolnModel.from_dict(m_dict)

    # If --calibfile argument is an existing calibration file, extract the parameters
    # for this phase
    calib_params = []
    calibration_file = sys.argv[3]
    if os.path.isfile(calibration_file):
        df = pd.read_csv(calibration_file,converters={'params': literal_eval})

    try:
        df_ph = df.loc[df['type'] == 'phase']
        # there has to be a cleaner way than this
        calib_params = df_ph['params'].loc[df_ph.name == phasename].tolist()[0]
        print('{}: using calibration parameters -  {}'.format(phasename, calib_params))
    except:
        pass
    model.write_code(silent=True,
                     identifier=identifier,
                     calib_params=calib_params)
    os.chdir(build_dir)

    # Make sure that directories exist
    try:
        os.makedirs('{}/include/phases'.format(build_dir))
    except FileExistsError:
        pass
    try:
        os.makedirs('{}/src/phases'.format(build_dir))
    except FileExistsError:
        pass

    # Touch include file if it doesn't exist
    open(build_dir + "/include/" + "phases.h", 'a').close()

    # Check if phase needs to be included
    write_to_include = True
    with open(build_dir + "/include/" + "phases.h") as include_file:
        line = include_file.readlines()
        if '#include \"phases/{}.h\"\n'.format(phasename) in line:
            write_to_include = False

    # Add endmember to include file
    if write_to_include:
        include_file = open(build_dir + "/include/" + "phases.h", "a")
        include_file.write('#include \"phases/{}.h\"\n'.format(phasename))
        include_file.close()

    # Add include and src dirs
    include_dir = build_dir + "/include/phases/"
    src_dir = build_dir + "/src/phases/"

    # Load header template
    with open(template_dir + '/phases/phase.h', 'r') as _file:
        header = _file.read()

    # Load source template
    with open(template_dir + '/phases/phase.cpp', 'r') as _file:
        source = _file.read()

    ndict = dict(phasename=phasename,
                 C_name=C_name,
                 phasename_uppercase=phasename.upper(),
                 endmembers=em_str,
                 abbrev=abbrev,
                 tcg_version=tcg.__version__,
                 tcg_git_sha=tcg.__git_sha__)

    header = template(header).safe_substitute(ndict)
    source = template(source).safe_substitute(ndict)

    # Write header file
    with open(include_dir + "{name}.h".format(name=phasename), "w") as _header:
        _header.write(header)

    # Write source file
    with open(src_dir + "{name}.cpp".format(name=phasename), "w") as _source:
        _source.write(source)


if __name__ == '__main__':
    main()
