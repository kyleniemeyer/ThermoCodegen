# -*- coding: utf-8 -*-
"Script to generate pybind11 code"

import glob
import sys
import os
import thermocodegen as tcg
from string import Template as template
from thermocodegen.spudio import Parser


def main():
    # Module name
    module_name = sys.argv[1]

    # path to build directory
    build_dir = sys.argv[2]
    spudfile_path = build_dir+'/spudfiles'

    # Include SWIM endmember
    include_swim = sys.argv[3]

    # Locate endmembers, phases and reactions in current project
    endmembers, phases, reactions = [], [], []

    for file in glob.glob("{}/*.emml".format(spudfile_path)):
        parser = Parser(file)
        endmembers.append(parser.name)

    for file in glob.glob("{}/*phml".format(spudfile_path)):
        parser = Parser(file)
        phases.append(parser.name)

    for file in glob.glob("{}/*rxml".format(spudfile_path)):
        parser = Parser(file)
        reactions.append(parser.name)

    # Path to templates
    tcg_home = os.environ.get('THERMOCODEGEN_HOME')
    if tcg_home is None:
        raise OSError('Environment variable THERMOCODEGEN_HOME is not set')

    template_dir = tcg_home + '/share/thermocodegen/templates'

    # Add include files
    endmember_include, phase_include, reaction_include = """""", """""", """"""

    for em in endmembers:
        endmember_include += """#include "endmembers/{em}.h"\n""".format(em=em)

    if include_swim == "True":
        endmembers.append("SWIM_water")
        endmember_include += """#include "endmembers/SWIM_water.h"\n"""

    for ph in phases:
        phase_include += """#include "phases/{ph}.h"\n""".format(ph=ph)

    for rx in reactions:
        reaction_include += """#include "reactions/{rx}.h"\n""".format(rx=rx)

    include_files = endmember_include + phase_include + reaction_include

    # Templates directory
    py_bindings_dir = template_dir + '/cpp/py_bindings/'

    # Generate endmember binding code
    pybind_endmembers = """"""
    for em in endmembers:
        with open(py_bindings_dir + 'py_endmember.cpp', 'r') as _file:
            _endmember = _file.read()

        endmember_tmp = template(_endmember).safe_substitute({'name': em})
        pybind_endmembers += endmember_tmp
        pybind_endmembers += """\n  """

    # Generate phase binding code
    pybind_phases = """"""
    for ph in phases:
        with open(py_bindings_dir + 'py_phase.cpp', 'r') as _file:
            _phase = _file.read()

        phase_tmp = template(_phase).safe_substitute({'name': ph})
        pybind_phases += phase_tmp
        pybind_phases += """\n  """

    # Generate string of vector of pointers to phases
    phase_list = ["std::make_shared<{}>()".format(phase) for phase in phases]
    phase_str = "{" + ",".join(phase_list) + "}"

    # Generate reaction binding code
    pybind_reactions = """"""
    for rx in reactions:
        with open(py_bindings_dir + 'py_reaction.cpp', 'r') as _file:
            _reaction = _file.read()

        reaction_tmp = template(_reaction).safe_substitute({'name': rx})
        pybind_reactions += reaction_tmp
        pybind_reactions += """\n  """

    # Create pybind dictionary to be substitued into py_bindings.cpp template
    mdict = {"include_files": include_files,
             "module_name": module_name,
             "pybind_endmembers": pybind_endmembers,
             "pybind_phases": pybind_phases,
             "pybind_reactions": pybind_reactions,
             "phases": phase_str,
             "tcg_version": tcg.__version__,
             "tcg_git_sha": tcg.__git_sha__}

    # Load header template
    with open(template_dir + '/cpp/py_bindings/py_bindings.cpp', 'r') as _file:
        py_bindings = _file.read()

    pybind_tmp = template(py_bindings).safe_substitute(mdict)

    # Write binding code
    pybind_file = build_dir + "/python/" + "{}.cpp".format(module_name)
    with open(pybind_file, "w") as _file:
        _file.write(pybind_tmp)


if __name__ == '__main__':
    main()
