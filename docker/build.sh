# Script to build (and push) docker images for ThermoCodegen.

# Note that this assumes that an appropriate multiplatform docker builder is available.
# i.e. it may be necessary to run:
# > docker buildx create --use
# first.

usage() {
    echo "Usage:" 1>&2
    echo "bash $0 [-h] [-t tag<string>] [-b branch<string>] [-r repo<string>] [-d] [-a] dir<string>" 1>&2
    echo "  dir: required name of the subdirectory containing the Dockerfile" 1>&2
    echo "  -h: print this help message and exit" 1>&2
    echo "  -t: specify a tag name (defaults to registry.gitlab.com/enki-portal/thermocodegen:<dir>)" 1>&2
    echo "  -b: specify a branch name (defaults to main, if tag name is default, used as a suffix)" 1>&2
    echo "  -r: specify a repo URL (defaults to https://gitlab.com/ENKI-portal/ThermoCodegen.git)" 1>&2
    echo "  -d: enable debugging (if tag name is default, suffixes name with -debug)" 1>&2
    echo "  -a: build all stages" 1>&2
}

error() {
    usage
    exit 1
}

# realpath not available by default on macs so define it here
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

full_path=$(realpath $0)
script_path=$(dirname $full_path)

# parse the arguments
TAG=''
BRANCH='main'
REPO='https://gitlab.com/ENKI-portal/ThermoCodegen.git'
BUILD='Release'
ALL=0

while getopts ":t:b:r:adh" opt; do
    case $opt in
        h )
           usage
           exit 0
           ;;
        t )
           TAG=${OPTARG}
           ;;
        b )
           BRANCH=${OPTARG}
           ;;
        r )
           REPO=${OPTARG}
           ;;
        d )
           BUILD='Debug'
           ;;
        a )
           ALL=1
           ;;
        : )
           echo "ERROR: -${OPTARG} requires an argument." 1>&2
           error
           ;;
        * )
           echo "ERROR: unknown option -${OPTARG}." 1>&2
           error
           ;;
    esac
done

shift $((OPTIND - 1))

if [ $# == 0 ]; then
    echo "ERROR: missing required subdirectory." 1>&2
    error
fi
DIR=$1

# if no tag is specified default to enki-portal
if [ -z "$TAG" ]; then
    TAG="registry.gitlab.com/enki-portal/thermocodegen:$DIR"
    if [ "$BRANCH" != 'main' ]; then
        TAG="${TAG}-$BRANCH"
    fi
    if [ "$BUILD" == 'Debug' ]; then
        TAG="${TAG}-debug"
    fi
fi

cd $script_path/$DIR
docker buildx build --file Dockerfile \
                    --build-arg BRANCH=$BRANCH \
                    --build-arg BUILD=$BUILD \
                    --build-arg REPO=$REPO \
                    --platform linux/amd64,linux/arm64 \
                    --tag ${TAG} --push .
if [ $ALL -eq 1 ]; then
    for stage in `grep FROM Dockerfile | awk '{print $(NF)}' | sed '$d'`; do 
        docker buildx build --target $stage --file Dockerfile \
                            --build-arg BRANCH=$BRANCH \
                            --build-arg BUILD=$BUILD \
                            --build-arg REPO=$REPO \
                            --platform linux/amd64,linux/arm64 \
                            --tag ${TAG}-$stage --push .
    done
fi

