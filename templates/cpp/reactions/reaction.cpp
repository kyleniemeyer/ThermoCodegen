#include <math.h>
#include <numeric>
#include <algorithm>
#include <sstream>
#include <iomanip>
#include <functional>
#include "reactions/${name}.h"
#include "tcgversion.h"

//-----------------------------------------------------------------------------
${name}::${name}()
: _name("${name}"), J(${J}),  _phases(${phases}), _C0(${C0}), _C(${C0}),_X(${C0}),
  _Mu(${C0}), _dMu_dT(${C0}), _dMu_dP(${C0}),_tmp_ik(${C0}),
  _A(J,0.), _dA_dT(_A), _dA_dP(_A), _dA_dC(J,${C0}),
  _M(${M}),
  _nu(${nu}),
  _nu_m(${nu_m}),
  ${parameter_container}
  ${init_parameters}
{
  // allocate  temporary vectors for Gamma and _dMu_dC construct
  N = _phases.size();
  _tmp.resize(N);
  _dMu_dC.resize(N);
  _tmp_dC.resize(N);
  for (int i = 0; i < N; i++)
  {
    int K = _C0[i].size();
    _dMu_dC[i].resize(K);
    _tmp_dC[i].resize(K);
    for (int k = 0; k < K; k++)
    {
       _dMu_dC[i][k].resize(K, 0.0);
       _tmp_dC[i][k].resize(K, 0.0);
    }
  }

  // Set parameter values in map container
  ${init_parameter_container}
}
//-----------------------------------------------------------------------------
${name}::~${name}()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
std::string ${name}::name() const
{
  return _name;
}
//-----------------------------------------------------------------------------
std::string ${name}::tcg_build_version()
{
    return TCG_VERSION;
}
//-----------------------------------------------------------------------------
std::string ${name}::tcg_build_git_sha()
{
    return TCG_GIT_SHA;
}
//-----------------------------------------------------------------------------
std::string ${name}::tcg_generation_version()
{
    return "${tcg_version}";
}
//-----------------------------------------------------------------------------
std::string ${name}::tcg_generation_git_sha()
{
    return "${tcg_git_sha}";
}
//-----------------------------------------------------------------------------
 std::vector<std::shared_ptr<Phase> > ${name}::phases() const
{
  return _phases;
}
//-----------------------------------------------------------------------------
std::vector<std::vector<double> > ${name}::zero_C() const
{
  return _C0;
}
//-----------------------------------------------------------------------------
std::vector<std::vector<double> > ${name}::M() const
{
  return _M;
}
//-----------------------------------------------------------------------------
std::vector<std::vector<std::vector<double> > > ${name}::nu() const
{
  return _nu;
}
//-----------------------------------------------------------------------------
std::vector<std::vector<std::vector<double> > > ${name}::nu_m() const
{
  return _nu_m;
}
//-----------------------------------------------------------------------------
// pass by reference interface
void ${name}::A(
    const double &T, const double &P,
		std::vector<std::vector<double> > &C, std::vector<double> &_A) const
{
  //Calculate chemical potential matrix M_ik
  Mu(T, P, C, _Mu);

  _A = {
  ${A}};
}
//-----------------------------------------------------------------------------
// return  vector<double>
std::vector<double> ${name}::A(
    const double &T, const double &P,
    std::vector<std::vector<double> > &C) const
{
  A(T, P, C, _A);
  return _A;
}

//-----------------------------------------------------------------------------
// overloaded version to just return the jth affinity A_j
double ${name}::A(
    const double &T, const double &P,
    std::vector<std::vector<double> > &C, const int j) const
{
  // FIXME:: this call should probably should be deprecated
  // it is no longer calling function A(T,P,C,j) just calling void A and returning
  // _A[j] just to keep interface and consistency with new affinity functions
  A(T, P, C, _A);
  return _A[j];
}
//-----------------------------------------------------------------------------
// pass by reference interface
void ${name}::dA_dT(
    const double &T, const double &P,
		std::vector<std::vector<double> > &C, std::vector<double> &_dA_dT) const
{
  //Calculate chemical potential matrix dMu_dT_ik
  dMu_dT(T, P, C, _dMu_dT);

  _dA_dT = {
  ${dA_dT}};
}
//-----------------------------------------------------------------------------
std::vector<double> ${name}::dA_dT(
    const double &T, const double &P,
    std::vector<std::vector<double> > &C) const
{
  dA_dT(T, P, C, _dA_dT);
  return _dA_dT;
}

//-----------------------------------------------------------------------------
double ${name}::dA_dT(
    const double &T, const double &P,
    std::vector<std::vector<double> > &C, const int j) const
{
  //FIXME:  now redundant to above call but just returns one scalar...not useful
  dA_dT(T, P, C, _dA_dT);
  return _dA_dT[j];
}
//-----------------------------------------------------------------------------
// pass by reference interface
void ${name}::dA_dP(
    const double &T, const double &P,
		std::vector<std::vector<double> > &C, std::vector<double> &_dA_dP) const
{
  //Calculate chemical potential matrix dMu_dP_ik
  dMu_dP(T, P, C, _dMu_dP);

  _dA_dP = {
  ${dA_dP}};
}
//-----------------------------------------------------------------------------
std::vector<double> ${name}::dA_dP(
    const double &T, const double &P,
    std::vector<std::vector<double> > &C) const
{
  dA_dP(T, P, C, _dA_dP);
  return _dA_dP;
}
//-----------------------------------------------------------------------------
double ${name}::dA_dP(
    const double &T, const double &P,
    std::vector<std::vector<double> > &C, const int j) const
{
  //FIXME:  now redundant to above call but just returns one scalar...not useful
  dA_dP(T, P, C, _dA_dP);
  return _dA_dP[j];
}
//-----------------------------------------------------------------------------
void ${name}::dA_dC(
    const double &T, const double &P,
    std::vector<std::vector<double> >& C,
    std::vector<std::vector<std::vector<double> > >& _dA_dC) const
{
    dMu_dC(T, P, C, _dMu_dC);
    // loop over reactions
    for (int j=0; j < J; j++)
    {
        // loop over phases
        for (int i = 0; i< N; i++)
        {   // zero _dA_dC
            _dA_dC[j][i] = _C0[i];
            int K = _C0[i].size();
            for (int k = 0; k< K; k++)
            {   //FIXME: maybe make this a sparse matvec without the if statement?
                //note for affinities this should use _nu not _nu_m
                if (_nu[j][i][k] != 0.0)
                {
                    for (int l = 0; l < K; l++)
                    {   //FIXME:  could also think about stl: transform here
                        _dA_dC[j][i][l] -= _nu[j][i][k]*_dMu_dC[i][k][l];
                    }
                }
            }
        }
    }
}
//-----------------------------------------------------------------------------
std::vector<std::vector<std::vector<double> > > ${name}::dA_dC(
    const double &T, const double &P,
    std::vector<std::vector<double> >& C) const
{
  dA_dC( T, P, C, _dA_dC);
  return _dA_dC;
}

//-----------------------------------------------------------------------------
double ${name}::dAj_dCik(
    const double &T, const double &P,
	  std::vector<std::vector<double> >& C,
		unsigned j, unsigned  i, unsigned k) const
{
  //FIXME: should be deprecated,  removed old function call version as its incorrect,  now is just redundant
  // to previous call but just returns one component
  dA_dC( T, P, C, _dA_dC);
  return _dA_dC[j][i][k];
}
//-----------------------------------------------------------------------------
void ${name}::Gamma_i(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi, std::vector<double> &_Gamma) const
{
  //calculate current affinities
  A(T, P, C, _A);
  _Gamma = {
  ${Gamma_phases}};
}
//-----------------------------------------------------------------------------
std::vector<double> ${name}::Gamma_i(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi) const
{
  A(T, P, C, _A);
  Gamma_i(T, P, C, Phi, _tmp);
  return _tmp;
}
//-----------------------------------------------------------------------------
double ${name}::Gamma_i(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi, const int i) const
{
  ${Gamma_i}
}
//-----------------------------------------------------------------------------
void ${name}::dGamma_i_dT(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi, std::vector<double>& _dGamma) const
{
  A(T, P, C, _A);
  dA_dT(T, P, C, _dA_dT);
  _dGamma = {
  ${dGamma_phases_dT}};
}
//-----------------------------------------------------------------------------
std::vector<double> ${name}::dGamma_i_dT(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi) const
{
  dGamma_i_dT (T, P, C, Phi, _tmp);
  return _tmp;
}
//-----------------------------------------------------------------------------
double ${name}::dGamma_i_dT(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi, const int i) const
{
  A(T, P, C, _A);
  dA_dT(T, P, C, _dA_dT);
  ${dGamma_i_dT}
}
//-----------------------------------------------------------------------------
void ${name}::dGamma_i_dP(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi, std::vector<double>& _dGamma) const
{
  A(T, P, C, _A);
  dA_dP(T, P, C, _dA_dP);
  _dGamma = {
  ${dGamma_phases_dP}};
}
//-----------------------------------------------------------------------------
std::vector<double> ${name}::dGamma_i_dP(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi) const
{
  dGamma_i_dP(T, P, C, Phi, _tmp);
  return _tmp;
}
//-----------------------------------------------------------------------------
double ${name}::dGamma_i_dP(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi, const int i) const
{
  A(T, P, C, _A);
  dA_dP(T, P, C, _dA_dP);
  ${dGamma_i_dP}
}

//-----------------------------------------------------------------------------
void  ${name}::dGamma_i_dC(
    const double &T, const double &P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi,
    std::vector<std::vector<std::vector<double> > >& _dGamma) const
{
  A(T, P, C, _A);
  dA_dC(T, P, C, _dA_dC);
  _dGamma = {
  ${dGamma_phases_dC}};
}
//-----------------------------------------------------------------------------
std::vector<std::vector<std::vector<double> > > ${name}::dGamma_i_dC(
    const double &T, const double &P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi) const
{
  dGamma_i_dC( T, P, C, Phi, _tmp_dC);
  return _tmp_dC;
}
//-----------------------------------------------------------------------------
double ${name}::dGamma_i_dC(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi,
    unsigned i, unsigned l, unsigned k) const
{
  A(T, P, C, _A);
  dA_dC(T, P, C, _dA_dC);
  ${dGamma_i_dC}
}
//-----------------------------------------------------------------------------
void ${name}::dGamma_i_dPhi(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi, std::vector<std::vector<double> >& _dGamma) const
{
  A(T, P, C, _A);
   _dGamma = {
  ${dGamma_phases_dPhi}};
}
//-----------------------------------------------------------------------------
std::vector<std::vector<double> > ${name}::dGamma_i_dPhi(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi) const
{
  A(T, P, C, _A);
  std::vector<std::vector<double> > _dGamma_dPhi = {
  ${dGamma_phases_dPhi}};
  return _dGamma_dPhi;
}
//-----------------------------------------------------------------------------
double ${name}::dGamma_i_dPhi(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi,
    unsigned i, unsigned l) const
{
  A(T, P, C, _A);
  ${dGamma_i_dPhi}
}
//-----------------------------------------------------------------------------
void ${name}::Gamma_ik(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi,
    std::vector<std::vector<double> > &_Gamma) const
{
  A(T, P, C, _A);
  _Gamma = {
  ${Gamma_components}};
}
//-----------------------------------------------------------------------------
std::vector<std::vector<double> > ${name}::Gamma_ik(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi) const
{
  Gamma_ik(T, P, C, Phi, _tmp_ik);
  return _tmp_ik;
}
//-----------------------------------------------------------------------------
double ${name}::Gamma_ik(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi,
    const int i, const int k) const
{
  A(T, P, C, _A);
  ${Gamma_ik}
}
//-----------------------------------------------------------------------------
void  ${name}::dGamma_ik_dT(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi, std::vector<std::vector<double> >& _dGamma ) const
{
  A(T, P, C, _A);
  dA_dT(T, P, C, _dA_dT);
  _dGamma = {
  ${dGamma_components_dT}};
}
//-----------------------------------------------------------------------------
std::vector<std::vector<double> > ${name}::dGamma_ik_dT(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi) const
{
  //FIXME: using_tmp_ik but starting to worry about thread safety
  dGamma_ik_dT(T, P, C, Phi, _tmp_ik);
  return _tmp_ik;
}
//-----------------------------------------------------------------------------
double ${name}::dGamma_ik_dT(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi,
    const int i, const int k) const
{
  A(T, P, C, _A);
  dA_dT(T, P, C, _dA_dT);
  ${dGamma_ik_dT}
}
//-----------------------------------------------------------------------------
void  ${name}::dGamma_ik_dP(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi, std::vector<std::vector<double> >& _dGamma ) const
{
  A(T, P, C, _A);
  dA_dP(T, P, C, _dA_dP);
  _dGamma = {
  ${dGamma_components_dP}};
}
//-----------------------------------------------------------------------------
std::vector<std::vector<double> >  ${name}::dGamma_ik_dP(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi) const
{
  //FIXME: thread safety again?
  dGamma_ik_dP(T, P, C, Phi, _tmp_ik);
  return _tmp_ik;
}
//-----------------------------------------------------------------------------
double ${name}::dGamma_ik_dP(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi,
    const int i, const int k) const
{
  A(T, P, C, _A);
  dA_dP(T, P, C, _dA_dP);
  ${dGamma_ik_dP}
}
//-----------------------------------------------------------------------------
void ${name}::dGamma_ik_dC(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi,
    int i, std::vector<std::vector<double> >& _dGamma) const
{
  A(T, P, C, _A);
  dA_dC(T, P, C, _dA_dC);

  ${dGamma_ik_dC_i}
}
//-----------------------------------------------------------------------------
std::vector<std::vector<double> > ${name}::dGamma_ik_dC(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi, int i) const
{
  int K = _C0[i].size();
  std::vector<std::vector<double> > _dGamma(K,std::vector<double>(K,0.));
  dGamma_ik_dC(T, P, C, Phi, i, _dGamma);
  return _dGamma;
}
//-----------------------------------------------------------------------------
void ${name}::dGamma_ik_dPhi(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi,
    int i, std::vector<std::vector<double> >& _dGamma) const
{
  A(T, P, C, _A);
  dA_dC(T, P, C, _dA_dC);
  ${dGamma_ik_dPhi_i}
}
//-----------------------------------------------------------------------------
std::vector<std::vector<double> > ${name}::dGamma_ik_dPhi(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi, int i) const
{
  int K = _C0[i].size();
  std::vector<std::vector<double> > _dGamma(K,std::vector<double>(N,0.));
  dGamma_ik_dPhi(T, P, C, Phi, i, _dGamma);
  return _dGamma;
}

//-----------------------------------------------------------------------------
double ${name}::dGamma_ik_dC(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi,
    unsigned i, unsigned k, unsigned l, unsigned m) const
{
  A(T, P, C, _A);
  dA_dC(T, P, C, _dA_dC);
  ${dGamma_ik_dC}
}
//-----------------------------------------------------------------------------
double ${name}::dGamma_ik_dPhi(
    const double& T, const double& P,
    std::vector<std::vector<double> >& C,
    std::vector<double>& Phi,
    unsigned i, unsigned k, unsigned l) const
{
  A(T, P, C, _A);
  ${dGamma_ik_dPhi}
}
//-----------------------------------------------------------------------------
void ${name}::rho(const double &T, const double &P,
		std::vector<std::vector<double> >& C,
        std::vector<double>&  _rho) const
{
  // inplace calculation of vector of phase densities (must be correct size)
  for (int i = 0; i < _phases.size(); i++ ) {
    _rho[i] = _phases[i]->rho(T,P,C[i]);
  }
}
//-----------------------------------------------------------------------------
std::vector<double> ${name}::rho(
    const double &T, const double &P,
    std::vector<std::vector<double> >& C) const
{
  // return vector of densities
  std::vector<double> _rho(${Nphases},0.);
  rho(T, P, C, _rho);
  return _rho;
}
//-----------------------------------------------------------------------------
void ${name}::Cp(
    const double &T, const double &P,
		std::vector<std::vector<double> >& C, std::vector<double>&  _Cp) const
{
  // inplace calculation of vector of  phase densities (must be correct size)
  // Convert C to X
  C_to_X(C,_X);

  for (int i = 0; i < _phases.size(); i++ ) {
    _Cp[i] = _phases[i]->cp(T,P,_X[i])/_phases[i]->Mass(_X[i]);
  }
}
//-----------------------------------------------------------------------------
std::vector<double> ${name}::Cp(
    const double &T, const double &P,
    std::vector<std::vector<double> >& C) const
{

  // return vector of densities
  std::vector<double> _Cp(${Nphases},0.);
  Cp(T, P, C, _Cp);
  return _Cp;
}
//-----------------------------------------------------------------------------
void ${name}::s(
    const double &T, const double &P,
    std::vector<std::vector<double> >& C,
    std::vector<double>&  _s) const
{
  // inplace calculation of vector of  phase densities (must be correct size)

  // Convert C to X
  C_to_X(C,_X);

  for (int i = 0; i < _phases.size(); i++)
  {
    _s[i] = _phases[i]->s(T,P,_X[i])/_phases[i]->Mass(_X[i]);
  }
}
//-----------------------------------------------------------------------------
std::vector<double> ${name}::s(
    const double &T, const double &P,
    std::vector<std::vector<double> >& C) const
{
  // return vector of densities
  std::vector<double> _s(${Nphases},0.);
  s(T, P, C, _s);
  return _s;
}
//-----------------------------------------------------------------------------
void ${name}::ds_dC(
                    const double &T, const double &P,
                    std::vector<std::vector<double> >& C,
                    std::vector<std::vector<double> >&  _ds) const
{
    // inplace calculation of derivatives of entropy/unit mass with phase composition
    
    // Convert C to X
    C_to_X(C,_X);
    // zero out all derivatives
    _ds = _C0;
    for (int i = 0; i < _phases.size(); i++)
    {
        if (C[i].size() > 1)
        {
            _ds[i] = _phases[i]->ds_dc(T,P,C[i]);
            double cTiM = std::inner_product(C[i].begin(), C[i].end(),_M[i].begin(), 0.,
                                             std::plus<double>(), std::divides<double>());
            double Mass = std::inner_product(_X[i].begin(), _X[i].end(),_M[i].begin(), 0.);
            double s = _phases[i]->s(T,P,_X[i]);
            for (int k = 0; k < C[i].size(); k++)
            {
                _ds[i][k] = (_ds[i][k] - (s / cTiM)*(1./Mass - 1./_M[i][k]) )/Mass;
            }
        }
    }
}
//-----------------------------------------------------------------------------
std::vector<std::vector<double> > ${name}::ds_dC(
                                                 const double &T, const double &P,
                                                 std::vector<std::vector<double> >& C) const
{
    ds_dC(T, P, C, _tmp_ik);
    return _tmp_ik;
}
//-----------------------------------------------------------------------------
void ${name}::alpha(
    const double &T, const double &P,
		std::vector<std::vector<double> >& C,
    std::vector<double>&  _alpha) const
{
  // inplace calculation of vector of  phase alphas (must be correct size)

  // Convert C to X
  C_to_X(C,_X);
  for (int i = 0; i < _phases.size(); i++)
  {
    _alpha[i] = _phases[i]->alpha(T,P,_X[i]);
  }
}
//-----------------------------------------------------------------------------
std::vector<double> ${name}::alpha(
    const double &T, const double &P,
    std::vector<std::vector<double> >& C) const
{
  // return vector of densities
  std::vector<double> _alpha(${Nphases},0.);
  alpha(T, P, C, _alpha);
  return _alpha;

}
//-----------------------------------------------------------------------------
void ${name}::beta(
    const double &T, const double &P,
		std::vector<std::vector<double> >& C,
    std::vector<double>&  _beta) const
{
  // inplace calculation of vector of  phase alphas (must be correct size)

  // Convert C to X
  C_to_X(C,_X);
  for (int i = 0; i < _phases.size(); i++)
  {
    _beta[i] = _phases[i]->beta(T,P,_X[i]);
  }
}
//-----------------------------------------------------------------------------
std::vector<double> ${name}::beta(
    const double &T, const double &P,
    std::vector<std::vector<double> >& C) const
{
  // return vector of densities
  std::vector<double> _beta(${Nphases},0.);
  beta(T, P, C, _beta);
  return _beta;

}

//-----------------------------------------------------------------------------
void ${name}::Mu(
    const double &T, const double &P,
		std::vector<std::vector<double> >& C,
    std::vector<std::vector<double> >& _mu) const
{
  // inplace calculation of "Matrix" of chemical potentials Mu_i^k as vector of vectors (must be correct size)

  // Convert C to X
  C_to_X(C,_X);

  // loop over phases and insert chemical potential vectors

  for (int i = 0; i < _phases.size(); i++)
  {
    _mu[i] = _phases[i]->mu(T,P,_X[i]);
  }
}
//-----------------------------------------------------------------------------
std::vector<std::vector<double> > ${name}::Mu(
    const double &T, const double &P,
		std::vector<std::vector<double> >& C) const
{
  // set and return private _Mu
  Mu(T,P,C,_Mu);
  return _Mu;
}

//-----------------------------------------------------------------------------
void ${name}::dMu_dT(
    const double &T, const double &P,
		std::vector<std::vector<double> >& C,
    std::vector<std::vector<double> >& _dmu) const
{
  // inplace calculation of "Matrix" of chemical potentials Mu_i^k as vector of vectors (must be correct size)

  // Convert C to X
  C_to_X(C,_X);

  // loop over phases and insert chemical potential vectors

  for (int i = 0; i < _phases.size(); i++)
  {
    _dmu[i] = _phases[i]->dmu_dT(T,P,_X[i]);
  }
}
//-----------------------------------------------------------------------------
void ${name}::dMu_dP(
    const double &T, const double &P,
		std::vector<std::vector<double> >& C,
    std::vector<std::vector<double> >& _dmu) const
{
  // inplace calculation of "Matrix" of chemical potentials Mu_i^k as vector of vectors (must be correct size)

  // Convert C to X
  C_to_X(C,_X);

  // loop over phases and insert chemical potential vectors

  for (int i = 0; i < _phases.size(); i++)
  {
    _dmu[i] = _phases[i]->dmu_dP(T,P,_X[i]);
  }
}

//-----------------------------------------------------------------------------
void ${name}::dMu_dC(
    const double &T, const double &P,
		std::vector<std::vector<double> >& C,
    std::vector<std::vector<std::vector<double> > >& _dmu) const
{
  // inplace calculation of "Matrix" of chemical potentials Mu_i^k as vector of vectors (must be correct size)

  // loop over phases and insert chemical potential vectors

  for (int i = 0; i < _phases.size(); i++)
  {
    _dmu[i] = _phases[i]->dmu_dc(T,P,C[i]);
  }
}
//-----------------------------------------------------------------------------
std::vector<std::vector<std::vector<double> > > ${name}::dMu_dC(
    const double &T, const double &P,
		std::vector<std::vector<double> >& C) const
{
  // set and return private _Mu
  dMu_dC(T,P,C,_dMu_dC);
  return _dMu_dC;
}
//-----------------------------------------------------------------------------
void ${name}::C_to_X(
    std::vector<std::vector<double> >& C,
    std::vector<std::vector<double> > &X) const
{
  //convert from weight fractions to Mole fractions (assumes Containers exist for both)
  for (int i = 0; i < _phases.size(); i++)
  {
    X[i] = _phases[i]->c_to_x(C[i]);
  }
}
//-----------------------------------------------------------------------------
void ${name}::X_to_C(
    std::vector<std::vector<double> >& X,
    std::vector<std::vector<double> > &C) const
{
  //convert from Mole fractions to weight fractions (assumes Containers exist for both)
  for (int i = 0; i < _phases.size(); i++)
  {
    C[i] = _phases[i]->x_to_c(X[i]);
  }
}
//-----------------------------------------------------------------------------
std::vector<std::vector<double> > ${name}::C_to_X(std::vector<std::vector<double> > &C) const
{
  //convert from weight fractions to Mole fractions and return X "matrix"
  for (int i = 0; i < _phases.size(); i++)
  {
    _C[i] = _phases[i]->c_to_x(C[i]);
  }
  return _C;
}
//-----------------------------------------------------------------------------
std::vector<std::vector<double> > ${name}::X_to_C(std::vector<std::vector<double> > &X) const
{
  //convert from Mole fractions to weight fractions and return C "matrix"
  for (int i = 0; i < _phases.size(); i++)
  {
    _C[i] = _phases[i]->x_to_c(X[i]);
  }
  return _C;
}
//-----------------------------------------------------------------------------
void ${name}::set_parameter(const std::string& p, const double& val) const
{
  *parameters[p] = val;
}
//-----------------------------------------------------------------------------
void ${name}::get_parameter(const std::string& p) const
{
  std::cout << p << " = " << *parameters[p] << std::endl;
}
//-----------------------------------------------------------------------------
void ${name}::list_parameters() const
{
  std::cout << "Parameters: \n" << std::endl;
  for (auto const& x : parameters)
  {
    std::cout << x.first << " = "  << *x.second << std::endl;
  }
}
//-----------------------------------------------------------------------------
void  ${name}::report() const
{
  // loop over phases and endmemembers and spit out the names in order
  std::cout << "Reaction object: ${name}" << std::endl <<std::endl;
  for (int i = 0; i < _phases.size(); i++)
  {
    std::cout << "Phase " << i << " " << _phases[i]->name() << " (" << _phases[i]->abbrev() << ")" << std::endl;
    const std::vector<std::shared_ptr<EndMember> >& endmembers = _phases[i]->endmembers();
    for (int k = 0; k < endmembers.size(); k++)
    {
      std::cout << "     Endmember " << k << " " << endmembers[k]->name() 
                << " : " << endmembers[k]->formula() << "_(" << _phases[i]->abbrev() << ")"
                << std::endl;
    }
  }
  std::cout << std::endl;
  
  for (int j = 0; j < _nu.size(); j++)
  {
    std::cout << "Reaction " << j << std::endl;
    std::vector<std::string> reactants, products;
    for (int i = 0; i < _phases.size(); i++)
    {
      const std::vector<std::shared_ptr<EndMember> >& endmembers = _phases[i]->endmembers();
      for (int k = 0; k < endmembers.size(); k++)
      {
        if (_nu[j][i][k] != 0.0)
        {
          std::ostringstream ss;
          std::string nu_s = _to_string(std::abs(_nu[j][i][k]));
          if (nu_s != "1")
          {
            ss << nu_s << " ";
          }
          ss << endmembers[k]->formula() << "_(" << _phases[i]->abbrev() << ")";

          if (_nu[j][i][k] < 0)
          {
             reactants.push_back(ss.str());
          }
          else if (_nu[j][i][k] > 0)
          {
             products.push_back(ss.str());
          }
        }
      }
    }
    std::string reactants_str = std::accumulate(++reactants.begin(), reactants.end(), reactants[0], 
                                                [](const std::string a, const std::string b){
                                                         return a + " + " + b;
                                                });
    std::string products_str = std::accumulate(++products.begin(), products.end(), products[0],
                                                [](const std::string a, const std::string b){
                                                         return a + " + " + b;
                                                });
    std::cout << "     " << reactants_str << " -> " << products_str <<std::endl;
  }
}
//-----------------------------------------------------------------------------
std::string ${name}::_to_string(const double& d) const
{
  std::string str = std::to_string(d);
  std::size_t finish = str.find_last_not_of("0");
  if (finish!=std::string::npos)
  {
    str.erase(finish + 1);
  }
  if (str.back()=='.')
  {
    str.pop_back();
  }
  return str;
}
//-----------------------------------------------------------------------------
