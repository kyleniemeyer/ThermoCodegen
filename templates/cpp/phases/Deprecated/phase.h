#ifndef ${phasename_uppercase}_H
#define ${phasename_uppercase}_H

#include "Phase.h"
#include "endmembers.h"

class ${phasename}: public Phase
{
public:
  // Constructor
  ${phasename}();

  // Destructor
  virtual ~${phasename}();

  // phase name
  std::string name() const;

  // Vector of molecular weights
  std::vector<double> get_M() const;

  //return pointers to endmembers
  std::vector<std::shared_ptr<EndMember> > endmembers() const;

  // Gibbs free energy
  double G(const double& T, const double& P,
           const std::vector<double>& n) const;

  // dGdT
  double dGdT(const double& T, const double& P,
              const std::vector<double>& n) const;

  // dGdP
  double dGdP(const double& T, const double& P,
              const std::vector<double>& n) const;

  // d2GdT2
  double d2GdT2(const double& T, const double& P,
                const std::vector<double>& n) const;

  // d2GdTdP
  double d2GdTdP(const double& T, const double& P,
                 const std::vector<double>& n) const;

  // d2GdP2
  double d2GdP2(const double& T, const double& P,
                const std::vector<double>& n) const;

  // Vector of chemical potentials for each component
  std::vector<double> mu(const double& T, const double& P,
                         const std::vector<double>& x) const;

  // dmu_dT
  std::vector<double> dmu_dT(const double& T, const double& P,
                             const std::vector<double>& x) const;

  // dmu_dP
  std::vector<double> dmu_dP(const double& T, const double& P,
                             const std::vector<double>& x) const;

  // dmu_dxi (where x[i] is the molar fraction of the ith component)
  std::vector<double> dmu_dxi(const double& T, const double& P,
                              const std::vector<double>& x,
                              const int &i) const;

  // d2mu_dPdxi
  std::vector<double> d2mu_dPdxi(const double& T, const double& P,
                                 const std::vector<double>& x,
                                 const int &i) const;

  // dmu_dci (where c[i] is the concentration of the ith component)
  std::vector<double> dmu_dci(const double& T, const double& P,
                              const std::vector<double>& c,
                              const int &i) const;

  // d2mu_dPdci
  std::vector<double> d2mu_dPdci(const double& T, const double& P,
                                 const std::vector<double>& c,
                                 const int &i) const;

  // Molar Mass
  double Mass(const std::vector<double>& x) const;

  // Molar Volume
  double V(const double& T, const double& P,
           const std::vector<double>& x) const;

  // Molar Entropy
  double s(const double& T, const double& P,
           const std::vector<double>& x) const;

  // Molar Thermal expansivity
  double alpha(const double& T, const double& P,
               const std::vector<double>& x) const;

  // Molar Heat capacity
  double Cp(const double& T, const double& P,
            const std::vector<double>& x) const;

  // Mean density of the phase in mass units
  double rho(const double& T, const double& P,
             const std::vector<double>& c) const;

  // drho_dT
  double drho_dT(const double& T, const double& P,
                 const std::vector<double>& c) const;

  // drho_dP
  double drho_dP(const double& T, const double& P,
                 const std::vector<double>& c) const;

  // drho_dci
  double drho_dci(const double& T, const double& P,
                  const std::vector<double>& c,
                  const int& i) const;

  // Returns weight percent concentrations given mole fraction
  std::vector<double> c_to_x(const std::vector<double> &c) const;

  // Returns  mole fraction given weight percent concentrations
  std::vector<double> x_to_c(const std::vector<double> &x) const;

  // Calculate derivative of vector x with respect to component x[i]
  std::vector<double> dx_dxi(const std::vector<double>& x,
                             const double& i) const;

  // Calculates derivative dx_k/dc_l of x[k] with c[l]
  double dxk_dcl(const std::vector<double>& c, const int &k,
                 const int &l) const;

private:
  // phase name
  const std::string _name;

  // Vector of pointers to endmembers
  std::vector<std::shared_ptr<EndMember> > _endmembers;

  // Vector of molecular masses
  std::vector<double> M;

  // Number of components
  int C;
};

#endif
