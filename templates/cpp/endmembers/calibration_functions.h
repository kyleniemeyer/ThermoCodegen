  // d${param}_G
  double d${param}_G(const double& T, const double& P);

  // d${param}_dGdT
  double d${param}_dGdT(const double& T, const double& P);

  // d${param}_dGdP
  double d${param}_dGdP(const double& T, const double& P);

  // d${param}_d2GdT2
  double d${param}_d2GdT2(const double& T, const double& P);

  // d${param}_d2GdTdP
  double d${param}_d2GdTdP(const double& T, const double& P);

  // d${param}_d2GdP2
  double d${param}_d2GdP2(const double& T, const double& P);

  // d${param}_d3GdT3
  double d${param}_d3GdT3(const double& T, const double& P);

  // d${param}_d3GdT2dP
  double d${param}_d3GdT2dP(const double& T, const double& P);

  // d${param}_d3GdTdP2
  double d${param}_d3GdTdP2(const double& T, const double& P);

  // d${param}_d3GdP3
  double d${param}_d3GdP3(const double& T, const double& P);
