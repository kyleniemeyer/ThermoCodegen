#include "endmembers/${endmembername}.h"
#include "tcgversion.h"

//-----------------------------------------------------------------------------
${endmembername}::${endmembername}()
{
  // Do Nothing
}
//-----------------------------------------------------------------------------
${endmembername}::~${endmembername}()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
std::string ${endmembername}::identifier()
{
    std::string _str(${C_name}_calib_identifier());
    return _str;
}
//-----------------------------------------------------------------------------
std::string ${endmembername}::name()
{
    std::string _str(${C_name}_calib_name());
    return _str;
}
//-----------------------------------------------------------------------------
std::string ${endmembername}::tcg_build_version()
{
    return TCG_VERSION;
}
//-----------------------------------------------------------------------------
std::string ${endmembername}::tcg_build_git_sha()
{
    return TCG_GIT_SHA;
}
//-----------------------------------------------------------------------------
std::string ${endmembername}::tcg_generation_version()
{
    return "${tcg_version}";
}
//-----------------------------------------------------------------------------
std::string ${endmembername}::tcg_generation_git_sha()
{
    return "${tcg_git_sha}";
}
//-----------------------------------------------------------------------------
std::string ${endmembername}::formula()
{
    std::string _str(${C_name}_calib_formula());
    return _str;
}
//-----------------------------------------------------------------------------
double ${endmembername}::molecular_weight()
{
    return ${C_name}_calib_mw();
}
//-----------------------------------------------------------------------------
std::vector<double> ${endmembername}::elements()
{
  std::vector<double> _elements;
  const double *el = ${C_name}_calib_elements();
  _elements.assign(el, el + 106);
  return _elements;
}
//-----------------------------------------------------------------------------
double ${endmembername}::G(const double &T, const double &P)
{
  return ${C_name}_calib_g(T,P);
}
//-----------------------------------------------------------------------------
double ${endmembername}::dGdT(const double &T, const double &P)
{
  return ${C_name}_calib_dgdt(T,P);
}
//-----------------------------------------------------------------------------
double ${endmembername}::dGdP(const double &T, const double &P)
{
  return ${C_name}_calib_dgdp(T,P);
}
//-----------------------------------------------------------------------------
double ${endmembername}::d2GdT2(const double &T, const double &P)
{
  return ${C_name}_calib_d2gdt2(T,P);
}
//-----------------------------------------------------------------------------
double ${endmembername}::d2GdTdP(const double &T, const double &P)
{
  return ${C_name}_calib_d2gdtdp(T,P);
}
//-----------------------------------------------------------------------------
double ${endmembername}::d2GdP2(const double &T, const double &P)
{
  return ${C_name}_calib_d2gdp2(T,P);
}
//-----------------------------------------------------------------------------
double ${endmembername}::d3GdT3(const double &T, const double &P)
{
  return ${C_name}_calib_d3gdt3(T,P);
}
//-----------------------------------------------------------------------------
double ${endmembername}::d3GdT2dP(const double &T, const double &P)
{
  return ${C_name}_calib_d3gdt2dp(T,P);
}
//-----------------------------------------------------------------------------
double ${endmembername}::d3GdTdP2(const double &T, const double &P)
{
  return ${C_name}_calib_d3gdtdp2(T,P);
}
//-----------------------------------------------------------------------------
double ${endmembername}::d3GdP3(const double &T, const double &P)
{
  return ${C_name}_calib_d3gdp3(T,P);
}
//**************************************************************************
// Convenience functions of T and P
//**************************************************************************

//-----------------------------------------------------------------------------
double ${endmembername}::S(const double& T, const double& P)
{
  return ${C_name}_calib_s(T,P);
}
//-----------------------------------------------------------------------------
double ${endmembername}::V(const double& T, const double& P)
{
  return ${C_name}_calib_v(T,P);
}
//-----------------------------------------------------------------------------
double ${endmembername}::dVdT(const double& T, const double& P)
{
  return ${C_name}_calib_d2gdtdp(T,P);
}
//-----------------------------------------------------------------------------
double ${endmembername}::dVdP(const double& T, const double& P)
{
  return ${C_name}_calib_d2gdp2(T,P);
}
//-----------------------------------------------------------------------------
double ${endmembername}::Cv(const double& T, const double& P)
{
  return ${C_name}_calib_cv(T,P);
}
//-----------------------------------------------------------------------------
double ${endmembername}::Cp(const double& T, const double& P)
{
  return ${C_name}_calib_cp(T,P);
}
//-----------------------------------------------------------------------------
double ${endmembername}::dCpdT(const double& T, const double& P)
{
  return ${C_name}_calib_dcpdt(T,P);
}
//-----------------------------------------------------------------------------
double ${endmembername}::alpha(const double& T, const double& P)
{
  return ${C_name}_calib_alpha(T,P);
}
//-----------------------------------------------------------------------------
double ${endmembername}::beta(const double& T, const double& P)
{
  return ${C_name}_calib_beta(T,P);
}
//-----------------------------------------------------------------------------
double ${endmembername}::K(const double& T, const double& P)
{
  return ${C_name}_calib_K(T,P);
}
//-----------------------------------------------------------------------------
double ${endmembername}::Kp(const double& T, const double& P)
{
  return ${C_name}_calib_Kp(T,P);
}
//**************************************************************************
// Active parameter functions directly from coder
//**************************************************************************

//-----------------------------------------------------------------------------
int ${endmembername}::get_param_number()
{
   return ${C_name}_get_param_number();
}
//-----------------------------------------------------------------------------
std::vector<std::string> ${endmembername}::get_param_names()
{
  std::vector<std::string> _param_names;
  const char **p = ${C_name}_get_param_names();
  _param_names.assign(p, p + ${C_name}_get_param_number());
  return _param_names;
}
//-----------------------------------------------------------------------------
std::vector<std::string> ${endmembername}::get_param_units()
{
  std::vector<std::string> _param_units;
  const char **p = ${C_name}_get_param_units();
  _param_units.assign(p, p + ${C_name}_get_param_number());
  return _param_units;
}
//-----------------------------------------------------------------------------
std::vector<double> ${endmembername}::get_param_values()
{
  std::vector<double> values(${C_name}_get_param_number());
  double* v = values.data();
  double** v_ptr = &v;
  ${C_name}_get_param_values(v_ptr);
  return values;
}
//-----------------------------------------------------------------------------
void ${endmembername}::get_param_values(std::vector<double>& values)
{
  double* v = values.data();
  double** v_ptr = &v;
  ${C_name}_get_param_values(v_ptr);
}
//-----------------------------------------------------------------------------
int ${endmembername}::set_param_values(std::vector<double>& values)
{
  ${C_name}_set_param_values(values.data());
  return 1;
}
//-----------------------------------------------------------------------------
double ${endmembername}::get_param_value(int& index)
{
  return ${C_name}_get_param_value(index);
}
//-----------------------------------------------------------------------------
int ${endmembername}::set_param_value(int& index, double& value)
{
  return ${C_name}_set_param_value(index,value);
}
//-----------------------------------------------------------------------------
double ${endmembername}::dparam_g(double& T, double& P, int& index)
{
  return ${C_name}_dparam_g(T,P,index);
}
//-----------------------------------------------------------------------------
double ${endmembername}::dparam_dgdt(double& T, double& P, int& index)
{
  return ${C_name}_dparam_dgdt(T,P,index);
}
//-----------------------------------------------------------------------------
double ${endmembername}::dparam_dgdp(double& T, double& P, int& index)
{
  return ${C_name}_dparam_dgdp(T,P,index);
}
//-----------------------------------------------------------------------------
double ${endmembername}::dparam_d2gdt2(double& T, double& P, int& index)
{
  return ${C_name}_dparam_d2gdt2(T,P,index);
}
//-----------------------------------------------------------------------------
double ${endmembername}::dparam_d2gdtdp(double& T, double& P, int& index)
{
  return ${C_name}_dparam_d2gdtdp(T,P,index);
}
//-----------------------------------------------------------------------------
double ${endmembername}::dparam_d2gdp2(double& T, double& P, int& index)
{
  return ${C_name}_dparam_d2gdp2(T,P,index);
}
//-----------------------------------------------------------------------------
double ${endmembername}::dparam_d3gdt3(double& T, double& P, int& index)
{
  return ${C_name}_dparam_d3gdt3(T,P,index);
}
//-----------------------------------------------------------------------------
double ${endmembername}::dparam_d3gdt2dp(double& T, double& P, int& index)
{
  return ${C_name}_dparam_d3gdt2dp(T,P,index);
}
//-----------------------------------------------------------------------------
double ${endmembername}::dparam_d3gdtdp2(double& T, double& P, int& index)
{
  return ${C_name}_dparam_d3gdtdp2(T,P,index);
}
//-----------------------------------------------------------------------------
double ${endmembername}::dparam_d3gdp3(double& T, double& P, int& index)
{
  return ${C_name}_dparam_d3gdp3(T,P,index);
}
//-----------------------------------------------------------------------------
