Serpentinization reactions
==========================

This example considers a single Olivine hydration reaction involved in serpentinization.

    Forsterite + |H2O| :math:`\rightarrow` Serpentine + Brucite.

This example is primarily to demonstrate a few advanced features of
ThermoCodegen including using Mark Ghiorso's Standard Water Interpolative Model (SWIM) code for the |H2O| endmember as well as exposing some
thermodynamic model parameters for calibration.


System Description
+++++++++++++++++++

.. some useful compositional macros

.. |Fo| replace:: Mg\ :sub:`2`\ SiO\ :sub:`4`
.. |H2O| replace:: H\ :sub:`2`\ O
.. |Ctl| replace:: Mg\ :sub:`3`\ (Si :sub:`2`\ O\ :sub:`5`)\ (OH)\ :sub:`4`
.. |Br| replace:: Mg(OH)\ :sub:`2`
.. |Fo_ol| replace:: |Fo| _(Ol)
.. |Ctl_sp| replace:: |Ctl|\ _(Sp)
.. |Br_Br| replace:: |Br| _(Br)



*Thermodynamic Database*
_________________________

This system is described by a custom **Thermodynamic Database** consisting of
four pure phases, constructed from four endmembers.  The solid phase endmembers are all based on
[Berman_1988]_ models, while the liquid water phase utilizes Ghiorso's SWIM model.

* Olivine (endmember: Forsterite, |Fo_ol|)
* Serpentine (endmember: Chrysotile, |Ctl_sp|)
* Brucite (endmember: Brucite |Br_Br|)
* Water (endmember: SWIM\_ |H2O|)

.. note::

    The SWIM water model for the pure |H2O| endmember is not described by a coder model,
    but is rather a C library that is linked at build time. To include the SWIM model in
    a thermodynamic database, use the ``--include_swim`` option in :doc:`tcg_builddb` (see below).

*Reaction Kinetics*
____________________

This model considers the single hydration reaction

    2 |Fo|  + 3 |H2O|  :math:`\rightarrow`  |Ctl| + |Br|


Thermodynamic models for solid endmembers, phases, and kinetic reactions are
described in a series of jupyter notebooks in the directory
:code:`Systems/fo_h2o/notebooks`  directory.  All paths and commands described below are
assumed to be relative to :code:`Systems/fo_h2o` (see :doc:`quickstart` for
specific instructions on extracting the ``Systems`` directory).

Modeling workflow
+++++++++++++++++

Quick build for the Fo-|H2O| system
___________________________________

To build and test all thermodynamic models execute the script ``build_system.sh`` in the directory ``Systems/fo_h2o``.

.. code-block:: bash

  bash ./build_system.sh

Details of ``build_system.sh``
++++++++++++++++++++++++++++++++

This section describes in detail the various phases of the build process.

Jupyter notebooks describing endmembers and phases
___________________________________________________

**Endmembers**: All solid endmembers are built from Berman models by the single notebook

*  :doc:`Generate_berman_endmembers.ipynb  <notebooks/fo_h2o/Generate_berman_endmembers>`

The SWIM |H2O| endmember is included in the build stage using :doc:`tcg_builddb`.

**Phases**: All pure phases are constructed using the notebook

* :doc:`Generate_phases.ipynb <notebooks/fo_h2o/Generate_phases>`

These notebooks can be run manually or executed as scripts using

.. code-block:: bash

  jupyter nbconvert --to notebook --execute notebooks/Generate_berman_endmembers.ipynb
  jupyter nbconvert --to notebook --execute notebooks/Generate_phases.ipynb


The endmember notebooks will generate a set of endmember ``.emml`` files and put them in the directory ``endmembers``.
The phase notebooks will generate a set of phase ``.phml`` files and put them in the directory ``phases``.

Build the thermodynamic database using :doc:`tcg_builddb` (and :doc:`tcg_dbparams`)
___________________________________________________________________________________

For this example we  build the database to include the SWIM library as well as expose some of
the parameters used to describe thermodynamic endmembers and phases for calibration.

Usage: from within the ``Systems/fo_h2o`` directory

.. code-block:: shell

  tcg_builddb --just_src --include_swim -zi database --calibfile calib_params.csv

which will generate the file ``database/fo_h2o.tar.gz``.  Here the option ``--include_swim`` links libSWIM
to the C++ library for use as a water endmember.

Calibration parameters
______________________

The option ``--calibfile calib_params.csv`` allows a subset of all thermodynamic model
parameters to be adjustable for calibration through the coder interfaces for getting and setting
parameters (e.g. see :doc:`py_endmember` and :doc:`py_phase`).  To view the full space of adjustable
parameters,  the script :doc:`tcg_dbparams` can be run to extract a pandas dataframe of all adjustable parameters.

.. code-block:: bash

    tcg_dbparams -l

provides a list of all adjustable parameters for each phase and endmember.

.. code-block:: bash

    name                   type                                            params
    Olivine                phase                                         [T_r, P_r]
    Forsterite_berman  endmember  [T_r, P_r, H_TrPr, S_TrPr, k0, k1, k2, k3, V_T...
    Water                  phase                                         [T_r, P_r]
    Brucite                phase                                         [T_r, P_r]
    Brucite_berman     endmember  [T_r, P_r, H_TrPr, S_TrPr, k0, k1, k2, k3, V_T...
    Chrysotile             phase                                         [T_r, P_r]
    Chrysotile_berman  endmember  [T_r, P_r, H_TrPr, S_TrPr, k0, k1, k2, k3, V_T...

or

.. code-block:: bash

    tcg_dbparams -s params

saves the parameters to ``params.csv``.  Invoking the build script

.. code-block:: bash

    tcg_builddb --just_src --include_swim -zi database --calibfile params.csv

makes all parameters adjustable.  To restrict the adjustable parameters to a subset, simply
edit the ``.csv`` file to include only the parameters that you wish to adjust (all the others
will be hard coded in during the code generation process).  For example,  the file ``calib_params.csv``
just includes

.. code-block:: bash

    name,type,params
    Olivine,phase,"['T_r', 'P_r']"
    Forsterite_berman,endmember,"['H_TrPr', 'S_TrPr' ]"
    Brucite,phase,"['T_r', 'P_r']"
    Brucite_berman,endmember,"['H_TrPr', 'S_TrPr']"
    Chrysotile,phase,"['T_r', 'P_r']"
    Chrysotile_berman,endmember,"['H_TrPr', 'S_TrPr']"

Which only allows the reference temperature and pressure to be adjusted in the phases, and the
enthalpy of formation ``H_TrPr`` and entropy of formation ``S_TrPr`` to be adjusted for the endmembers.

.. note::

    This set of parameters was chosen just for illustration and to test the calibration
    interfaces of the generated code.  A proper calibration would probably choose a
    more useful set of parameters.

Restricting the space of adjustable parameters reduces the total amount of code generated and
the number of floating point operations in the generated code.

Jupyter notebooks describing reactions
___________________________________________________

The jupyter notebook :doc:`Generate_reactions.ipynb <notebooks/fo_h2o/Generate_reactions>`
creates a ``.rxml`` file describing the single Olivine hydration reaction.

It is run using

.. code-block:: shell

  jupyter nbconvert --to notebook --execute notebooks/Generate_reactions.ipynb


Build the reaction object using :doc:`tcg_buildrx`
___________________________________________________

.. code-block:: bash

    cd reactions
    tcg_buildrx fo_h2o_hydration.rxml --include_swim -i


.. note::

    You must also use the ``--include_swim`` option in :doc:`tcg_buildrx` to include the
    SWIM library in the final compiled object.


Testing the models
__________________

At this point there should be a subdirectory in the ``reactions`` directory called
``fo_h2o_hydration`` which contains

* ``fo_h2o_hydration.module``
* ``fo_h2o_hydration.senv``
* ``include``
* ``lib``
* ``src``

If you are using environment modules, you can set your environment using

.. code-block:: bash

  module load ./reactions/fo_h2o_hydration/fo_h2o_hydration.module


Alternatively if you are using singularity,  you can leave and restart the shell with

.. code-block:: bash

  singularity shell --env-file <path to>/fo_h2o_hydration.senv <path to_singularity_sif-file>

The file ``fo_h2o_hydration.senv`` is a singularity environment file generated during
generation of the reaction object which adds the reaction  environment variables to the
container.

Running ``pytest``
******************

You can now test the python bindings with

.. code-block:: bash

  cd tests
  pytest --disable-warnings test*

Generating test files
**********************

The test files included in this example were generated with the jupyter notebook
:doc:`Generate_tests.ipynb <notebooks/fo_h2o/Generate_tests>` in the ``notebooks`` directory.  This notebook uses the
:doc:`Tester <testing>` class imported from ``thermocodegen.testing`` which
takes in a ThermoCodegen reaction or database object and tests all of its
methods against known input values.  Output of ``Tester``  includes
``pandas.DataFrames`` of output values as well as ``pytest`` test files.

.. note::

    The hydration tests also include tests for getting and setting parameters from the
    calibration code.


Jupyter Notebooks
+++++++++++++++++

Endmembers
__________

.. toctree::
  :maxdepth: 1

  Generate Berman endmembers <notebooks/fo_h2o/Generate_berman_endmembers>

Phases
______

.. toctree::
  :maxdepth: 1

  Generate phases <notebooks/fo_h2o/Generate_phases>


Reactions
_________

.. toctree::
  :maxdepth: 1

  Generate reactions <notebooks/fo_h2o/Generate_reactions>

Tests and Applications
______________________

.. toctree::
  :maxdepth: 1

  Generate tests <notebooks/fo_h2o/Generate_tests>
