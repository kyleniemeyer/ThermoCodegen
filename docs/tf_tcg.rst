TerraFERMA integration
=================================================

A primary design goal of ThermoCodegen is to  generate custom, simplified thermodynamic and kinetic reaction
models that can be  integrated into general geodynamics codes such as `TerraFERMA
<https://terraferma.github.io>`_ or `ASPECT
<https://aspect.geodynamics.org/>`_.  Here we demonstrate integration with `TerraFERMA
<https://terraferma.github.io>`_ by reproducing the
1-D isentropic melting column model in the  Fo-|Si| system, previously implemented in python in the jupyter notebook
:doc:`1-D isentropic melting columns <notebooks/fo_sio2/Isentropic_columns>`.


Model Description
++++++++++++++++++++

.. some useful compositional macros

.. |Si| replace:: SiO\ :sub:`2`
.. |Si2| replace:: Si\ :sub:`2`\ O\ :sub:`4`
.. |Fo| replace:: Mg\ :sub:`2`\ SiO\ :sub:`4`
.. |En| replace:: Mg\ SiO\ :sub:`3`
.. |Fo_ol| replace:: |Fo|\ :sup:`ol`
.. |Fo_lq| replace:: |Fo|\ :sup:`lq`
.. |Si_lq| replace:: |Si|\ :sup:`lq`
.. |Si2_lq| replace:: |Si2|\ :sup:`lq`
.. |En_opx| replace:: |En|\ :sup:`opx`
.. |Si_qz| replace:: |Si|\ :sup:`qz`


This problem uses TerraFERMA to solve the 1-D, steady-state isentropic upwelling problem without phase
separation described by the system of equations

.. math::


      \frac{d\ }{dz} F_i = \frac{1}{\bar{\rho}_0W_0}\Gamma_i

      \frac{d\ }{dz} F_i c_i^k = \frac{1}{\bar{\rho}_0W_0}\Gamma_i^k

      \sum_i F_i s_i  = S_0

      \frac{d P}{dz} = -\left(\sum_i \frac{F_i}{\rho_i}\right)^{-1} g

where :math:`F_i` is the mass fraction of phase :math:`i` and :math:`\Gamma_i` is its  corresponding mass transfer rate.
:math:`c_i^k` is the concentration in weight-percent of component :math:`k` in phase :math:`i` with corresponding
component mass-transfer rate :math:`\Gamma_i^k`.  For each phase :math:`i`, :math:`s_i` is the specific entropy and :math:`\rho_i`
the density. The mean density is

.. math::

      \bar{\rho} = \left(\sum_i \frac{F_i}{\rho_i}\right)^{-1}

At the bottom of the column,  the temperature and pressure are :math:`T_0,P_0`, the upwelling velocity is :math:`W_0` and
:math:`\bar{\rho}_0`  and :math:`S_0` are respectively the mean density and  total entropy of the mantle assemblage. :math:`P` is the pressure.

.. note::
    Because :math:`s_i(T,P,c_i^k)` are known functions of temperature, pressure and composition, the conservation of total
    entropy equation is actually an implicit constraint on the temperature :math:`T`.

Computational details:
_________________________

`TerraFERMA <https://terraferma.github.io>`_   provides a framework for  constructing  custom finite element models (built on the
`FEniCS <https://fenicsproject.org>`_ and `PETSc <https://petsc.org>`_ libraries) from a single hierarchical options file that
contains all the mathematical, computational and runtime choices required to build and run a compiled parallel custom executable.

For this problem,  we solve a dimensionless version of the equations above using piecewise constant (:math:`P0`) elements as a non-linear
variational problem using a simple backwards Euler integrator.  Full details of the model are beyond the scope of this tutorial
but can be found in the TerraFERMA markup language file ``1D-isentropic.tfml`` in the
``examples/Systems/fo_sio2/terraferma/1D-isentropic`` directory.

In particular,  the full coupled thermodynamic/geodynamic model is completely described in three XML files in the directory
``examples/Systems/fo_sio2/terraferma``

* ``reactions/fo_sio2_poly_linear_rxns.rxml``: A thermocodegen reaction file providing kinetics models for three melting reactions in the Fo-|Si| system, all built on the thermodynamic database available at `fo_sio2_db.tar.gz <https://zenodo.org/record/7976277>`_

* ``1D-isentropic/1D-isentropic.tfml``: A TerraFERMA markup language file describing the finite element model

* ``1D-isentropic/1D-isentropic.shml``: A TerraFERMA `simulation markup` language file that drives a parameter sweep as well as performs tests

All of these files can be viewed and edited using the ``diamond`` GUI available in the containers.



Modeling workflow
+++++++++++++++++

This demo will illustrate the workflow of a ThermoCodegen/TerraFERMA project using the available ``tf-focal`` docker image that
includes both software packages.

All paths and commands described below are
assumed to be relative to :code:`Systems/fo_sio2/terraferma` (see :doc:`quickstart` for
specific instructions on extracting the ``Systems`` directory).


Start a container
_________________

Following the instructions in :doc:`quickstart` start either a :doc:`docker<docker>` or interactive :doc:`singularity<singularity>`
shell container.  If using docker, proper X11 configuration will be necessary for some of the optional steps
below.

This example will use the two subdirectories ``reactions``, which contains information for building the ThermoCodegen
reaction objects, and ``1D-isentropic``, which contains the TerraFERMA model.

Generate ThermoCodegen Reaction Libraries
__________________________________________

Within the ``reactions`` directory is a single ThermoCodegen ``.rxml`` file describing three melting reactions over
the space of endmembers and phases in the `fo_sio2_db.tar.gz <https://zenodo.org/record/7976277>`_ thermodynamic database hosted at zenodo.

To view details of this file

.. code-block:: bash

    diamond reactions/fo_sio2_poly_linear_rxns.rxml &

which, if the container is configured correctly, should pop open a window

.. image:: images/Diamond-fo-si-rxml.png
  :width: 90%
  :align: center

.. note::
    The jupyter notebook :doc:`Generate_fo_sio2_poly_linear_rxns <notebooks/fo_sio2/Generate_fo_sio2_poly_linear_rxns>`
    provides details on this kinetic model.

To generate the compiled C++ reactions and phase library (together with python bindings), and then clean the build directory use

.. code-block:: bash

    cd reactions
    tcg_buildrx fo_sio2_poly_linear_rxns.rxml -i
    tcg_buildrx -c

this may take some time to build (see :doc:`tcg_buildrx` for more options).

Once the library is built and installed locally into the directory ``reactions/fo_sio2_poly_linear_rxns``,  load the environment module to set appropriate paths needed for TerraFERMA

.. code-block:: bash

    module load ./reactions/fo_sio2_poly_linear_rxns/*.module

Alternatively if you are using :doc:`singularity<singularity>`,  you can leave and restart the shell with

.. code-block:: bash

  singularity shell --cleanenv --env-file <path to>/fo_sio2_poly_linear_rxns.senv <path to>/thermocodegen_tf-focal.sif

The file ``fo_sio2_poly_linear_rxns.senv`` is a singularity environment file generated during
construction of the reaction object which adds the reaction  environment variables to the
container.


Run and Test TerraFERMA simulation
___________________________________

At this point the appropriate thermodynamic model has been built and made available for use in TerraFERMA.  Here we will
use it in the model described in the directory ``1D-isentropic`` which contains 4 files

* ``1D-isentropic.shml``
* ``1D-isentropic.tfml``
* ``PlotTFruns.ipynb``
* ``plots.mplstyle``

The actual TerraFERMA model is contained in ``1D-isentropic.tfml`` and the driver `simulationharness` file is ``1D-isentropic.shml``
(the other two files are for visualization).  To view or edit either file, use ``diamond``.

To build and run the model

.. code-block:: bash

    cd <path to>/1D-isentropic
    tfsimulationharness --test 1D-isentropic.shml

which will build the TerraFERMA executable from the ``.tfml`` file and then perform a parameter sweep over 3 values of the Damkohler number Da,
as well as check that the relative error in the total entropy is less than :math:`10^{-6}`.  On successful completion
the output should end with

.. code-block:: bash

    Maximum Errors
    Da = 100.:	 7.2995831601e-11
    Da = 1000.:	 4.7611170562e-10
    Da = 2000.:	 4.0117461397e-07
    all errors <= 1e-06
    1D-isentropic.shml: success.
    1D-isentropic.shml: Running finish_time:
    1D-isentropic.shml: success.
    1D-isentropic.shml: Running err_file:
    1D-isentropic.shml: success.
    1D-isentropic.shml: PPP
    Passes:   3
    Failures: 0


Visualize Model output
_______________________

Output for all the runs can be found in the directory hierarchy under ``1D-isentropic.tfml.run`` however for convenient
visualization there is also a jupyter notebook ``PlotTFruns.ipynb`` for generating PDFs.  The notebook can be run interactively
from the container (see :doc:`docker` or :doc:`singularity` for details on running jupyter-lab),  or can be run as a script

.. code-block:: bash

    jupyter nbconvert --to notebook --execute PlotTFruns.ipynb
    rm  *.nbconvert.ipynb

Output (`` Isentropic_TF_Da*_dz*.pdf``) for the near-equilibrium isentrope with :math:`Da=2000` should look like
(compare to ``Isentropic_TF_Da2000.0_dz0.001.pdf``)

.. image:: images/Isentropic_TF_Da2000.0_dz0.001.png
  :width: 90%
  :align: center


