Examples
=========

The usage and behavior of ThermoCodegen is best explored through specific examples.
Here we consider four simplified thermodynamic systems as well as an example of integrating ThermoCodegen generated
models into `TerraFERMA <https://terraferma.github.io>`_

.. toctree::
   :maxdepth: 1

   fo_fa
   fo_sio2
   Stixrude
   fo_h20
   tf_tcg

Scripts and jupyter notebooks for describing and building each system from scratch
are available in the supplied containers in the directory

.. code-block:: bash

   ${THERMOCODEGEN_HOME}/share/thermocodegen/examples/Systems

All of the examples above assume you have made a personal copy of this directory
and all of its  subdirectories  to a local directory ``Systems`` (see
:doc:`quickstart` for specific instructions for either :doc:`docker<docker>` or
:doc:`singularity<singularity>` containers).
