C++ API Reference
===================================

Thermodynamic object Classes
----------------------------

.. toctree::
   :maxdepth: 1

   endmember
   phase
   reaction
