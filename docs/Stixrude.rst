Stixrude Models for |MgFe| phases
=================================================

This demo describes code generation for a basic set of endmembers and phases in
the Mg-Fe-SiO\ :sub:`4` system as described by the models of [Stixrude_Lithgow-Bertelloni_2011]_
which are a useful subset for considering sub-solidus reactions at high pressure
for fully compressible convection calculations.

.. image:: ./images/Mg2SiO4_stixrude_phase_diagram.png
  :width: 90%

*Phase diagrams in the pure* |Fo| *system. (a) phase diagram from is from*  [Stixrude_Lithgow-Bertelloni_2011]_
*(b)  a simplified system generated using* ThermoCodegen *with just the 3* |Fo| *polymorphs, Forsterite,
MgWadsleyite, and MgRingwoodite*.


System Description
++++++++++++++++++++

.. some useful compositional macros

.. |MgFe| replace:: MgFeSiO\ :sub:`4`
.. |Fo| replace:: Mg\ :sub:`2`\ SiO\ :sub:`4`
.. |Fa| replace:: Fe\ :sub:`2`\ SiO\ :sub:`4`
.. |Fo_ol| replace:: |Fo|\ _(ol)
.. |Fa_ol| replace:: |Fa|\ _(ol)
.. |MgWd_Wd| replace:: |Fo|\ _(wd)
.. |MgRi_Ri| replace:: |Fo|\ _(ri)


*Thermodynamic Database*
_________________________

This system is described by a custom **Thermodynamic Database** consisting of
three pure  phases each constructed from a single endmember.

* Olivine (endmember: Forsterite, |Fo_ol|)
* Wadsleyite (endmember: MgWadsleyite, |MgWd_Wd|)
* Ringwoodite (endmember: MgRingwoodite |MgRi_Ri|)

where all endmembers are described by the thermodynamic models of
[Stixrude_Lithgow-Bertelloni_2005]_ and [Stixrude_Lithgow-Bertelloni_2011]_.

*Reaction Kinetics*
___________________

Given this  system,  there are two solid state  reactions

* **Olivine to Wadsleyite**:

    |Fo_ol| :math:`\rightarrow` |MgWd_Wd|

* **Wadsleyite to Ringwoodite**:

    |MgWd_Wd| :math:`\rightarrow` |MgRi_Ri|

All thermodynamic models for endmembers, phases, and kinetic reactions are
described in a series of jupyter notebooks in the directory
:code:`Systems/MgFeSiO4_Stixrude/notebooks`.  All paths and commands described below are
assumed to be relative to :code:`Systems/MgFeSiO4_Stixrude` (see :doc:`quickstart` for
specific instructions on extracting the ``Systems`` directory from the containers).

Modeling workflow
+++++++++++++++++

This demo will illustrate the full workflow of a ThermoCodegen project.

1. Describe the thermodynamic models for endmembers in jupyter notebooks using ``coder`` models and store them as ``*.emml`` files
2. Describe thermodynamic models for phases built on endmembers in jupyter notebooks and store them as ``*.phml`` files
3. Generate source code from model description files using the script ``tcg_builddb`` to construct a custom **thermodynamic database** of endmembers and phases
4. Describe reactions between endmembers in phases  in jupyter notebooks and store them as ``*.rxml`` files
5. Generate a compiled C++ library with python bindings for all code generated objects
6. Test the compiled libraries against benchmark solutions
7. Import the python bindings into jupyter notebooks to perform calculations (like calculating 1-D steady-state disequilibrium isentropes)

Quick build for the Stixrude |Fo| system
___________________________________________________

Steps 1--6 can be run using the shell scripts ``build_database.sh``,  to construct a  thermodynamic database
of endmembers and phases in the larger |MgFe| system, followed by
``build_reactions_pure_phases.sh`` to build reactions for just the pure Mg phases.
Within the directory ``Systems/MgFeSiO4_Stixrude``.

.. code-block:: bash

  bash ./build_database.sh
  bash ./build_reactions_pure_phases.sh



Details of ``build_reactions_pure_phases.sh``
++++++++++++++++++++++++++++++++++++++++++++++
This section describes in detail the various phases of the build process.


Jupyter notebooks describing endmembers and phases
___________________________________________________

**Endmembers**: There is a single  jupyter notebook that describes, in detail,  thermodynamic models
for endmembers. In the directory ``notebooks``


*  :doc:`Generate_stixrude_endmembers.ipynb  <notebooks/MgFeSiO4_Stixrude/Generate_stixrude_endmembers>` : calculates pure endmembers using the model of [Stixrude_Lithgow-Bertelloni_2011]_

**Phases**: There are two jupyter notebooks that describe, in detail, thermodynamic models
for both pure phases and Mg-Fe solid solution phases in this system.  In the directory ``notebook/phases``

* :doc:`Generate_pure_phases.ipynb <notebooks/MgFeSiO4_Stixrude/Generate_pure_phases>`: describe thermodynamic models for the 3 pure Mg endmember phases, Olivine, Wadsleyite, and Ringwoodite built on the solid endmembers above.
* :doc:`Generate_solution_phases.ipynb <notebooks/MgFeSiO4_Stixrude/Generate_solution_phases>`: describes the thermodynamic models Mg-Fe solid solution phases for  Olivine, Wadsleyite, and Ringwoodite using the :class:`coder.SimpleSolnModel`

These notebooks can be run manually or executed as scripts using

.. code-block:: bash

  # generate spudfiles and build  thermodynamic database
  jupyter nbconvert --to notebook --execute notebooks/Generate_stixrude_endmembers.ipynb
  jupyter nbconvert --to notebook --execute notebooks/Generate_pure_phases.ipynb
  jupyter nbconvert --to notebook --execute notebooks/Generate_solution_phases.ipynb

The endmember notebooks will generate a set of endmember ``.emml`` files and put them in the directory ``endmembers``.
The phase notebooks will generate a set of phase ``.phml`` files and put them in the directory ``phases``.

Viewing ThermoCodegen xml files with ``diamond``
******************************************************

Any of the spud xml files can be viewed directly using the ``diamond`` gui
available in the containers.  For example, running

.. code-block:: bash

  diamond phases/Olivine.phml

from within a singularity container (or a docker container with appropriate X11 configuration) in the ``Systems/MgFeSiO4_Stixrude`` subdirectory
should pop open a ``diamond`` window that includes all the parameters and sympy
expressions required to generate code for the Olivine model.

.. note::

  The file ``Olivine.phml`` generated from the Stixrude models, will generate a
  different thermodynamic model than e.g. ``Olivine.phml`` based on a Berman Forsterite
  endmember.  However, every ``coder`` generated source  file can be mapped by a hash
  back to the SPuD file it was generated from using the ``.identifier`` attribute.

.. .. image:: images/Diamond-Olivine-screenshot.png
  :width: 90%
  :align: center


.. note::

  It is possible to work with the xml files directly through the ``diamond``
  interface but it is usually easier to generate them from jupyter notebooks
  using the python :ref:`py_code_generation`  then writing ``.to_xml``.

Build the thermodynamic database using :doc:`tcg_builddb`
_________________________________________________________

The primary thermocodegen script :doc:`tcg_builddb`  reads the model description  files and
autogenerates source code from the xml files and generates a compressed **Thermodynamic database** file

Usage: from within the ``Systems/MgFeSiO4_Stixrude`` directory

.. code-block:: shell

  tcg_builddb --just_src  -zi database

which will generate the file ``database/MgFeSiO4_Stixrude.tar.gz``

Other available commands can be found with ``tcg_builddb -h`` or see :doc:`tcg_builddb`.
This script should be run in the directory above the directories containing the
relevant ``.emml`` and ``.phml`` files.

For example, given a directory or subdirectory containing a set of ``.emml`` and
``.phml`` files, we can inspect and validate those files (i.e. check that the
phases are completely described by the endmembers). Running

.. code-block:: bash

  tcg_builddb -evp

from within the ``MgFeSiO4_Stixrude`` directory should produce the output

.. code-block:: bash

  **** Available Endmembers ***
          Name              Formula                 File
       Wuestite_stixrude       Fe(1)O(1)       Wuestite_stixrude.emml
   FePerovskite_stixrude  Fe(1)Si(1)O(3)   FePerovskite_stixrude.emml
  FeRingwoodite_stixrude  Fe(2)Si(1)O(4)  FeRingwoodite_stixrude.emml
  MgRingwoodite_stixrude  Mg(2)Si(1)O(4)  MgRingwoodite_stixrude.emml
   MgPerovskite_stixrude  Mg(1)Si(1)O(3)   MgPerovskite_stixrude.emml
     Forsterite_stixrude  Mg(2)Si(1)O(4)     Forsterite_stixrude.emml
       Fayalite_stixrude  Fe(2)Si(1)O(4)       Fayalite_stixrude.emml
      Periclase_stixrude       Mg(1)O(1)      Periclase_stixrude.emml
   MgWadsleyite_stixrude  Mg(2)Si(1)O(4)   MgWadsleyite_stixrude.emml
   FeWadsleyite_stixrude  Fe(2)Si(1)O(4)   FeWadsleyite_stixrude.emml


  **** Available Phases ***
  Abbrev     Name              File
  FePv  FePerovskite      FePerovskite.phml
  Fa    Fayalite              Fayalite.phml
  Ol    Olivine                Olivine.phml
  Wa    Wadsleyite          Wadsleyite.phml
  FeWa  FeWadsleyite      FeWadsleyite.phml
  Pe    Periclase            Periclase.phml
  FeRi  FeRingwoodite    FeRingwoodite.phml
  Wu    Wuestite              Wuestite.phml
  Ri    Ringwoodite        Ringwoodite.phml
  MgRi  MgRingwoodite    MgRingwoodite.phml
  MgWa  MgWadsleyite      MgWadsleyite.phml
  Fo    Forsterite          Forsterite.phml
  MgPv  MgPerovskite      MgPerovskite.phml


  FePerovskite
   OK  FePerovskite_stixrude
  Fayalite
   OK  Fayalite_stixrude
  Olivine
   OK  Forsterite_stixrude
   OK  Fayalite_stixrude
  Wadsleyite
   OK  MgWadsleyite_stixrude
   OK  FeWadsleyite_stixrude
  FeWadsleyite
   OK  FeWadsleyite_stixrude
  Periclase
   OK  Periclase_stixrude
  FeRingwoodite
   OK  FeRingwoodite_stixrude
  Wuestite
   OK  Wuestite_stixrude
  Ringwoodite
   OK  MgRingwoodite_stixrude
   OK  FeRingwoodite_stixrude
  MgRingwoodite
   OK  MgRingwoodite_stixrude
  MgWadsleyite
   OK  MgWadsleyite_stixrude
  Forsterite
   OK  Forsterite_stixrude
  MgPerovskite
   OK  MgPerovskite_stixrude



Jupyter notebooks describing reactions
___________________________________________________

The jupyter notebook
:doc:`notebooks/Generate_reactions_Mg_pure_phases.ipynb <notebooks/MgFeSiO4_Stixrude/Generate_reactions_Mg_pure_phases>` creates an ``.rxml`` file describing the two solid state   reactions described above using phases and endmembers
described in the local thermodynamic database file ``database/MgFeSiO4_Stixrude.tar.gz``.

It can be run using

.. code-block:: shell

  jupyter nbconvert --to notebook --execute notebooks/Generate_reactions_Mg_pure_phases.ipynb


this script will generate a file ``reactions/Mg2SiO4_stixrude.rxml``.

For reactions in the Mg-Fe solid solution system use :doc:`notebooks/Generate_reactions_solution_phases.ipynb
<notebooks/MgFeSiO4_Stixrude/Generate_reactions_solution_phases>`.


Build the reaction object using :doc:`tcg_buildrx`
___________________________________________________

.. code-block:: bash

  cd reactions
  tcg_buildrx Mg2SiO4_stixrude.rxml -i

uses CMake to compile a C++ library and its python bindings  from the ``.rxml``
file that also includes all of the endmember and phase objects.

.. note::

	:doc:`tcg_buildrx` assumes that the first argument is an ``.rxml`` file.  For
	more options see :doc:`tcg_buildrx`.


Testing the models
__________________

At this point there should be a subdirectory in the ``reactions`` directory called
``Mg2SiO4_stixrude`` which contains

* ``Mg2SiO4_stixrude.module``
* ``Mg2SiO4_stixrude.senv``
* ``include``
* ``lib``
* ``src``

If you are using environment modules, you can set your environment using

.. code-block:: bash

  module load ./reactions/Mg2SiO4_stixrude/Mg2SiO4_stixrude.module

Alternatively if you are using singularity,  you can leave and restart the shell with

.. code-block:: bash

  singularity shell --env-file <path to>/Mg2SiO4_stixrude.senv <path to>/thermocodegen_tf-focal.sif

The file ``Mg2SiO4_stixrude.senv`` is a singularity environment file generated during
generation of the reaction object which adds the reaction  environment variables to the
container. See the documentation :doc:`docker` and :doc:`singularity` for more details.

Running ``pytest``
*******************

You can now test the python bindings with

.. code-block:: bash

  cd tests
  pytest --disable-warnings test*pure*

which if successful should return something similar to

.. code-block:: bash

  ===================================================================== test session starts ======================================================================
  platform darwin -- Python 3.9.5, pytest-6.2.4, py-1.10.0, pluggy-0.13.1
  rootdir: /Users/mspieg/Repos/gitlab/ThermoCodegen/examples/Systems/MgFeSiO4_Stixrude/tests
  plugins: anyio-3.3.0
  collected 1162 items

  test-v0.6.9_pure_rxn_2022-07-21_00:59:00/test_endmembers_21-Jul-2022_00:59:00.py ....................................................................... [  6%]
  ........................................................................................................................................................ [ 19%]
  ...............................................                                                                                                          [ 23%]
  test-v0.6.9_pure_rxn_2022-07-21_00:59:00/test_phases_21-Jul-2022_00:59:03.py ........................................................................... [ 29%]
  ........................................................................................................................................................ [ 42%]
  ........................................................................................................................................................ [ 55%]
  ........................................................................................................................................................ [ 68%]
  ........................................................................................................................................................ [ 82%]
  ........................................................................................................................................................ [ 95%]
  .......................                                                                                                                                  [ 97%]
  test-v0.6.9_pure_rxn_2022-07-21_00:59:00/test_rxns_21-Jul-2022_00:59:07.py ..................................                                            [100%]

  ===================================================================== 1162 passed in 3.80s =====================================================================

Generating test files
**********************

The test files included in this example were generated with the jupyter notebook
:doc:`Generate_tests.ipynb <notebooks/MgFeSiO4_Stixrude/Generate_tests>` in the ``notebooks`` directory.  This notebook uses the
:doc:`Tester <testing>` class imported from ``thermocodegen.testing`` which
takes in a ThermoCodegen reaction or database object and tests all of its
methods against known input values.  Output of ``Tester``  includes
``pandas.DataFrames`` of output values as well as ``pytest`` test files.


Using ThermoCodegen objects
+++++++++++++++++++++++++++

Once the ThermoCodegen objects are compiled, their python bindings can be
imported directly into any python project or juypter notebook and be used to
explore the system and generate models or figures.  As an example, the notebook
:doc:`Isentropic_columns.ipynb
<notebooks/MgFeSiO4_Stixrude/Isentropic_columns>` provides code for generating
1-D steady-state isentropic profiles for a disequilibrium upwelling system.

Jupyter Notebooks
+++++++++++++++++

Endmembers
__________

.. toctree::
  :maxdepth: 1

  Generate Stixrude endmembers <notebooks/MgFeSiO4_Stixrude/Generate_stixrude_endmembers>

Phases
______

.. toctree::
  :maxdepth: 1

  Generate pure phases <notebooks/MgFeSiO4_Stixrude/Generate_pure_phases>
  Generate solution phases <notebooks/MgFeSiO4_Stixrude/Generate_solution_phases>

Reactions
_________

.. toctree::
  :maxdepth: 1

  Generate reactions for Mg pure phases <notebooks/MgFeSiO4_Stixrude/Generate_reactions_Mg_pure_phases>
  Generate reactions for Mg-Fe solution phases <notebooks/MgFeSiO4_Stixrude/Generate_reactions_solution_phases>

Tests and Applications
______________________

.. toctree::
  :maxdepth: 1

  Generate tests <notebooks/MgFeSiO4_Stixrude/Generate_tests>
  Mg2SiO4 phase diagram <notebooks/MgFeSiO4_Stixrude/Mg2SiO4_phase_diagram>
  1-D isentropic melting columns <notebooks/MgFeSiO4_Stixrude/Isentropic_columns>
