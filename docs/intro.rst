Introduction
===================================


ThermoCodegen is part of the larger `ENKI <http://enki-portal.org/>`_ project
for Thermodynamic modeling.

`This software <https://gitlab.com/ENKI-portal/ThermoCodegen>`_ provides code-generation tools for developing and
exploring reproducible, custom thermodynamic models and reaction
kinetics libraries for use in a wide range of applications.

At its heart, `ThermoCodegen <https://gitlab.com/ENKI-portal/ThermoCodegen>`_, shares the same code-generation module ``coder.py``
from `ThermoEngine <https://gitlab.com/ENKI-portal/ThermoEngine>`_
for generating high-performance C code from sympy expressions for the
free energy of thermodynamic  endmembers and phases.
ThermoCodegen supports most models supported by :class:`ThermoEngine.coder`.

ThermoCodegen adds the following features:

* Model generation is described in jupyter notebooks, but stored in structured, schema-validated
  xml files that contain all the information required to describe and
  auto-generate code for thermodynamic databases of endmembers and phases.
* ThermoCodegen also provides code generation of custom kinetic reaction models
  built on the phase databases.
* C++ libraries are generated for inclusion in other
  modeling systems such as  `TerraFERMA <https://terraferma.github.io>`_ or
  `ASPECT <https://aspect.geodynamics.org>`_.
* Python bindings of the C++ classes can also be directly imported into python applications.
* Python bindings  can also be included in  ThermoEngine as a custom
  database for use in equilibration or calibration calculations (requires installing the ``tcg_model_integration``
  branch of `ThermoEngine <ThermoEngine_>`_).

The ThermoCodegen code repository is available at `https://gitlab.com/ENKI-portal/ThermoCodegen
<https://gitlab.com/ENKI-portal/ThermoCodegen>`_.


.. toctree::
