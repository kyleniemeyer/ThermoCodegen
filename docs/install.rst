Install
=======

.. note::
    The following instructions are intended as a guide only and are not supported or guaranteed to work.  
    Rather than following these instructions we encourage all users to use the supplied docker or
    singularity containers described in :doc:`docker` and :doc:`singularity` respectively.

Dependencies
++++++++++++

`ThermoCodegen <https://gitlab.com/ENKI-portal/ThermoCodegen>`_ (like most packages)  has a rather large number of dependencies.  The most
specific description of the required dependencies can be found in the Dockerfiles for building on
Ubuntu systems (see below).  Here we will just list the primary dependencies for reference.

**Python Dependencies**

* python >= 3.7
* sympy == 1.3
* numpy
* scipy
* pandas
* pytest
* jupyterlab
* `molmass <https://pypi.org/project/molmass/>`_, a very useful python package by Christoph Gohlke for interacting with chemical formulas

**C/C++ support**

* CMake >= 3.0.0
* `pybind11 <https://pybind11.readthedocs.io/en/stable>`_ >= 2.2.3
* libgsl
* `SPuD <https://github.com/FluidityProject/spud>`_ from the Fluidity project.

`SPuD <https://github.com/FluidityProject/spud>`_ is a science-neutral options
handling system used for viewing and interacting with thermodynamic models
stored as xml files. The basic installation will provide the python package
``libspud`` which is required for parsing the xml files.  Full functionality of
the ``diamond`` GUI will also require `GTK <https://www.gtk.org/>`_ support.

It is also highly useful to have `environment modules <http://modules.sourceforge.net/>`_
installed as we will create module files to set various environment variables and paths.

Installation Instructions
+++++++++++++++++++++++++

Installation Notes for  Ubuntu
______________________________

A specific recipe for building ThermoCodegen on Ubuntu 20.04 (focal) can be found in the
file `docker/focal/Dockerfile <https://gitlab.com/ENKI-portal/ThermoCodegen/-/blob/main/docker/focal/Dockerfile>`_ which describes how to build ThermoCodegen.  
This container also includes all the packages required to build this documentation.  
The following is a summary of that file and will likely require modification if installed
manually.

Ubuntu packages
---------------

.. code-block:: bash

    sudo apt -y install \
                       git \
                       gcc \
                       g++ \
                       gfortran \
                       cmake \
                       flex \
                       wget \
                       gir1.2-gtksource-3.0 \
                       gir1.2-gtk-3.0 \
                       pkg-config \
                       tcl \
                       vim \
                       libgsl-dev \
                       pybind11-dev \
                       doxygen \
                       imagemagick \
                       plantuml \
                       pandoc \
                       environment-modules \
                       nodejs \
                       npm \
                       cython3 \
                       python3 \
                       python3-setuptools \
                       python3-matplotlib \
                       python3-ply \
                       python3-future \
                       python3-gi \
                       python3-lxml \
                       python3-cairocffi \
                       python3-cairo \
                       python3-gi-cairo \
                       python3-pip \
                       python3-requests \
                       python3-sphinx \
                       python3-sphinx-argparse \
                       python3-nbsphinx \
                       python3-mock \
                       python3-sphinx-rtd-theme \
                       python3-breathe

NPM packages (only required for documentation)
**********************************************

.. code-block:: bash

    sudo npm install --global \
                       jsdoc \
                       typedoc

Pypi packages
-------------

.. code-block:: bash

    sudo pip3 install \
                       -Iv sympy==1.3 \
                       -Iv numpy==1.22.4 \
                       -Iv scipy==1.6.3 \
                       pandas==1.4.3 \
                       pytest==4.6.9 \
                       jupyter-client==6.1.2 \
                       jupyter-console==6.0.0 \
                       jupyter-core==4.6.3 \
                       jupyterlab==2.3.2 \
                       nbconvert==5.6.1 \
                       MarkupSafe==1.1.0 \
                       Jinja2==2.10.1 \
                       molmass==2021.6.18

Install SPuD
-------------

.. code-block:: bash

    git clone -q https://github.com/FluidityProject/spud.git
    cd spud
    ./configure
    make
    sudo make install
    sudo pip3 install ./python
    sudo pip3 install ./diamond
    cd ..


Install ThermoCodegen
----------------------

Once all  dependencies  are satisfied,  ThermoCodegen can be installed locally using cmake.  For example, to install
from the ThermoCodegen repository into ``/usr/local/thermocodegen`` use

.. code-block:: bash

    THERMOCODEGEN_HOME=/usr/local/thermocodegen  # or alternate installation location
    git clone -q https://gitlab.com/ENKI-portal/ThermoCodegen.git
    cd ThermoCodegen
    mkdir build
    cd build
    cmake .. -DCMAKE_INSTALL_PREFIX=${THERMOCODEGEN_HOME} -DCMAKE_BUILD_TYPE=Release
    sudo make install
    cd ../..

Set appropriate Environment variables
*************************************

.. code-block:: bash

    source /usr/share/modules/init/bash
    export THERMOCODEGEN_HOME=${THERMOCODEGEN_HOME}
    export THERMOCODEGEN_CMAKE_PATH=${THERMOCODEGEN_HOME}/share/thermocodegen/cmake
    export PATH=${THERMOCODEGEN_HOME}/bin:${PATH}
    export PYTHONPATH=${THERMOCODEGEN_HOME}/lib/python3.8/site-packages:${PYTHONPATH}
    export LD_LIBRARY_PATH=/usr/local/lib:${THERMOCODEGEN_HOME}/lib:${LD_LIBRARY_PATH}
    export DIAMOND_CONFIG_PATH=${THERMOCODEGEN_HOME}/share/thermocodegen/diamond:${DIAMOND_CONFIG_PATH}

alternatively, you can use environment-modules to set the environment.  On installation,
a modules file ``thermocodegen.configmodule`` will be created that can be loaded using

.. code-block:: bash

    module load ${THERMOCODEGEN_HOME}/share/thermocodegen/thermocodegen.configmodule

Test
****

.. code-block:: bash

    mkdir examples
    cp -r ${THERMOCODEGEN_HOME}/share/thermocodegen/examples/Systems examples
    cd examples/Systems/fo_fa
    bash build_system.sh
    cd ../../..

Installation notes for MacOS
_____________________________

ThermoCodegen was developed on MacOS using `Xcode <https://developer.apple.com/xcode>`_ command line tools and 
`Homebrew <https://brew.sh>`_ as the primary package manager.  The following was tested on MacOS Monterey (12.4) 
using an Apple M1 chip with Xcode 13.4 and Homebrew 3.5.5.
The list of dependencies below will likely require modification for different systems and we recommend using
:doc:`docker<docker>` on MacOS instead.


Brew packages
-------------

.. code-block:: bash

    brew install python@3.9 \
                 cmake \
                 pybind11 \
                 gtk+3 \
                 pygobject3 \
                 gtksourceview3 \
                 adwaita-icon-theme \
                 gsl \
                 doxygen \
                 pandoc \
                 npm \
                 jing-trang \
                 gfortran \
                 modules

Pypi packages
-------------

.. code-block:: bash

    pip3 install \
                 -Iv sympy==1.3 \
                 numpy==1.21 \
                 scipy==1.8.1 \
                 jupyter-client==6.1.2 \
                 jupyter-console==6.0.0 \
                 jupyter-core==4.6.3 \
                 jupyterlab==2.3.2 \
                 ipython-genutils==0.2.0 \
                 future==0.18.2 \
                 pandas==1.4.3 \
                 docutils==0.16 \
                 requests==2.22.0 \
                 lxml==4.5.0 \
                 pytest==4.6.9 \
                 cython==0.29.14 \
                 molmass==2021.6.18 \
                 matplotlib==3.1.2 \
                 Jinja2==2.10.1 \
                 MarkupSafe==1.1.0 \
                 nbconvert==5.6.1 \
                 sphinx==1.8.5 \
                 sphinx-argparse==0.2.2 \
                 nbsphinx==0.4.3 \
                 mock==3.0.5 \
                 breathe==4.12.0 \
                 sphinx-rtd-theme==0.4.3

Install SPuD
-------------

.. code-block:: bash

    SPUD_DIR=$HOME/Work/spud  # or alternate installation location
    git clone https://github.com/FluidityProject/spud.git
    cd spud
    ./configure --prefix=$SPUD_DIR
    make
    make install
    pip3 install --prefix=$SPUD_DIR ./python
    pip3 install --prefix=$SPUD_DIR ./diamond
    cd ..

and set appropriate environment variables due to the non-default installation location used above

.. code-block:: bash

    export SPUD_DIR=$SPUD_DIR
    export PATH=${SPUD_DIR}/bin:${PATH}
    export PYTHONPATH=${SPUD_DIR}/lib/python3.9/site-packages:${PYTHONPATH}

Install ThermoCodegen
----------------------

Once all  dependencies  are satisfied,  ThermoCodegen can be installed locally using cmake.  For example, to install
from the ThermoCodegen repository into ``~/Work/ThermoCodegen`` use

.. code-block:: bash

    THERMOCODEGEN_HOME=${HOME}/Work/ThermoCodegen  # or alternate installation location
    git clone -q https://gitlab.com/ENKI-portal/ThermoCodegen.git
    cd ThermoCodegen
    mkdir build
    cd build
    cmake .. -DCMAKE_INSTALL_PREFIX=${THERMOCODEGEN_HOME} -DCMAKE_BUILD_TYPE=Release
    make install
    cd ../..

Set appropriate Environment variables
*************************************

.. code-block:: bash

    source /opt/homebrew/opt/modules/init/bash
    export THERMOCODEGEN_HOME=${THERMOCODEGEN_HOME}
    export THERMOCODEGEN_CMAKE_PATH=${THERMOCODEGEN_HOME}/share/thermocodegen/cmake
    export PATH=${THERMOCODEGEN_HOME}/bin:${PATH}
    export PYTHONPATH=${THERMOCODEGEN_HOME}/lib/python3.9/site-packages:${PYTHONPATH}
    export DIAMOND_CONFIG_PATH=${THERMOCODEGEN_HOME}/share/thermocodegen/diamond:${DIAMOND_CONFIG_PATH}

or use environment modules to set your environment with

.. code-block:: bash

    module load ${THERMOCODEGEN_HOME}/share/thermocodegen/thermocodegen.configmodule

Test
****

.. code-block:: bash

    mkdir examples
    cp -r ${THERMOCODEGEN_HOME}/share/thermocodegen/examples/Systems examples
    cd examples/Systems/fo_fa
    bash build_system.sh
    cd ../../..


Building documentation
______________________

The best way to access the documentation is online and the best way to build the documentation is in the docker container or through
gitlab CI.  If you need to build the documentation locally, all the examples must be built and loaded first.

.. code-block:: bash

    cd examples/Systems
    cd fo_fa
    bash build_system.sh
    cd ..
    cd fo_h2o
    bash build_system.sh
    cd ..
    cd fo_sio2
    bash build_system.sh
    cd ..
    cd MgFeSiO4_Stixrude
    bash build_database.sh
    bash build_reactions_pure_phases.sh
    bash build_reactions_solution_phases.sh
    cd ../../..
    module load ./examples/Systems/fo_fa/reactions/fo_fa_binary/*.module
    module load ./examples/Systems/fo_sio2/reactions/fo_sio2_poly_linear_rxns/*.module
    module load ./examples/Systems/MgFeSiO4_Stixrude/reactions/Mg2SiO4_stixrude/*.module
    module load ./examples/Systems/fo_h2o/reactions/fo_h2o_hydration/*.module

Following this, the documentation can be made in the ThermoCodegen ``docs`` directory

.. code-block:: bash

    cd ThermoCodegen/docs
    make html
    mv  _build/html ../public
    cd ../..

The documentation will then be available locally in ``ThermoCodegen/public/index.html``.

Building docker
_______________

The directory ``docker`` contains subdirectories describing different docker images.  ``docker/focal`` contains a
complete installation of ThermoCodegen on an Ubuntu 20.04 (focal) system.  ``docker/tf-focal`` contains an installation
of ThermoCodegen on top of an installation of `TerraFERMA <https://terraferma.github.io>`_ and is used in most of the examples in this documentation.

The docker images are available in the gitlab repository (see :doc:`docker`) but can be built locally, for example

.. code-block:: bash

    cd docker/focal
    docker build --tag thermocodegen:focal .

where ``thermocodegen:focal`` is the resulting docker image name locally.

More complicated multiplatform and multistage builds are available and described in the script ``docker/build.sh``, which can be used
to rebuild the docker images using, e.g. for ``focal``

.. code-block:: bash

    cd docker
    bash build.sh -a focal
    cd ..

