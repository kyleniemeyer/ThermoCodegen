.. ThermoCodegen documentation master file, created by
   sphinx-quickstart on Fri Aug 21 21:43:58 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ThermoCodegen's documentation!
=========================================


.. toctree::
   :maxdepth: 1
   :caption: Getting Started

   intro
   quickstart
   install
   contributing

.. toctree::
   :maxdepth: 1
   :caption: Using the software

   design
   examples
   build_scripts

.. toctree::
   :maxdepth: 1
   :caption: API Reference

   py_bindings
   cpp

Indices and tables
==================

* :ref:`genindex`
