The Forsterite-Fayalite Ideal Binary
=================================================

Perhaps one of the simplest geologically relevant thermodynamic systems is the
Forsterite-Fayalite ideal binary melting loop

.. image:: ./images/fo_fa_binary.png
  :width: 400
  :align: center

Which, in equilibrium, calculates the composition of coexisting olivine and liquid
at a fixed :math:`T` and :math:`P`

System Description
++++++++++++++++++++

.. some useful compositional macros

.. |Fo| replace:: Mg\ :sub:`2`\ SiO\ :sub:`4`
.. |Fa| replace:: Fe\ :sub:`2`\ SiO\ :sub:`4`
.. |Fo_ol| replace:: |Fo|\ _(ol)
.. |Fa_ol| replace:: |Fa|\ _(ol)
.. |Fo_lq| replace:: |Fo|\ _(lq)
.. |Fa_lq| replace:: |Fa|\ _(lq)



*Thermodynamic Database*
_________________________

This system is described by a custom **Thermodynamic Database** consisting of
two phases, constructed from four endmembers.

The first phase is crystalline **Olivine** constructed as an ideal solid solution between
two endmembers

* Forsterite : |Fo_ol|
* Fayalite   : |Fa_ol|

Both described using [Berman_1988]_ mineral models.

The second phase is  **Liquid** which is also an ideal solid solution composed of

* Forsterite : |Fo_lq|
* Fayalite   : |Fa_lq|

endmembers using the [Ghiorso_and_Sack_1995]_ xMelts models.

*Reaction Kinetics*
____________________

Given this simple system,  there are two reactions involved in melting and crystallization of olivine

* |Fo_ol| :math:`\rightarrow` |Fo_lq|
* |Fa_ol| :math:`\rightarrow` |Fa_lq|


All thermodynamic models for endmembers, phases, and kinetic reactions are
described in a series of jupyter notebooks in the directory
:code:`Systems/fo_fa/notebooks`  directory.  All paths and commands described below are
assumed to be relative to :code:`Systems/fo_fa` (see :doc:`quickstart` for
specific instructions on extracting the ``Systems`` directory).

Modeling workflow
+++++++++++++++++

This demo will illustrate the full workflow of a ThermoCodegen project.

1. Describe the thermodynamic models for endmembers in jupyter notebooks using ``coder`` models and store them as ``*.emml`` files
2. Describe thermodynamic models for phases built on endmembers in jupyter notebooks and store them as ``*.phml`` files
3. Generate source code from model description files using the script ``tcg_builddb`` to construct a custom **thermodynamic database** of endmembers and phases
4. Describe reactions between endmembers in phases  in jupyter notebooks and store them as ``*.rxml`` files
5. Generate a compiled C++ library with python bindings for all code generated objects
6. Test the compiled libraries against benchmark solutions
7. Import the python bindings into jupyter notebooks to perform calculations (like calculating the equilibrium melting loop above)

Quick build for the Fo-Fa system
___________________________________________________

Steps 1--6 can be run using the shell script ``build_system.sh`` in the
directory ``Systems/fo_fa``.

.. code-block:: bash

  bash ./build_system.sh

Details of ``build_system.sh``
++++++++++++++++++++++++++++++++++
This section describes in detail the various phases of the build process


Jupyter notebooks describing endmembers and phases
___________________________________________________

There are three jupyter notebooks that describe, in detail,  thermodynamic models
for endmembers and phases.

*  :doc:`Generate_berman_endmembers.ipynb <notebooks/fo_fa/Generate_berman_endmembers>`: calculates pure forsterite and fayalite endmembers of Olivine using the model of [Berman_1988]_.
*  :doc:`Generate_xmelts_endmembers.ipynb <notebooks/fo_fa/Generate_xmelts_endmembers>`: calculate Fo and Fa liquid endmembers using the xMelts model of [Ghiorso_and_Sack_1995]_.
*  :doc:`Generate_phases.ipynb <notebooks/fo_fa/Generate_phases>`: describe thermodynamic models for the ideal solid solutions for ``Olivine`` and ``Liquid`` built on the Berman and xMelts endmembers.

These notebooks can be run manually or executed as scripts using

.. code-block:: bash

  jupyter nbconvert --to notebook --execute notebooks/Generate_berman_endmembers.ipynb
  jupyter nbconvert --to notebook --execute notebooks/Generate_xmelts_endmembers.ipynb
  jupyter nbconvert --to notebook --execute notebooks/Generate_phases.ipynb

The first two notebooks will generate a set of endmember ``.emml`` files and put them in the directory ``endmembers``.
The third notebook will generate a set of phase ``.phml`` files and put them in the directory ``phases``.

Viewing ThermoCodegen xml files with ``diamond``
******************************************************

any of the spud xml files can be viewed directly using the ``diamond`` gui
available in the containers.  For example, running

.. code-block:: bash

  diamond phases/Olivine.phml

from within a singularity container (or a docker container with appropriate X11 configuration) in the ``Systems/fo_fa`` subdirectory should
pop open a ``diamond`` window

.. image:: images/Diamond-Olivine-screenshot.png
  :width: 90%
  :align: center

that includes all the parameters and sympy expressions required to generate code for the Olivine model.

.. note::

	It is possible to work with the xml files directly through the ``diamond``
	interface but it is usually easier to generate them from jupyter Notebooks
	using the python :ref:`py_code_generation`  then writing ``.to_xml``.

Build the thermodynamic database using :doc:`tcg_builddb`
_________________________________________________________

The primary thermocodegen script :doc:`tcg_builddb`  reads the model description  files and
autogenerates source code from the xml files and generates a compressed **Thermodynamic Database** file.

Usage: from within the ``Systems/fo_fa`` directory

.. code-block:: shell

  tcg_builddb --just_src  -zi database

which will generate the file ``database/fo_fa.tar.gz``.

Other available commands can be found with ``tcg_builddb -h`` or see :doc:`tcg_builddb`.
This script should be run in the directory above the directories containing the
relevant ``.emml`` and ``.phml`` files.

For example, given a directory or subdirectory containing a set of ``.emml`` and
``.phml`` files, we can inspect and validate those files (i.e. check that the
phases are completely described by the endmembers). Running

.. code-block:: bash

  tcg_builddb -evp

from within the ``Systems/fo_fa`` directory should produce the output

.. code-block:: bash

  **** Available Endmembers ***
         Name            Formula               File
      Fayalite_berman  FE(2)SI(1)O(4)    Fayalite_berman.emml
    Forsterite_xmelts  MG(2)SI(1)O(4)  Forsterite_xmelts.emml
      Fayalite_xmelts  FE(2)SI(1)O(4)    Fayalite_xmelts.emml
    Forsterite_berman  MG(2)SI(1)O(4)  Forsterite_berman.emml


  **** Available Phases ***
  Abbrev  Name        File
  Ol   Olivine    Olivine.phml
  Liq  Liquid      Liquid.phml


  Olivine
     OK  Forsterite_berman
     OK  Fayalite_berman
  Liquid
     OK  Forsterite_xmelts
     OK  Fayalite_xmelts




Jupyter notebooks describing reactions
___________________________________________________

The jupyter notebook :doc:`Generate_reactions.ipynb <notebooks/fo_fa/Generate_reactions>` creates a ``.rxml`` file describing reactions between Olivine and Liquid endmember components
based on the generated code in the thermodynamic database file.

Running

.. code-block:: shell

  jupyter nbconvert --to notebook --execute notebooks/Generate_reactions.ipynb


will generate a file ``fo_fa_binary.rxml`` in the subdirectory `reactions`
that uses the local database file

.. code-block:: bash

  database/fo_fa.tar.gz

that was just created using :doc:`tcg_builddb`.  However, an ``.rxml``
can also be built from a pre-compiled database accessible through a URL.  As an example,
we have included an equivalent ``.rxml`` file :file:`reactions/fo_fa_binary_zenodo.rxml` which is built on
an equivalent database file that was previously created and published at zenodo with
the DOI `10.5281/zenodo.7976277 <https://doi.org/10.5281/zenodo.7976277>`_ and provides the database at the URL https://zenodo.org/record/7976277/files/fo_fa.tar.gz.

Build the reaction object using :doc:`tcg_buildrx`
___________________________________________________

.. code-block:: bash


  cd reactions
  tcg_buildrx fo_fa_binary.rxml -i

uses CMake to compile a C++ library and its python bindings  from the ``.rxml``
file that also includes all of the endmember and phase objects.  Alternatively,
we could build the same libraries using

.. code-block:: bash

  cd reactions
  tcg_buildrx fo_fa_binary_zenodo.rxml -i

which would use the endmember and phase source code from the remote database.  To check the database and validate the reactions run

.. code-block:: bash

  cd reactions
  tcg_buildrx fo_fa_binary_zenodo.rxml -evp

.. note::

	:doc:`tcg_buildrx` assumes that the first argument is an ``.rxml`` file.  For
	more options see :doc:`tcg_buildrx`.


Testing the models
__________________

At this point there should be a subdirectory of the ``reactions`` directory called
``fo_fa_binary``, which contains

* ``fo_fa_binary.module``
* ``fo_fa_binary.senv``
* ``include``
* ``lib``
* ``src``

If you are using environment modules, you can set your environment using

.. code-block:: bash

  module load ./reactions/fo_fa_binary/fo_fa_binary.module



Alternatively if you are using :doc:`singularity<singularity>`,  you can leave and restart the shell with

.. code-block:: bash

  singularity shell --env-file <path to>/fo_fa_binary.senv <path to>/thermocodegen_tf-focal.sif

The file ``fo_fa_binary.senv`` is a singularity environment file generated during
construction of the reaction object which adds the reaction  environment variables to the
container.

Running ``pytest``
******************

You can now test the python bindings with

.. code-block:: bash

  cd tests
  pytest --disable-warnings test*

which if successful should return something similar to

.. code-block:: bash

    ===================================================================== test session starts ======================================================================
    platform linux -- Python 3.8.10, pytest-4.6.9, py-1.11.0, pluggy-0.13.1
    rootdir: /home/tfuser/shared/Systems/fo_fa
    collected 274 items

    tests/test-v0.6.9-2022-07-21_00:15:09/test_endmembers_21-Jul-2022_00:15:09.py .......................................................................... [ 27%]
    ..................................                                                                                                                       [ 39%]
    tests/test-v0.6.9-2022-07-21_00:15:09/test_phases_21-Jul-2022_00:15:10.py .............................................................................. [ 67%]
    ......................................................                                                                                                   [ 87%]
    tests/test-v0.6.9-2022-07-21_00:15:09/test_rxns_21-Jul-2022_00:15:11.py ..................................                                               [100%]

    ================================================================== 274 passed in 1.63 seconds ==================================================================


Generating test files
**********************

The test files included in this example were generated with the jupyter notebook
:doc:`Generate_tests.ipynb <notebooks/fo_fa/Generate_tests>` in the ``notebooks`` directory.  This notebook uses the
:doc:`Tester <testing>` class imported from ``thermocodegen.testing`` which
takes in a ThermoCodegen reaction or database object and tests all of its
methods against known input values.  Output of ``Tester``  includes
``pandas.DataFrames`` of output values as well as ``pytest`` test files.


Using ThermoCodegen objects
+++++++++++++++++++++++++++

Once the ThermoCodegen objects are compiled, their python bindings can be
imported directly into any python project or Juypter Notebook and be used to
explore the system and generate models or figures.  As an example, the notebook
:doc:`Examples_Fo_Fa_system.ipynb <notebooks/fo_fa/Examples_Fo_Fa_system>` demonstrates basic thermodynamic
properties of the phases  as well as code to calculate the binary melting loop
figure shown above.  This notebook also uses the reactions object in a batch
reactor ODE based model using python ODE solvers.

Jupyter Notebooks
+++++++++++++++++

Endmembers and Phases
_____________________

.. toctree::
  :maxdepth: 1

  Generate Berman endmembers <notebooks/fo_fa/Generate_berman_endmembers>
  Generate xMelts endmembers <notebooks/fo_fa/Generate_xmelts_endmembers>
  Generate phases <notebooks/fo_fa/Generate_phases>


Reactions
_________

.. toctree::
  :maxdepth: 1

  Generate reactions <notebooks/fo_fa/Generate_reactions>

Tests and Applications
______________________

.. toctree::
  :maxdepth: 1

  Generate tests <notebooks/fo_fa/Generate_tests>
  Example usage <notebooks/fo_fa/Examples_Fo_Fa_system>
