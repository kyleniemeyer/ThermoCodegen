<?xml version='1.0' encoding='utf-8'?>
<phase_options>
  <name name="Orthopyroxene">
    <abbrev name="Opx"/>
  </name>
  <formula>
    <formula_string>
      <string_value lines="1">Ca[Ca]Mg[Mg]Fe[Fe]Si[Si]O6</string_value>
    </formula_string>
    <conversion_string>
      <string_value lines="1">['[0]=[Ca]-[Fe]', '[1]=[Fe]', '[2]=([Mg]-[Ca])/2.0']</string_value>
    </conversion_string>
    <test_string>
      <string_value lines="1">['[0]+[1] &gt; 0.0', '[1] &gt; 0.0', '[0]+2.0*[2] &gt; 0.0']</string_value>
    </test_string>
  </formula>
  <reference>
    <string_value lines="1">Thermocodegen-v0.6/share/thermocodegen/examples/Notebooks/coder_to_xml/phases/Example-04-Ternary_OPX_Complex_Soln.ipynb</string_value>
  </reference>
  <endmembers>
    <endmember_name name="Diopside_berman"/>
    <endmember_name name="Hedenbergite_berman"/>
    <endmember_name name="Enstatite_berman"/>
  </endmembers>
  <free_energy_model name="Gibbs">
    <variable name="T">
      <rank name="Scalar"/>
      <units>K</units>
    </variable>
    <variable name="P">
      <rank name="Scalar"/>
      <units>bar</units>
    </variable>
    <variable name="n">
      <rank name="Vector">
        <size>K</size>
      </rank>
      <units>mol</units>
      <symbol>
        <string_value lines="1">Matrix([[n1], [n2], [n3]])</string_value>
      </symbol>
    </variable>
    <function name="mu">
      <type name="external">
        <rank name="Vector">
          <size>K</size>
        </rank>
        <variable name="T">
          <rank name="Scalar"/>
          <units>K</units>
        </variable>
        <variable name="P">
          <rank name="Scalar"/>
          <units>bar</units>
        </variable>
      </type>
      <symbol>
        <string_value lines="1">Matrix([[mu1(T, P)], [mu2(T, P)], [mu3(T, P)]])</string_value>
      </symbol>
    </function>
  </free_energy_model>
  <parameters>
    <parameter name="T_r">
      <rank name="Scalar">
        <value>
          <real_value rank="0">298.15</real_value>
        </value>
        <units>
          <string_value lines="1">'K'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="P_r">
      <rank name="Scalar">
        <value>
          <real_value rank="0">1</real_value>
        </value>
        <units>
          <string_value lines="1">'bar'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="Fh">
      <rank name="Scalar">
        <value>
          <real_value rank="0">-13807</real_value>
        </value>
        <units>
          <string_value lines="1">'J/mol'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="Fs">
      <rank name="Scalar">
        <value>
          <real_value rank="0">-2.319</real_value>
        </value>
        <units>
          <string_value lines="1">'J/K-mol'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="Fv">
      <rank name="Scalar">
        <value>
          <real_value rank="0">-0.05878</real_value>
        </value>
        <units>
          <string_value lines="1">'J/bar-mol'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="Hex">
      <rank name="Scalar">
        <value>
          <real_value rank="0">-7824</real_value>
        </value>
        <units>
          <string_value lines="1">'J/mol'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="Vex">
      <rank name="Scalar">
        <value>
          <real_value rank="0">-0.1213</real_value>
        </value>
        <units>
          <string_value lines="1">'J/bar-mol'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="Hx">
      <rank name="Scalar">
        <value>
          <real_value rank="0">-1883</real_value>
        </value>
        <units>
          <string_value lines="1">'J/mol'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="Vx">
      <rank name="Scalar">
        <value>
          <real_value rank="0">0.02824</real_value>
        </value>
        <units>
          <string_value lines="1">'J/bar-mol'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="WhM2CaMg">
      <rank name="Scalar">
        <value>
          <real_value rank="0">31631</real_value>
        </value>
        <units>
          <string_value lines="1">'J/mol'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="WvM2CaMg">
      <rank name="Scalar">
        <value>
          <real_value rank="0">0.03347</real_value>
        </value>
        <units>
          <string_value lines="1">'J/bar-mol'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="WhM2CaFe">
      <rank name="Scalar">
        <value>
          <real_value rank="0">17238</real_value>
        </value>
        <units>
          <string_value lines="1">'J/mol'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="WvM2CaFe">
      <rank name="Scalar">
        <value>
          <real_value rank="0">0.04602</real_value>
        </value>
        <units>
          <string_value lines="1">'J/bar-mol'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="WhM1FeMg">
      <rank name="Scalar">
        <value>
          <real_value rank="0">8368</real_value>
        </value>
        <units>
          <string_value lines="1">'J/mol'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="WvM1FeMg">
      <rank name="Scalar">
        <value>
          <real_value rank="0">0.01412</real_value>
        </value>
        <units>
          <string_value lines="1">'J/bar-mol'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="WhM2FeMg">
      <rank name="Scalar">
        <value>
          <real_value rank="0">8368</real_value>
        </value>
        <units>
          <string_value lines="1">'J/mol'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="WvM2FeMg">
      <rank name="Scalar">
        <value>
          <real_value rank="0">0.01412</real_value>
        </value>
        <units>
          <string_value lines="1">'J/bar-mol'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="R">
      <rank name="Scalar">
        <value>
          <real_value rank="0">8.31446261815324</real_value>
        </value>
        <units>
          <string_value lines="1">'J/mol/K'</string_value>
        </units>
      </rank>
    </parameter>
  </parameters>
  <functions>
    <ordering_function name="oF">
      <variable name="s">
        <rank name="Vector">
          <size>
            <integer_value rank="0">1</integer_value>
          </size>
        </rank>
        <units>None</units>
        <symbol>
          <string_value lines="1">Matrix([[s1]])</string_value>
        </symbol>
      </variable>
      <residual name="oF">
        <expression>
          <string_value type="code" language="python" lines="20">oF = (
[-R*T*(n1 + n2 + n3)*(log((n3 - s1*(n1 + n2 + n3))/(n1 + n2 + n3)) -
log((n3 + s1*(n1 + n2 + n3))/(n1 + n2 + n3)) + log(-(-2*n2 + n3 +
s1*(n1 + n2 + n3))/(n1 + n2 + n3)) - log((-2*n2 + n3 + (s1 + 2)*(n1 +
n2 + n3))/(n1 + n2 + n3)))/2 - n2*(Hx + P*Vx - 2*P*WvM1FeMg -
2*WhM1FeMg)/2 + n3*(-Fh + Fs*T - Fv*P + Hex + Hx + P*Vex + P*Vx -
2*P*WvM1FeMg - 2*P*WvM2CaFe + 2*P*WvM2CaMg - 2*WhM1FeMg - 2*WhM2CaFe +
2*WhM2CaMg)/4 + (n1 + n2 + n3)*(Fh - Fs*T + Fv*P + Hex + Hx + P*Vex +
P*Vx - 2*P*WvM1FeMg + 2*P*WvM2CaFe - 2*P*WvM2CaMg - 2*WhM1FeMg +
2*WhM2CaFe - 2*WhM2CaMg - 2*s1*(-Hx - P*Vx + P*WvM1FeMg + P*WvM2FeMg +
WhM1FeMg + WhM2FeMg))/4]
)</string_value>
        </expression>
      </residual>
      <guess name="guess">
        <expression>
          <string_value type="code" language="python" lines="20">guess = [0]</string_value>
        </expression>
      </guess>
      <bounds name="bounds">
        <expression>
          <string_value type="code" language="python" lines="20">bounds = (-oo &lt; s1) &amp; (s1 &lt; oo) &amp; (s1 &gt;= -2*n3/(2*n1 + 2*n2 + 2*n3)) &amp; (s1 &lt;= 2*n3/(2*n1 + 2*n2 + 2*n3)) &amp; (s1 &lt;= -2*(-2*n2 + n3)/(2*n1 + 2*n2 + 2*n3)) &amp; (s1 &gt;= 2*(-2*n1 - 3*n3)/(2*n1 + 2*n2 + 2*n3)) &amp; (s1 &gt;= -2*(2*n1 + 3*n3)/(2*n1 + 2*n2 + 2*n3)) &amp; (s1 &lt;= 2*(2*n2 - n3)/(2*n1 + 2*n2 + 2*n3)) &amp; (s1 &gt;= 2*(-2*n1 - 2*n2 - n3)/(2*n1 + 2*n2 + 2*n3)) &amp; (s1 &lt;= -2*(-2*n1 - 2*n2 - n3)/(2*n1 + 2*n2 + 2*n3))</string_value>
        </expression>
      </bounds>
    </ordering_function>
  </functions>
  <potential name="G">
    <expression>
      <string_value type="code" language="python" lines="20">G = (
-R*T*(n1 + n2 + n3)*(-(-n3/(n1 + n2 + n3) + 1)*log(-n3/(n1 + n2 + n3)
+ 1) - (n3/(2*(n1 + n2 + n3)) - s1/2)*log(n3/(2*(n1 + n2 + n3)) -
s1/2) - (n3/(2*(n1 + n2 + n3)) + s1/2)*log(n3/(2*(n1 + n2 + n3)) +
s1/2) - (n2/(n1 + n2 + n3) - n3/(2*(n1 + n2 + n3)) - s1/2)*log(n2/(n1
+ n2 + n3) - n3/(2*(n1 + n2 + n3)) - s1/2) - (-n2/(n1 + n2 + n3) +
n3/(2*(n1 + n2 + n3)) + s1/2 + 1)*log(-n2/(n1 + n2 + n3) + n3/(2*(n1 +
n2 + n3)) + s1/2 + 1)) + (n1 + n2 + n3)*(n2**2*(-P*WvM1FeMg -
WhM1FeMg)/(n1 + n2 + n3)**2 + n2*n3*(Fh/2 - Fs*T/2 + Fv*P/2 - Hex/2 -
P*Vex/2 + P*WvM1FeMg + WhM1FeMg)/(n1 + n2 + n3)**2 + n2*s1*(-Hx/2 -
P*Vx/2 + P*WvM1FeMg + WhM1FeMg)/(n1 + n2 + n3) + n2*(P*WvM1FeMg +
WhM1FeMg - mu1(T, P) + mu2(T, P))/(n1 + n2 + n3) + n3**2*(-Fh/4 +
Fs*T/4 - Fv*P/4 + Hex/4 + P*Vex/4 - P*WvM1FeMg/4 - P*WvM2CaFe/2 -
P*WvM2CaMg/2 + P*WvM2FeMg/4 - WhM1FeMg/4 - WhM2CaFe/2 - WhM2CaMg/2 +
WhM2FeMg/4)/(n1 + n2 + n3)**2 + n3*s1*(-Fh/4 + Fs*T/4 - Fv*P/4 + Hex/4
+ Hx/4 + P*Vex/4 + P*Vx/4 - P*WvM1FeMg/2 - P*WvM2CaFe/2 + P*WvM2CaMg/2
- WhM1FeMg/2 - WhM2CaFe/2 + WhM2CaMg/2)/(n1 + n2 + n3) + n3*(Fh/4 -
Fs*T/4 + Fv*P/4 + Hex/4 + Hx/4 + P*Vex/4 + P*Vx/4 - P*WvM1FeMg/2 +
P*WvM2CaFe/2 + P*WvM2CaMg/2 - WhM1FeMg/2 + WhM2CaFe/2 + WhM2CaMg/2 -
mu1(T, P) + mu3(T, P))/(n1 + n2 + n3) + s1**2*(Hx/4 + P*Vx/4 -
P*WvM1FeMg/4 - P*WvM2FeMg/4 - WhM1FeMg/4 - WhM2FeMg/4) + s1*(Fh/4 -
Fs*T/4 + Fv*P/4 + Hex/4 + Hx/4 + P*Vex/4 + P*Vx/4 - P*WvM1FeMg/2 +
P*WvM2CaFe/2 - P*WvM2CaMg/2 - WhM1FeMg/2 + WhM2CaFe/2 - WhM2CaMg/2) +
mu1(T, P))
)</string_value>
    </expression>
  </potential>
</phase_options>
