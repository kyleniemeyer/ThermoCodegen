<?xml version='1.0' encoding='utf-8'?>
<endmember_options>
  <name name="Na_1_cation_hkf"/>
  <formula name="Na(1)"/>
  <reference>
    <string_value lines="1">Thermocodegen-v0.6/share/thermocodegen/examples/Notebooks/coder_to_xml/endmembers/Example-09-HKF.ipynb</string_value>
  </reference>
  <free_energy_model name="Gibbs">
    <variable name="T">
      <rank name="Scalar"/>
      <units>K</units>
    </variable>
    <variable name="P">
      <rank name="Scalar"/>
      <units>bar</units>
    </variable>
    <parameter name="T_r">
      <rank name="Scalar">
        <value>
          <real_value rank="0">298.15</real_value>
        </value>
        <units>
          <string_value lines="1">'K'</string_value>
        </units>
      </rank>
      <symbol>
        <string_value lines="1">T_r</string_value>
      </symbol>
    </parameter>
    <parameter name="P_r">
      <rank name="Scalar">
        <value>
          <real_value rank="0">1</real_value>
        </value>
        <units>
          <string_value lines="1">'bar'</string_value>
        </units>
      </rank>
      <symbol>
        <string_value lines="1">P_r</string_value>
      </symbol>
    </parameter>
  </free_energy_model>
  <parameters>
    <parameter name="G_ref">
      <rank name="Scalar">
        <value>
          <real_value rank="0">-261880.744</real_value>
        </value>
        <units>
          <string_value lines="1">'J'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="S_ref">
      <rank name="Scalar">
        <value>
          <real_value rank="0">58.40864</real_value>
        </value>
        <units>
          <string_value lines="1">'J/K'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="a1">
      <rank name="Scalar">
        <value>
          <real_value rank="0">0.7694376</real_value>
        </value>
        <units>
          <string_value lines="1">'J/bar-m'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="a2">
      <rank name="Scalar">
        <value>
          <real_value rank="0">-956.044</real_value>
        </value>
        <units>
          <string_value lines="1">'J/bar^2-m'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="a3">
      <rank name="Scalar">
        <value>
          <real_value rank="0">13.623104</real_value>
        </value>
        <units>
          <string_value lines="1">'J/bar-m'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="a4">
      <rank name="Scalar">
        <value>
          <real_value rank="0">-114055.84</real_value>
        </value>
        <units>
          <string_value lines="1">'J/bar^2-m'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="c1">
      <rank name="Scalar">
        <value>
          <real_value rank="0">76.06512</real_value>
        </value>
        <units>
          <string_value lines="1">'J/K-m'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="c2">
      <rank name="Scalar">
        <value>
          <real_value rank="0">-124725.04</real_value>
        </value>
        <units>
          <string_value lines="1">'J-K/m'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="Psi">
      <rank name="Scalar">
        <value>
          <real_value rank="0">2600</real_value>
        </value>
        <units>
          <string_value lines="1">'bar'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="eta">
      <rank name="Scalar">
        <value>
          <real_value rank="0">694656.968</real_value>
        </value>
        <units>
          <string_value lines="1">'Å-J/mole</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="rH">
      <rank name="Scalar">
        <value>
          <real_value rank="0">3.082</real_value>
        </value>
        <units>
          <string_value lines="1">'Å</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="omega0">
      <rank name="Scalar">
        <value>
          <real_value rank="0">138323.04</real_value>
        </value>
        <units>
          <string_value lines="1">'J'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="theta">
      <rank name="Scalar">
        <value>
          <real_value rank="0">228</real_value>
        </value>
        <units>
          <string_value lines="1">'K'</string_value>
        </units>
      </rank>
    </parameter>
    <parameter name="z">
      <rank name="Scalar">
        <value>
          <real_value rank="0">1</real_value>
        </value>
        <units>
          <string_value lines="1">'None'</string_value>
        </units>
      </rank>
    </parameter>
  </parameters>
  <functions/>
  <potential name="G_hk">
    <expression>
      <string_value type="code" language="python" lines="20">G_hk = (
G_ref + P*(T*a1 - a1*theta + a3)/(T - theta) - P_r*(T*a1 - a1*theta +
a3)/(T - theta) - S_ref*(T - T_r) + T*c2*log(T + (-c1*theta**3 -
2*c2*theta)/(c1*theta**2 + 2*c2))/theta**2 -
T*(-T_r*c1*theta**2*log(T_r) - T_r*c1*theta**2 - T_r*c2*log(T_r) +
T_r*c2*log(T_r - c1*theta**3/(c1*theta**2 + 2*c2) -
2*c2*theta/(c1*theta**2 + 2*c2)) + c1*theta**3*log(T_r) + c1*theta**3
+ c2*theta*log(T_r) - c2*theta*log(T_r - c1*theta**3/(c1*theta**2 +
2*c2) - 2*c2*theta/(c1*theta**2 + 2*c2)) + c2*theta)/(T_r*theta**2 -
theta**3) - T_r*c2*log(T_r + (-c1*theta**3 - 2*c2*theta)/(c1*theta**2
+ 2*c2))/theta**2 + T_r*(-T_r*c1*theta**2*log(T_r) - T_r*c1*theta**2 -
T_r*c2*log(T_r) + T_r*c2*log(T_r - c1*theta**3/(c1*theta**2 + 2*c2) -
2*c2*theta/(c1*theta**2 + 2*c2)) + c1*theta**3*log(T_r) + c1*theta**3
+ c2*theta*log(T_r) - c2*theta*log(T_r - c1*theta**3/(c1*theta**2 +
2*c2) - 2*c2*theta/(c1*theta**2 + 2*c2)) + c2*theta)/(T_r*theta**2 -
theta**3) + omega0*(T - T_r)*Y(T_r, P_r) + omega0*(B(T_r, P_r) + 1) -
(B(T, P) + 1)*Piecewise((omega0, Eq(z, 0)), (-eta*z/(rH + gSolvent(T,
P)) + (eta*z/rH + omega0)/(1 + (z/rH + omega0/eta)*Abs(z)*gSolvent(T,
P)/z**2), True)) + (T*a2 - a2*theta + a4)*log(P + Psi)/(T - theta) -
(T*a2 - a2*theta + a4)*log(P_r + Psi)/(T - theta) - (T*c1*theta**2 +
T*c2)*log(T + (-c1*theta**3 - c2*theta + theta*(c1*theta**2 +
c2))/(c1*theta**2 + 2*c2))/theta**2 + (T_r*c1*theta**2 +
T_r*c2)*log(T_r + (-c1*theta**3 - c2*theta + theta*(c1*theta**2 +
c2))/(c1*theta**2 + 2*c2))/theta**2
)</string_value>
    </expression>
  </potential>
</endmember_options>
